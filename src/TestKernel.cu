
#include "../head/TestKernel.cuh"


float testCalculQpart(int method) {

	std::string fileName = "TempsQpart"+std::to_string(method) + ".csv";
	//float elapsedTime;
	std::chrono::high_resolution_clock::time_point a;
	std::chrono::high_resolution_clock::time_point b;
	float time;
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	const int nNAgent = 6;
	const int nNLine = 6;
	const int nSimu = 100;
	const int nRepet = 10;
	int nAgent[nNAgent] = { 10, 100, 500, 1000, 5000, 10000 };//, 10000

	int nLine[nNLine] = { 10, 100, 500, 1000, 5000, 10000 }; // 10000 };
	int blockSize = 256;
	float values[nSimu];
	
	MatrixCPU temps(nNAgent*nNLine, nSimu, 0);
	MatrixCPU nAgentMat(1, nNAgent);
	MatrixCPU nLineMat(1, nNLine);
	for (int j = 0; j < nSimu; j++) {
		values[j] = (float)(rand()) / rand();
	}
	int indice = 0;
	for (int i = 0; i < nNAgent; i++) {
		nAgentMat.set(0, i, nAgent[i]);
		for (int j = 0; j < nNLine; j++) {
			nLineMat.set(0, j, nLine[j]);
			std::cout << "iteration (" << i << ", " << j << ") nAgent " << nAgent[i] << " nline " << nLine[j] << std::endl;
			for (int simu = 0; simu < nSimu; simu++) {
				MatrixGPU alpha(nLine[j], nAgent[i], values[j], 1);
				MatrixGPU Qpart(nLine[j], nAgent[i], 0, 1);
				MatrixGPU alphaTrans(nAgent[i], nLine[j], values[j], 1);
				MatrixGPU QpartTrans(nAgent[i], nLine[j], 0, 1);
				//alpha.setRand(values[j]);
				//alphaTrans.setTrans(&alpha);
				cudaDeviceSynchronize();
				time = 0;
				int numBlocks;
				for (int repet = 0; repet < nRepet; repet++) {
					MatrixGPU alphaCopy(alpha);
					MatrixGPU QpartCopy(Qpart);
					MatrixGPU alphaTransCopy(alphaTrans);
					MatrixGPU QpartTransCopy(QpartTrans);
					int N = nAgent[i];
					int L = nLine[j];
					cudaDeviceSynchronize();
					switch (method)
					{
					case 0:
						numBlocks = L;
						a = std::chrono::high_resolution_clock::now();
						calculQpartLineBloc <<<numBlocks, blockSize, N * sizeof(float) >> > (QpartCopy._matrixGPU, alphaCopy._matrixGPU, N);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					case 1:
						numBlocks = N;
						a = std::chrono::high_resolution_clock::now();
						calculQpartAgentBloc <<<numBlocks, blockSize >> > (QpartCopy._matrixGPU, alphaCopy._matrixGPU, L, N);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					case 2:
						numBlocks = L;
						a = std::chrono::high_resolution_clock::now();
						calculQpartLineBlocTrans << <numBlocks, blockSize, N * sizeof(float) >> > (QpartTransCopy._matrixGPU, alphaTransCopy._matrixGPU, N, L);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					case 3: 
						numBlocks = N;
						a = std::chrono::high_resolution_clock::now();
						calculQpartAgentBlocTrans << <numBlocks, blockSize >> > (QpartTransCopy._matrixGPU, alphaTransCopy._matrixGPU, L, N);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					case 4:
						numBlocks = L;
						a = std::chrono::high_resolution_clock::now();
						calculQpartLineBlocReverse << <numBlocks, blockSize, N * sizeof(float) >> > (QpartCopy._matrixGPU, alphaCopy._matrixGPU, N);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					case 5:
						numBlocks = L;
						a = std::chrono::high_resolution_clock::now();
						calculQpartLineBlocReverseTrans << <numBlocks, blockSize, N * sizeof(float) >> > (QpartTransCopy._matrixGPU, alphaTransCopy._matrixGPU, N, L);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					case 6:
						numBlocks = L;
						a = std::chrono::high_resolution_clock::now();
						calculQpartLineBlocReverseBis << <numBlocks, blockSize, N * sizeof(float) >> > (QpartCopy._matrixGPU, alphaCopy._matrixGPU, N);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					case 7:
						numBlocks = L;
						a = std::chrono::high_resolution_clock::now();
						calculQpartLineBlocReverseBisTrans << <numBlocks, blockSize, N * sizeof(float) >> > (QpartTransCopy._matrixGPU, alphaTransCopy._matrixGPU, N, L);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					default:
						return 0;
						break;
					}
					time += std::chrono::duration_cast<std::chrono::nanoseconds>(b - a).count();
				}
				temps.set(indice, simu, (float)time / nRepet);
			}
			indice++;
		}
	}
	nAgentMat.saveCSV(fileName, mode);
	nLineMat.saveCSV(fileName, mode);
	temps.saveCSV(fileName, mode);
	return temps.sum();
}

__global__ void calculQpartLineBloc(float* Qpart, float* alpha, const int N)
{
	int index = threadIdx.x;
	int step = blockDim.x;
	int l = blockIdx.x;
	extern __shared__ float shAlpha[];

	for (int n = index; n < N; n += step)
	{
		shAlpha[n] = alpha[l * N + n];
	}
	__syncthreads();

	for (int n = index; n < N; n += step)
	{
		float s = 0;
		for (int j = n + 1; j < N; j++) {
			s += shAlpha[j]; // c'est moche cet acc�s de m�moire partag�e
		}
		Qpart[l*N + n] = s;
	}
}

__global__ void calculQpartAgentBloc(float* Qpart, float* alpha, const int L, const int N) {
	int index = threadIdx.x;
	int step = blockDim.x;
	int n = blockIdx.x;
	

	for (int l = index; l < L; l += step)
	{
		float s = 0;
		for (int j = n + 1; j < N; j++) {
			s += alpha[l*N+j]; 
		}
		Qpart[l*N + n] = s;
	}
}

__global__ void calculQpartLineBlocTrans(float* Qpart, float* alpha, const int N, const int nLine)
{
	int index = threadIdx.x;
	int step = blockDim.x;
	int l = blockIdx.x;
	extern __shared__ float shAlpha[];

	for (int n = index; n < N; n += step)
	{
		shAlpha[n] = alpha[ n*nLine + l]; // moche
	}
	__syncthreads();

	for (int n = index; n < N; n += step)
	{
		float s = 0;
		for (int j = n + 1; j < N; j++) {
			s += shAlpha[j]; // c'est moche cet acc�s de m�moire partag�e
		}
		Qpart[ n*nLine +l] = s; // moche
	}
}

__global__ void calculQpartLineBlocReverseTrans(float* Qpart, float* alpha, const int N, const int nLine)
{
	int index = threadIdx.x;
	int step = blockDim.x;
	int l = blockIdx.x;
	extern __shared__ float shAlpha[];

	for (int n = index; n < N; n += step)
	{
		shAlpha[n] = alpha[n * nLine + l]; // moche
	}
	__syncthreads();
	float s_pre = 0;
	int n_pre = N - 1;
	for (int n = (N - index - 1); n >= 0; n -= step)
	{
		float s = 0;
		for (int j = n_pre; j > n; j--) {
			s += shAlpha[j]; // c'est moche cet acc�s de m�moire partag�e
		}
		s = s + s_pre;
		Qpart[n * nLine + l] = s;
		s_pre = s;
		n_pre = n;
	}
}

__global__ void calculQpartAgentBlocTrans(float* Qpart, float* alpha, const int L, const int N) {
	int index = threadIdx.x;
	int step = blockDim.x;
	int n = blockIdx.x;
	

	for (int l = index; l < L; l += step)
	{
		float s = 0;
		for (int j = n + 1; j < N; j++) {
			s += alpha[j * L + l]; // nombre de boucle d�pend du bloc pas du thread, acc�s coalescent
		}
		Qpart[ n * L + l] = s;
	}
}

__global__ void calculQpartLineBlocReverse(float* Qpart, float* alpha, const int N) // est ce que cela marche ???????
{
	int index = threadIdx.x;
	int step = blockDim.x;
	int l = blockIdx.x;
	extern __shared__ float shAlpha[];

	for (int n = index; n < N; n += step)
	{
		shAlpha[n] = alpha[l * N + n];
	}
	__syncthreads();

	float s_pre = 0;
	int n_pre = N-1;
	for (int n = (N-index-1); n >=0; n -= step)
	{
		float s = 0;
		for (int j = n_pre; j > n; j--) {
			s += shAlpha[j]; // c'est moche cet acc�s de m�moire partag�e
		}
		s = s + s_pre;
		Qpart[l * N + n] = s; 
		s_pre = s;
		n_pre = n;
	}
}

__global__ void calculQpartLineBlocReverseBis(float* Qpart, float* alpha, const int N) // est ce que cela marche ???????
{
	int index = threadIdx.x;
	int step = blockDim.x;
	int l = blockIdx.x;
	extern __shared__ float shAlpha[];

	for (int n = index; n < N; n += step)
	{
		shAlpha[n] = alpha[l * N + n];
	}
	__syncthreads();

	for (int n = (N - index - 1); n >= 0; n -= step)
	{
		float s = 0;
		for (int j = N-1; j > n; j--) {
			s += shAlpha[j]; // c'est moche cet acc�s de m�moire partag�e
		}
		Qpart[l * N + n] = s;
	}
}

__global__ void calculQpartLineBlocReverseBisTrans(float* Qpart, float* alpha, const int N, const int nLine) // est ce que cela marche ???????
{
	int index = threadIdx.x;
	int step = blockDim.x;
	int l = blockIdx.x;
	extern __shared__ float shAlpha[];

	for (int n = index; n < N; n += step)
	{
		shAlpha[n] = alpha[l * N + n];
	}
	__syncthreads();

	for (int n = (N - index - 1); n >= 0; n -= step)
	{
		float s = 0;
		for (int j = N - 1; j > n; j--) {
			s += shAlpha[j]; // c'est moche cet acc�s de m�moire partag�e
		}
		Qpart[n * nLine + l] = s;
	}
}


float testCalculAlpha(int method)
{
	std::string fileName = "TempsAlpha" + std::to_string(method) + ".csv";
	//float elapsedTime;
	std::chrono::high_resolution_clock::time_point a;
	std::chrono::high_resolution_clock::time_point b;
	unsigned int time;
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	const int nNAgent = 6;
	const int nNLine = 6;
	const int nSimu = 100;
	const int nRepet = 10;
	int nAgent[nNAgent] = { 10, 100, 500, 1000, 5000, 10000 };
	int nLine[nNLine] = { 10, 100, 500, 1000, 5000, 10000 };
	int blockSize = 256;
	float values[nSimu];
	float values2[nSimu];

	MatrixCPU temps(nNAgent * nNLine, nSimu, 0);
	MatrixCPU nAgentMat(1, nNAgent);
	MatrixCPU nLineMat(1, nNLine);
	for (int j = 0; j < nSimu; j++) {
		values[j] = (float)(rand()) / rand();
		values2[j] = (float)(rand()) / rand();
	}
	int indice = 0;
	for (int i = 0; i < nNAgent; i++) {
		nAgentMat.set(0, i, nAgent[i]);
		for (int j = 0; j < nNLine; j++) {
			nLineMat.set(0, j, nLine[j]);
			//std::cout << "iteration (" << i << ", " << j << ") nAgent " << nAgent[i] << " nline " << nLine[j] << std::endl;
			for (int simu = 0; simu < nSimu; simu++) {
				MatrixGPU G(nLine[j], nAgent[i], values[j], 1);
				MatrixGPU Pn(nAgent[i], 1, values2[j], 1);
				MatrixGPU GTrans(nAgent[i], nLine[j], values[j], 1);
				MatrixGPU alpha(nLine[j], nAgent[i], 0, 1);
				MatrixGPU alphaTrans(nAgent[i], nLine[j], 0, 1);
				//G.setRand(values[j]);
				//GTrans.setTrans(&G);
				//Pn.setRand(values2[j]);
				cudaDeviceSynchronize();
				time = 0;
				int numBlocks;
				
				for (int repet = 0; repet < nRepet; repet++) {
					MatrixGPU alphaCopy(alpha);
					MatrixGPU PnCopy(Pn);
					MatrixGPU GCopy(G);
					MatrixGPU alphaTransCopy(alphaTrans);
					MatrixGPU GTransCopy(GTrans);
					int N = nAgent[i];
					int L = nLine[j];
					const int nThread = 16;
					const int bx = (N + nThread - 1) / nThread;
					const int by = (L + nThread - 1) / nThread;
					dim3 dimBlock(nThread, nThread);
					dim3 gridBlock(bx, by);
					dim3 gridBlockTrans(by, bx);
				
					cudaDeviceSynchronize();
					switch (method)
					{
					case 0:
						numBlocks = N;
						a = std::chrono::high_resolution_clock::now();
						updateAlphaSh << <numBlocks, blockSize >> > (alphaCopy._matrixGPU, GCopy._matrixGPU, PnCopy._matrixGPU, L, N);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					case 1:
						a = std::chrono::high_resolution_clock::now();
						updateAlpha2D << <gridBlock, dimBlock >> > (alphaCopy._matrixGPU, GCopy._matrixGPU, PnCopy._matrixGPU, L, N);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					case 2:
						numBlocks = (N * L + blockSize - 1) / blockSize;
						a = std::chrono::high_resolution_clock::now();
						updateAlpha1D << <numBlocks, blockSize >> > (alphaCopy._matrixGPU, GCopy._matrixGPU, PnCopy._matrixGPU, L, N);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					case 3: 
						numBlocks = N;
						a = std::chrono::high_resolution_clock::now();
						updateAlphaShTrans << <numBlocks, blockSize >> > (alphaTransCopy._matrixGPU, GTransCopy._matrixGPU, PnCopy._matrixGPU, L, N);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					case 4: 
						a = std::chrono::high_resolution_clock::now();
						updateAlpha2DTrans << <gridBlockTrans, dimBlock >> > (alphaTransCopy._matrixGPU, GTransCopy._matrixGPU, PnCopy._matrixGPU, L, N);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					case 5: 
						numBlocks = (N * L + blockSize - 1) / blockSize;
						a = std::chrono::high_resolution_clock::now();
						updateAlpha1DTrans << <numBlocks, blockSize >> > (alphaTransCopy._matrixGPU, GTransCopy._matrixGPU, PnCopy._matrixGPU, L, N);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
						
					default:
						return 0;
						break;
					}
					time += std::chrono::duration_cast<std::chrono::nanoseconds>(b - a).count();
				}
				temps.set(indice, simu, (float)time / nRepet);
			}
			indice++;
		}
	}
	nAgentMat.saveCSV(fileName, mode);
	nLineMat.saveCSV(fileName, mode);
	temps.saveCSV(fileName, mode);
	return temps.sum();
}


__global__ void updateAlphaSh(float* alpha, float* G, float* Pn, const int nLine, const int nAgent)
{
	// un bloc par agent
	int index = threadIdx.x;
	int step = blockDim.x;
	int n = blockIdx.x;
	__shared__ float shPn;
	if (index == 0) {
		shPn = Pn[n];
	}
	__syncthreads();
	for (int l = index; l < nLine; l += step)
	{
		alpha[l * nAgent + n] = G[l * nAgent + n] * shPn;
	}
}

__global__ void updateAlpha2D(float* alpha, float* G, float* Pn, const int nLine, const int nAgent)
{
	int indexX = threadIdx.x + blockIdx.x*blockDim.x;
	int stepX = blockDim.x*gridDim.x;
	int indexY = threadIdx.y + blockIdx.y * blockDim.y;
	int stepY = blockDim.y * gridDim.y;
	
	for (int n = indexX; n < nAgent; n += stepX)
	{
		float PnLocal = Pn[n];
		for (int l = indexY; l < nLine; l += stepY)
		{
			alpha[l * nAgent + n] = G[l * nAgent + n] * PnLocal;
		}
	}
}

__global__ void updateAlpha1D(float* alpha, float* G, float* Pn, const int nLine, const int nAgent)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int step = blockDim.x * gridDim.x;
	int N = nAgent * nLine;

	for (int i = index; i < N; i += step)
	{
		int k = i % nAgent;
		alpha[i] = G[i] * Pn[k];
	}
}

__global__ void updateAlphaShTrans(float* alpha, float* G, float* Pn, const int nLine, const int nAgent)
{
	// un bloc par agent
	int index = threadIdx.x;
	int step = blockDim.x;
	int n = blockIdx.x;
	__shared__ float shPn;
	if (index == 0) {
		shPn = Pn[n];
	}
	__syncthreads();
	for (int l = index; l < nLine; l += step)
	{
		alpha[ n * nLine + l ] = G[n * nLine + l] * shPn;
	}
}

__global__ void updateAlpha2DTrans(float* alpha, float* G, float* Pn, const int nLine, const int nAgent)
{
	// alpha et G en (n,l)
	int indexX = threadIdx.x + blockIdx.x * blockDim.x;
	int stepX = blockDim.x * gridDim.x;
	int indexY = threadIdx.y + blockIdx.y * blockDim.y;
	int stepY = blockDim.y * gridDim.y;

	for (int n = indexY; n < nAgent; n += stepY)
	{
		float PnLocal = Pn[n];
		for (int l = indexX; l < nLine; l += stepX)
		{
			alpha[n*nLine +l] = G[n * nLine + l] * PnLocal;
		}
	}
}

__global__ void updateAlpha1DTrans(float* alpha, float* G, float* Pn, const int nLine, const int nAgent) {

	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int step = blockDim.x * gridDim.x;
	int N = nAgent * nLine;

	for (int i = index; i < N; i += step)
	{
		int k = i / nAgent;
		alpha[i] = G[i] * Pn[k];
	}

}

float testCalculCpa(int method) {
	std::string fileName = "TempsCpa" + std::to_string(method) + ".csv";
	//float elapsedTime;
	std::chrono::high_resolution_clock::time_point a;
	std::chrono::high_resolution_clock::time_point b;
	unsigned int time;
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	const int nNAgent = 6;
	const int nNLine = 6;
	const int nSimu = 100;
	const int nRepet = 10;
	int nAgent[nNAgent] = { 10, 100, 500, 1000, 5000, 10000 };
	int nLine[nNLine] = { 10, 100, 500, 1000, 5000, 10000 };
	int blockSize = 256;
	float values[nSimu];
	float values2[nSimu];

	MatrixCPU temps(nNAgent * nNLine, nSimu, 0);
	MatrixCPU nAgentMat(1, nNAgent);
	MatrixCPU nLineMat(1, nNLine);
	for (int j = 0; j < nSimu; j++) {
		values[j] = (float)(rand()) / rand();
		values2[j] = (float)(rand()) / rand();
	}
	int indice = 0;
	for (int i = 0; i < nNAgent; i++) {
		nAgentMat.set(0, i, nAgent[i]);
		for (int j = 0; j < nNLine; j++) {
			nLineMat.set(0, j, nLine[j]);
			//std::cout << "iteration (" << i << ", " << j << ") nAgent " << nAgent[i] << " nline " << nLine[j] << std::endl;
			for (int simu = 0; simu < nSimu; simu++) {
				MatrixGPU Cp2(nAgent[i], 1, 0, 1);
				MatrixGPU tempL1(nLine[j], 1, values[j], 1);
				MatrixGPU G(nLine[j], nAgent[i], values2[j], 1);
				MatrixGPU GTrans(nAgent[i], nLine[j], values2[j], 1);
				//tempL1.setRand(values[j]);
				//G.setRand(values2[j]);
				//GTrans.setTrans(&G);
				cudaDeviceSynchronize();
				time = 0;
				int numBlocks;
				for (int repet = 0; repet < nRepet; repet++) {
					MatrixGPU Cp2Copy(Cp2);
					MatrixGPU tempL1Copy(tempL1);
					MatrixGPU GCopy(G);
					MatrixGPU GTransCopy(GTrans);
					int N = nAgent[i];
					int L = nLine[j];
					cudaDeviceSynchronize();
					switch (method)
					{
					case 0:
						numBlocks = N;
						a = std::chrono::high_resolution_clock::now();
						updateCp2aTest<256> << <numBlocks, blockSize >> > (Cp2Copy._matrixGPU, tempL1Copy._matrixGPU, GCopy._matrixGPU, L, N);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					case 1:
						numBlocks = N;
						a = std::chrono::high_resolution_clock::now();
						updateCp2aTestTrans<256> << <numBlocks, blockSize >> > (Cp2Copy._matrixGPU, tempL1Copy._matrixGPU, GTransCopy._matrixGPU, L, N);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					default:
						return 0;
						break;
					}
					time += std::chrono::duration_cast<std::chrono::nanoseconds>(b - a).count();
				}
				temps.set(indice, simu, (float)time / nRepet);
			}
			indice++;
		}
	}
	nAgentMat.saveCSV(fileName, mode);
	nLineMat.saveCSV(fileName, mode);
	temps.saveCSV(fileName, mode);
	return temps.sum();
}

template <unsigned int blockSize>
__global__ void updateCp2aTest(float* Cp2, float* diffKappa, float* G, const int nLine, const int nAgent) {
	// un bloc par agent
	int thIdx = threadIdx.x;
	int step = blockDim.x;
	int n = blockIdx.x;
	float sum = 0;
	__shared__ float shArr[blockSize];
	for (int j = thIdx; j < nLine; j += step) {

		float t = G[j * nAgent + n] * diffKappa[j];
		sum += t;
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduceTest<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		Cp2[n] = shArr[0];
	}
}

template <unsigned int blockSize>
__global__ void updateCp2aTestTrans(float* Cp2, float* diffKappa, float* G, const int nLine, const int nAgent) {
	// un bloc par agent
	int thIdx = threadIdx.x;
	int step = blockDim.x;
	int n = blockIdx.x;
	float sum = 0;
	__shared__ float shArr[blockSize];
	for (int j = thIdx; j < nLine; j += step) {

		float t = G[ n * nLine + j] * diffKappa[j];
		sum += t;
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduceTest<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		Cp2[n] = shArr[0];
	}
}

float testCalculCpb(int method)
{
	std::string fileName = "TempsCpb" + std::to_string(method) + ".csv";
	//float elapsedTime;
	std::chrono::high_resolution_clock::time_point a;
	std::chrono::high_resolution_clock::time_point b;
	unsigned int time;
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	const int nNAgent = 6;
	const int nNLine = 6;
	const int nSimu = 100;
	const int nRepet = 10;
	int nAgent[nNAgent] = { 10, 100, 500, 1000, 5000, 10000 };
	int nLine[nNLine] = { 10, 100, 500, 1000, 5000, 10000 };
	int blockSize = 256;
	float values[nSimu];
	float values2[nSimu];

	MatrixCPU temps(nNAgent * nNLine, nSimu, 0);
	MatrixCPU nAgentMat(1, nNAgent);
	MatrixCPU nLineMat(1, nNLine);
	for (int j = 0; j < nSimu; j++) {
		values[j] = (float)(rand()) / rand();
		values2[j] = (float)(rand()) / rand();
	}
	int indice = 0;
	for (int i = 0; i < nNAgent; i++) {
		nAgentMat.set(0, i, nAgent[i]);
		for (int j = 0; j < nNLine; j++) {
			nLineMat.set(0, j, nLine[j]);
			int N = nAgent[i];
			int L = nLine[j];
			//std::cout << "iteration (" << i << ", " << j << ") nAgent " << nAgent[i] << " nline " << nLine[j] << std::endl;
			for (int simu = 0; simu < nSimu; simu++) {
				MatrixGPU tempN1(N, 1, 0, 1);
				MatrixGPU Qpart(L, N, values[simu], 1);
				MatrixGPU QpartTrans(N, L, values[simu], 1);
				MatrixGPU G(L, N, values2[simu], 1);
				MatrixGPU GTrans(N, L, values2[simu], 1);
				//Qpart.setRand(values[simu]); en faisant de l'al�atoire, la mesure "plante" au bout d'un moment...
				//G.setRand(values2[simu]);
				//GTrans.setTrans(&G);
				//QpartTrans.setTrans(&Qpart);
				cudaDeviceSynchronize();
				time = 0;
				int numBlocks;
				for (int repet = 0; repet < nRepet; repet++) {
					MatrixGPU tempN1Copy(tempN1);
					MatrixGPU QpartCopy(Qpart);
					MatrixGPU QpartTransCopy(QpartTrans);
					MatrixGPU GCopy(G);
					MatrixGPU GTransCopy(GTrans);
					
					cudaDeviceSynchronize();
					switch (method)
					{
					case 0:
						numBlocks = N;
						a = std::chrono::high_resolution_clock::now();
						updateCp2bTest<256> << <numBlocks, blockSize >> > (tempN1Copy._matrixGPU, GCopy._matrixGPU, QpartCopy._matrixGPU, L, N);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					case 1:
						numBlocks = N;
						a = std::chrono::high_resolution_clock::now();
						updateCp2bTestTrans<256> << <numBlocks, blockSize >> > (tempN1Copy._matrixGPU, GTransCopy._matrixGPU, QpartTransCopy._matrixGPU, L, N);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					default:
						return 0;
						break;
					}
					time += std::chrono::duration_cast<std::chrono::nanoseconds>(b - a).count();
				}
				temps.set(indice, simu, (float)time / nRepet);
			}
			indice++;
		}
	}
	nAgentMat.saveCSV(fileName, mode);
	nLineMat.saveCSV(fileName, mode);
	temps.saveCSV(fileName, mode);
	return temps.sum();
}
template <unsigned int blockSize>
__global__ void updateCp2bTest(float* tempN1, float* G, float* Qpart, const int nLine, const int nAgent)
{
	// un bloc par agent
	int thIdx = threadIdx.x;
	int step = blockDim.x;
	int n = blockIdx.x;
	float sum = 0;
	__shared__ float shArr[blockSize];
	for (int j = thIdx; j < nLine; j += step) {

		float t = G[j * nAgent + n] * Qpart[j * nAgent + n];
		sum += t;
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduceTest<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		tempN1[n] = 2 * shArr[0];
	}

}

template <unsigned int blockSize>
__global__ void updateCp2bTestTrans(float* tempN1, float* G, float* Qpart, const int nLine, const int nAgent)
{
	// un bloc par agent
	int thIdx = threadIdx.x;
	int step = blockDim.x;
	int n = blockIdx.x;
	float sum = 0;
	__shared__ float shArr[blockSize];
	for (int j = thIdx; j < nLine; j += step) {

		float t = G[n * nLine + j] * Qpart[n * nLine + j];
		sum += t;
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduceTest<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		tempN1[n] = 2 * shArr[0];
	}

}


float testCalculQtot(int method)
{
	std::string fileName = "TempsQtot" + std::to_string(method) + ".csv";
	//float elapsedTime;
	std::chrono::high_resolution_clock::time_point a;
	std::chrono::high_resolution_clock::time_point b;
	unsigned int time;
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	const int nNAgent = 6;
	const int nNLine = 6;
	const int nSimu = 100;
	const int nRepet = 10;
	int nAgent[nNAgent] = { 10, 100, 500, 1000, 5000, 10000 };
	int nLine[nNLine] = { 10, 100, 500, 1000, 5000, 10000 };
	int blockSize = 256;
	float values[nSimu];
	float values2[nSimu];

	MatrixCPU temps(nNAgent * nNLine, nSimu, 0);
	MatrixCPU nAgentMat(1, nNAgent);
	MatrixCPU nLineMat(1, nNLine);
	for (int j = 0; j < nSimu; j++) {
		values[j] = (float)(rand()) / rand();
		values2[j] = (float)(rand()) / rand();
	}
	int indice = 0;
	for (int i = 0; i < nNAgent; i++) {
		nAgentMat.set(0, i, nAgent[i]);
		for (int j = 0; j < nNLine; j++) {
			nLineMat.set(0, j, nLine[j]);
			//std::cout << "iteration (" << i << ", " << j << ") nAgent " << nAgent[i] << " nline " << nLine[j] << std::endl;
			for (int simu = 0; simu < nSimu; simu++) {
				MatrixGPU Qtot(nLine[j], 1, 0, 1);
				MatrixGPU Qpart(nLine[j], nAgent[i], values[j], 1);
				MatrixGPU QpartTrans(nAgent[i], nLine[j], values[j], 1);
				MatrixGPU alpha(nLine[j], nAgent[i], values2[j], 1);
				MatrixGPU alphaTrans(nAgent[i], nLine[j], values2[j], 1);
				//Qpart.setRand(values[j]);
				//alpha.setRand(values2[j]);
				//QpartTrans.setTrans(&Qpart);
				//alphaTrans.setTrans(&alpha);
				
				cudaDeviceSynchronize();
				time = 0;
				int numBlocks;
				for (int repet = 0; repet < nRepet; repet++) {
					MatrixGPU QtotCopy(Qtot);
					MatrixGPU QpartCopy(Qpart);
					MatrixGPU QpartTransCopy(QpartTrans);
					MatrixGPU alphaCopy(alpha);
					MatrixGPU alphaTransCopy(alphaTrans);
					int N = nAgent[i];
					int L = nLine[j];
					cudaDeviceSynchronize();
					switch (method)
					{
					case 0:
						a = std::chrono::high_resolution_clock::now();
						QtotCopy.sum(&alphaCopy);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					case 1:
						numBlocks = ( L + blockSize - 1) / blockSize;
						a = std::chrono::high_resolution_clock::now();
						updateQtotTest << <numBlocks, blockSize >> > (QtotCopy._matrixGPU, QpartCopy._matrixGPU, alphaCopy._matrixGPU, L, N);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					case 2:
						numBlocks = (L + blockSize - 1) / blockSize;
						a = std::chrono::high_resolution_clock::now();
						updateQtotTestTrans << <numBlocks, blockSize >> > (QtotCopy._matrixGPU, QpartTransCopy._matrixGPU, alphaTransCopy._matrixGPU, L);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					default:
						return 0;
						break;
					}
					time += std::chrono::duration_cast<std::chrono::nanoseconds>(b - a).count();
				}
				temps.set(indice, simu, (float)time / nRepet);
			}
			indice++;
		}
	}
	nAgentMat.saveCSV(fileName, mode);
	nLineMat.saveCSV(fileName, mode);
	temps.saveCSV(fileName, mode);
	return temps.sum();
}

__global__ void updateQtotTest(float* Qtot, float* Qpart, float* alpha, const int nLine, const int nAgent) {

	
	int thIdx = threadIdx.x + blockIdx.x * blockDim.x;
	int step = blockDim.x * gridDim.x;
	
	for (int l = thIdx; l < nLine; l += step) {

		Qtot[l] = Qpart[l * nAgent] + alpha[l * nAgent];
	}
}

__global__ void updateQtotTestTrans(float* Qtot, float* Qpart, float* alpha, const int nLine) {

	
	int thIdx = threadIdx.x + blockIdx.x * blockDim.x;
	int step = blockDim.x * gridDim.x;

	for (int l = thIdx; l < nLine; l += step) {

		Qtot[l] = Qpart[l] + alpha[l];
	}
}

template <unsigned int blockSize>
__device__ void warpReduceTest(volatile float* sdata, unsigned int tid) {
	if (blockSize >= 64) sdata[tid] += sdata[tid + 32];
	if (blockSize >= 32) sdata[tid] += sdata[tid + 16];
	if (blockSize >= 16) sdata[tid] += sdata[tid + 8];
	if (blockSize >= 8) sdata[tid] += sdata[tid + 4];
	if (blockSize >= 4) sdata[tid] += sdata[tid + 2];
	if (blockSize >= 2) sdata[tid] += sdata[tid + 1];
}

float testCalculCp(int method) {
	std::string fileName = "TempsCp" + std::to_string(method) + ".csv";
	//float elapsedTime;
	std::chrono::high_resolution_clock::time_point a;
	std::chrono::high_resolution_clock::time_point b;
	unsigned int time;
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	const int nNAgent = 6;
	const int nNLine = 6;
	const int nSimu = 100;
	const int nRepet = 10;
	int nAgent[nNAgent] = { 10, 100, 500, 1000, 5000, 10000 };
	int nLine[nNLine] = { 10, 100, 500, 1000, 5000, 10000 };
	int blockSize = 256;
	float values[nSimu];
	float values2[nSimu];
	float values3[nSimu];
	float values4[nSimu];

	MatrixCPU temps(nNAgent * nNLine, nSimu, 0);
	MatrixCPU nAgentMat(1, nNAgent);
	MatrixCPU nLineMat(1, nNLine);
	for (int j = 0; j < nSimu; j++) {
		values[j] = (float)(rand()) / rand();
		values2[j] = (float)(rand()) / rand();
		values3[j] = (float)(rand()) / rand();
		values4[j] = (float)(rand()) / rand();
	}
	int indice = 0;
	for (int i = 0; i < nNAgent; i++) {
		nAgentMat.set(0, i, nAgent[i]);
		for (int j = 0; j < nNLine; j++) {
			nLineMat.set(0, j, nLine[j]);
			std::cout << "iteration (" << i << ", " << j << ") nAgent " << nAgent[i] << " nline " << nLine[j] << std::endl;
			for (int simu = 0; simu < nSimu; simu++) {
				MatrixGPU Cp2(nAgent[i], 1, 0, 1);
				MatrixGPU tempL1(nLine[j], 1, values[j], 1);
				MatrixGPU G(nLine[j], nAgent[i], values2[j], 1);
				MatrixGPU GTrans(nAgent[i], nLine[j], values2[j], 1);
				MatrixGPU Qpart(nLine[j], nAgent[i], values3[j], 1);
				MatrixGPU QpartTrans(nAgent[i], nLine[j], values3[j], 1);
				MatrixGPU nVoisin(nAgent[i], 1, nAgent[i], 1);
				float rho1 = values4[j];
				//tempL1.setRand(values[j]);
				//G.setRand(values2[j]);
				//GTrans.setTrans(&G);
				cudaDeviceSynchronize();
				time = 0;
				int numBlocks;
				for (int repet = 0; repet < nRepet; repet++) {
					MatrixGPU Cp2Copy(Cp2);
					MatrixGPU tempL1Copy(tempL1);
					MatrixGPU GCopy(G);
					MatrixGPU GTransCopy(GTrans);
					MatrixGPU QpartCopy(Qpart);
					MatrixGPU QpartTransCopy(QpartTrans);
					MatrixGPU nVoisinCopy(nVoisin);
					int N = nAgent[i];
					int L = nLine[j];
					cudaDeviceSynchronize();
					switch (method)
					{
					case 0:
						numBlocks = N;
						a = std::chrono::high_resolution_clock::now();
						updateCp2Test<256> << <numBlocks, blockSize >> > (Cp2Copy._matrixGPU, tempL1Copy._matrixGPU, GCopy._matrixGPU, QpartCopy._matrixGPU, nVoisinCopy._matrixGPU, rho1, L, N);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					case 1:
						numBlocks = N;
						a = std::chrono::high_resolution_clock::now();
						updateCp2TestTrans<256> << <numBlocks, blockSize >> > (Cp2Copy._matrixGPU, tempL1Copy._matrixGPU, GTransCopy._matrixGPU, QpartTransCopy._matrixGPU, nVoisinCopy._matrixGPU, rho1, L, N);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					default:
						return 0;
						break;
					}
					time += std::chrono::duration_cast<std::chrono::nanoseconds>(b - a).count();
				}
				temps.set(indice, simu, (float)time / nRepet);
			}
			indice++;
		}
	}
	nAgentMat.saveCSV(fileName, mode);
	nLineMat.saveCSV(fileName, mode);
	temps.saveCSV(fileName, mode);
	return temps.sum();
}

template <unsigned int blockSize>
__global__ void updateCp2Test(float* Cp2, float* diffKappa, float* G, float* Qpart, float* nVoisin, float rho1, const int nLine, const int nAgent) {
	// un bloc par agent
	int thIdx = threadIdx.x;
	int step = blockDim.x;
	int n = blockIdx.x;
	float sum = 0;
	__shared__ float shArr[blockSize];
	for (int j = thIdx; j < nLine; j += step) {

		float t = G[j * nAgent + n] * (diffKappa[j] + 2 * Qpart[j * nAgent + n]);
		sum += t;
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduceTest<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		Cp2[n] = rho1 * nVoisin[n] * shArr[0];
	}
}

template <unsigned int blockSize>
__global__ void updateCp2TestTrans(float* Cp2, float* diffKappa, float* G, float* Qpart,float* nVoisin, float rho1, const int nLine, const int nAgent) {
	// un bloc par agent
	int thIdx = threadIdx.x;
	int step = blockDim.x;
	int n = blockIdx.x;
	float sum = 0;
	__shared__ float shArr[blockSize];
	for (int j = thIdx; j < nLine; j += step) {
		float Gloc = G[n * nLine + j];
		float dKloc = diffKappa[j];
		float Q = Qpart[n * nLine + j];
		float t = Gloc * (dKloc + 2 * Q);
		sum += t;
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduceTest<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		Cp2[n] = rho1 * nVoisin[n] * shArr[0];
	}
}

float testCalculResX(int method)
{
	std::string fileName = "TempsResX" + std::to_string(method) + ".csv";
	//float elapsedTime;
	std::chrono::high_resolution_clock::time_point a;
	std::chrono::high_resolution_clock::time_point b;
	unsigned int time;
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	const int nNAgent = 1;
	const int nNLine = 8;
	const int nSimu = 100;
	const int nRepet = 10;
	int nLine[nNLine] = { 10, 100, 500, 1000, 5000, 10000, 100000, 1000000 };
	int blockSize = 256;
	float values[nSimu];
	float values2[nSimu];
	float values3[nSimu];
	float values4[nSimu];

	MatrixCPU temps(nNAgent * nNLine, nSimu, 0);
	MatrixCPU nAgentMat(1, nNAgent);
	MatrixCPU nLineMat(1, nNLine);
	for (int j = 0; j < nSimu; j++) {
		values[j] =  (float)(rand()) / rand() * (0.5 - rand() % 2);
		values2[j] = (float)(rand()) / rand() * (0.5 - rand() % 2);
		values3[j] = (float)(rand()) / rand() * (0.5 - rand() % 2);
		values4[j] = (float)(rand()) / rand() * (0.5 - rand() % 2);
	}
	int indice = 0;
	for (int i = 0; i < nNAgent; i++) {
		for (int j = 0; j < nNLine; j++) {
			nLineMat.set(0, j, nLine[j]);
			int numBlocks = ceil((nLine[j] + blockSize - 1) / blockSize);
			std::cout << "iteration (" << i << ", " << j << ") "  << " nline " << nLine[j] << std::endl;
			for (int simu = 0; simu < nSimu; simu++) {
				MatrixGPU res(nLine[j], 1, 0, 1);
				MatrixGPU tempL2(nLine[j], 1, 0, 1);
				MatrixGPU kappa1(nLine[j], 1, values[j], 1);
				MatrixGPU kappa2(nLine[j], 1, values2[j], 1);
				MatrixGPU kappaPre1(nLine[j], 1, values3[j], 1);
				MatrixGPU kappaPre2(nLine[j], 1, values4[j], 1);
				cudaDeviceSynchronize();
				time = 0;
				int numBlocks;
				for (int repet = 0; repet < nRepet; repet++) {
					MatrixGPU resCopy(res);
					MatrixGPU tempL2Copy(tempL2);
					MatrixGPU kappa1Copy(kappa1);
					MatrixGPU kappa2Copy(kappa2);
					MatrixGPU kappaPre1Copy(kappaPre1);
					MatrixGPU kappaPre2Copy(kappaPre2);
					int L = nLine[j];
					cudaDeviceSynchronize();
					switch (method)
					{
					case 0:
						a = std::chrono::high_resolution_clock::now();
						resCopy.set(&kappa1Copy);
						tempL2.set(&kappa2Copy);
						kappaPre1Copy.projectNeg();
						kappaPre2Copy.projectNeg();
						resCopy.projectNeg();
						tempL2.projectNeg();
						resCopy.subtract(&kappaPre1Copy);
						tempL2.subtract(&kappaPre2Copy);
						resCopy.multiplyT(&resCopy);
						tempL2.multiplyT(&tempL2);
						resCopy.add(&tempL2);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					case 1:
						a = std::chrono::high_resolution_clock::now();
						updateResXTest << <numBlocks, blockSize >> > (resCopy._matrixGPU, kappa1Copy._matrixGPU, kappa2Copy._matrixGPU, kappaPre1Copy._matrixGPU, kappaPre2Copy._matrixGPU, L);
						cudaDeviceSynchronize();
						b = std::chrono::high_resolution_clock::now();
						break;
					default:
						return 0;
						break;
					}
					time += std::chrono::duration_cast<std::chrono::nanoseconds>(b - a).count();
				}
				temps.set(indice, simu, (float)time / nRepet);
			}
			indice++;
		}
	}
	nAgentMat.saveCSV(fileName, mode);
	nLineMat.saveCSV(fileName, mode);
	temps.saveCSV(fileName, mode);
	return temps.sum();
}

__global__ void updateResXTest(float* res, float* Kappa1, float* Kappa2, float* KappaPre1, float* KappaPre2, const int nLine) {

	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;
	for (int l = index; l < nLine; l += step)
	{
		float k1 = (Kappa1[l] < 0) * Kappa1[l];
		float k2 = (Kappa2[l] < 0) * Kappa2[l];
		float k1pre = (KappaPre1[l] < 0) * KappaPre1[l];
		float k2pre = (KappaPre2[l] < 0) * KappaPre2[l];

		k1 -= k1pre;
		k2 -= k2pre;

		res[l] = k1 * k1 + k2 * k2;	
	}
}




float testCalculLAMBDABt1(int method)
{
	std::string fileName = "TempsLAMBDA" + std::to_string(method) + ".csv";
	//float elapsedTime;
	std::chrono::high_resolution_clock::time_point a;
	std::chrono::high_resolution_clock::time_point b;
	unsigned int time;
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	const int nNAgent = 6;
	const int nSimu = 100;
	const int nRepet = 10;
	int nAgent[nNAgent] = { 10, 100, 500, 1000, 5000, 10000 };
	int ntrade[nNAgent];
	int blockSize = 256;
	float values[nSimu];
	float values2[nSimu];
	float rhos[nSimu];
	MatrixCPU temps(nNAgent, nSimu, 0);
	MatrixCPU nAgentMat(1, nNAgent, 0);
	MatrixCPU nTradeMat(1, nNAgent, 0);
	for (int j = 0; j < nSimu; j++) {
		values[j] = (float)(rand()) / rand();
		values2[j] = (float)(rand()) / rand();
		rhos[j] = (float)(rand() % 100) / rand();
	}
	for (int i = 0; i < nNAgent; i++) {
		ntrade[i] = nAgent[i] * nAgent[i] / 2;
		int numBlocks = ceil((ntrade[i] + blockSize - 1) / blockSize);
		nAgentMat.set(0, i, nAgent[i]);
		nTradeMat.set(0, i, ntrade[i]);
		MatrixGPU Bt1(ntrade[i], 1, 0, 1);
		MatrixGPU CoresLinAgent(ntrade[i], 1);
		MatrixGPU CoresLinVoisin(ntrade[i], 1);
		MatrixGPU CoresMatLin(nAgent[i], nAgent[i], -1);
		MatrixGPU CoresLinTrans(ntrade[i], 1);
		int indice = 0;
		int idVoisin = 0;

		for (int idAgent = 0; idAgent < nAgent[i]; idAgent++) {
			int Nvoisinmax = nAgent[i] / 2;
			if (idAgent < Nvoisinmax) {
				idVoisin = 0;
			}
			else {
				idVoisin = nAgent[i] / 2;
			}
			for (int voisin = idVoisin; voisin < Nvoisinmax; voisin++) {
				CoresLinAgent.set(indice, 0, idAgent);
				CoresLinVoisin.set(indice, 0, voisin);
				CoresMatLin.set(idAgent, voisin, indice);
				indice = indice + 1;
			}
		}
		for (int lin = 0; lin < ntrade[i]; lin++) {
			int i = CoresLinAgent.get(lin, 0);
			int j = CoresLinVoisin.get(lin, 0);
			int k = CoresMatLin.get(j, i);
			CoresLinTrans.set(lin, 0, k);
		}
		CoresLinAgent.transferGPU();
		CoresLinVoisin.transferGPU();
		CoresMatLin.transferGPU();
		CoresLinTrans.transferGPU();
		//std::cout << "iteration (" << i << ", " << j << ") nAgent " << nAgent[i] << " nline " << nLine[j] << std::endl;
		for (int simu = 0; simu < nSimu; simu++) {
			MatrixGPU LAMBDALin(ntrade[i], 1, values[simu], 1);
			MatrixGPU trade(ntrade[i], 1, values2[simu], 1);
			float rho = rhos[simu];
			cudaDeviceSynchronize();
			time = 0;
			for (int repet = 0; repet < nRepet; repet++) {
				MatrixGPU LAMBDALinCopy(LAMBDALin);
				MatrixGPU tradeCopy(trade);
				MatrixGPU CoresLinTransCopy(CoresLinTrans);
				MatrixGPU Bt1Copy(Bt1);
				int M = ntrade[i];
				cudaDeviceSynchronize();
				switch (method)
				{
				case 0:
					a = std::chrono::high_resolution_clock::now();
					updateLAMBDAGPUTest << <numBlocks, blockSize >> > (LAMBDALinCopy._matrixGPU, tradeCopy._matrixGPU, rho, CoresLinTransCopy._matrixGPU, M);
					updateBt1GPUTest << <numBlocks, blockSize >> > (Bt1Copy._matrixGPU, tradeCopy._matrixGPU, rho, LAMBDALinCopy._matrixGPU, CoresLinTransCopy._matrixGPU, M);
					cudaDeviceSynchronize();
					b = std::chrono::high_resolution_clock::now();
					break;
				case 1:
					a = std::chrono::high_resolution_clock::now();
					updateLAMBDABt1GPUTest << <numBlocks, blockSize >> > (Bt1Copy._matrixGPU, LAMBDALinCopy._matrixGPU, tradeCopy._matrixGPU, rho, CoresLinTransCopy._matrixGPU, M);
					cudaDeviceSynchronize();
					b = std::chrono::high_resolution_clock::now();
					break;
				default:
					return 0;
					break;
				}
				time += std::chrono::duration_cast<std::chrono::nanoseconds>(b - a).count();
			}
			temps.set(i, simu, (float)time / nRepet);
		}
	}
	nAgentMat.saveCSV(fileName, mode);
	nTradeMat.saveCSV(fileName, mode);
	temps.saveCSV(fileName, mode);
	return temps.sum();
}



__global__ void updateLAMBDAGPUTest(float* LAMBDALin, float* tradeLin, float rho, float* CoresLinTrans, int const N)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;
	for (int l = index; l < N; l += step)
	{
		float m = LAMBDALin[l];
		int k = CoresLinTrans[l];
		LAMBDALin[l] = m + 0.5 * rho * (tradeLin[l] + tradeLin[k]);
	}
}
__global__ void updateBt1GPUTest(float* Bt1, float* tradeLin, float rho, float* LAMBDA, float* CoresLinTrans, int const N)
{
	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;
	for (int l = index; l < N; l += step)
	{
		int k = CoresLinTrans[l];
		Bt1[l] = 0.5 * (tradeLin[l] - tradeLin[k]) - LAMBDA[l] / rho;
	}

}

__global__ void updateLAMBDABt1GPUTest(float* Bt1, float* LAMBDA, float* tradeLin, float rho, float* CoresLinTrans, int const N) {

	int index = blockIdx.x * blockDim.x + threadIdx.x;
	int step = blockDim.x * gridDim.x;
	for (int l = index; l < N; l += step)
	{
		int k = CoresLinTrans[l];
		float m = LAMBDA[l] + 0.5 * rho * (tradeLin[l] + tradeLin[k]);
		Bt1[l] = 0.5 * (tradeLin[l] - tradeLin[k]) - m / rho;
		LAMBDA[l] = m;
	}


}