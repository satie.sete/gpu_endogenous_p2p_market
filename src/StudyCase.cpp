#include "../head/StudyCase.h"


void StudyCase::setMatFromFile(const std::string& path, const std::string& date, MatrixCPU* Pgen, MatrixCPU* P0, MatrixCPU* costGen)
{
	std::string namePgen = "/PowerMaxCountry.csv";
	std::string nameP0 = "/load/Country_" + date + ".txt";
	std::string namecostGen = "/CoefPoly.csv";
	Pgen->setFromFile(path + namePgen);
	P0->setFromFile(path + nameP0,1);
	costGen->setFromFile(path + namecostGen);

}

void StudyCase::setGenFromFile(const std::string& path, MatrixCPU* Pgen, MatrixCPU* costGen, MatrixCPU* BusGen)
{
	MatrixCPU Generator(_nGen, 3);
	Generator.setFromFile(path, 1);
	
	for (int i = 0; i < _nGen; i++) {
		
		costGen->set(i, 0, Generator.get(i, 0));
		Pgen->set(i, 0, Generator.get(i, 1));
		BusGen->set(i, 0, Generator.get(i, 2));
	}
}

void StudyCase::setGridFromFile(const std::string& path, MatrixCPU* fileBusAgent)
{
	MatrixCPU matFile(_nLine, 4);
	matFile.setFromFile(path);
	_nLineConstraint = 0;
	for (int i = 0; i < _nLine; i++) {
		int nodeFromFile = matFile.get(i, 0);
		int nodeToFile = matFile.get(i, 1);
		float react = matFile.get(i, 2); 
		if (react == 100000) { // cas pas de donn�e dans le r�seau europ�en
			react = 100;// que faire de ces "non" donn�es ?
		}
		float limit = matFile.get(i, 3);

		int nodeFrom = fileBusAgent->get(nodeFromFile, 0);
		int nodeTo = fileBusAgent->get(nodeToFile, 0);

		//std::cout << " Ligne numero " << i << " entre bus " << nodeFromFile << " et " << nodeToFile << " limite " << limit << " react " << react << std::endl;
		//std::cout << " Ligne numero " << i << " entre bus " << nodeFromFile << " et " << nodeToFile << " dans le fichier mais en vrai c'est entre " << nodeFrom << " et" << nodeTo << std::endl;
		
		_LineImpedance.set(i, i, react);
		_CoresBusLine.set(nodeFrom, i, 1);
		_CoresBusLine.set(nodeTo, i, -1);
		_CoresLineBus.set(i, 0, nodeFrom);
		_CoresLineBus.set(i, 1, nodeTo);
		if (limit > 0) {
			_nLineConstraint++;
			_lineLimits.set(i, 0, limit);
		}
		//else {
			
			//_lineLimits.set(i, 0, 1000000);
			//_lineLimits.set(i, 0, FLT_MAX);
		//}
	}
}

void StudyCase::setBusFromFile(const std::string& path, MatrixCPU* fileCoresBus)
{
	int zone = 0;
	std::ifstream myfile(path, std::ios::in);
	bool found = false;
	int indice = zone;
	if (myfile)
	{
		for (int i = 0; i < _nBus; i++) {		
			int idAgent;
			int idBus;
			std::string country;
			myfile >> idAgent;
			myfile >> idBus;
			myfile >> country;
			fileCoresBus->set(idAgent, 0, idBus);
			
			found = false;
			indice = zone;
			for (int j = 0; j < _nameZone.size(); j++) {
				std::string value = _nameZone[j];
				if (!value.compare(country)) {
					found = true;
					indice = j;
					break;
				}
			}
			if (!found) {
				_nameZone.push_back(country);
				zone++;
			}
			_zoneBus.set(idAgent, 0, indice);

		}
		myfile.close();
	}
	else {
		throw std::invalid_argument("can't open this file");
	}
}

void StudyCase::setDistance(bool alreadyDefine, std::string name )
{
	_Distance = MatrixCPU(_nAgent, _nAgent);
	if (alreadyDefine) {
		_Distance.setFromFile(name);
	}
	else {
		std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
		for (int i = 0; i < _nAgent; i++) {
			for (int j = i+1; j < _nAgent; j++) {
				float sum = 0;
				for (int k = 0; k < _nLine; k++) {
					float p = _SensiPower.get(k, i) - _SensiPower.get(k, j);
					sum = sum + fabs(p);
					// on remarque ici qu'ajouter 1 � tous les coef de G, ne modifie pas la distance
				}
				_Distance.set(i, j, sum);
				_Distance.set(j, i, sum);
			}
		}
		_Distance.saveCSV(name, mode);
	}
}

void StudyCase::setBricolage(float offset)
{
	std::cout << "Attention bricolage de G" << std::endl;
	MatrixCPU Bricolage(_nLine, _nAgent, offset);
	MatrixCPU BricolageReduce(_nLineConstraint, _nAgent, offset);
	_SensiPower.add(&Bricolage);
	_SensiPowerReduce.add(&BricolageReduce);
}

void StudyCase::setLineLimitMin(float min)
{
	if (lineoffset) {
		lineoffset = 0;
	}
	_lineLimitsChange = getLineLimit();
	lineMin = min;
	for (int l = 0; l < getNLine(); l++) {
		if (_lineLimitsChange.get(l, 0) < lineMin) {
			_lineLimitsChange.set(l, 0, lineMin);
		}
	}
}

void StudyCase::setLineLimitRelaxation(float eps)
{
	if (lineMin) {
		lineMin = 0;
	}
	_lineLimitsChange = getLineLimit();
	lineoffset = eps;
	for (int l = 0; l < getNLine(); l++) {
		
		_lineLimitsChange.increment(l, 0, -eps);
		
	}
}


void StudyCase::CalcGridSensi()
{
	MatrixCPU temp1(_nLine, _nBus); // BC^T 
	MatrixCPU temp2(_nBus, _nBus); // CBC^T
	MatrixCPU temp3(_nBus, _nBus); // (CBC^T)^-1 avec mise � 0 de la ligne et colonne du noued de ref
	MatrixCPU temp33(_nBus - 1, _nBus - 1); //(CBC ^ T) ^ -1 sans la ligne et colonne du noued de ref
	MatrixCPU temp22(_nBus - 1, _nBus - 1); // on enl�ve la ligne du noeud de ref�rence
	_SensiBusLine = MatrixCPU(_nLine, _nBus);
	MatrixCPU result(_nBus - 1, _nBus - 1);
	
	//MatrixCPU identity(_nBus - 1, _nBus - 1);
	//identity.setEyes(1);

	temp1.multiplyTrans(&_LineImpedance, &_CoresBusLine);
	temp2.multiply(&_CoresBusLine, &temp1);
	
	temp2.getBloc(&temp22, 1, _nBus, 1, _nBus);

	if (_invertMethod==1) {
		
		MatrixGPU temp33GPU(_nBus-1, _nBus-1, 0, 1);
		
		MatrixGPU temp22GPU(temp22,1);	
		temp33GPU.invertGaussJordan(&temp22GPU);
		temp33GPU.toMatCPU(temp33);


		result.multiply(&temp33,&temp22);
		//float err = result.distance2(&identity);

		//std::cout << "err GPU " << err << std::endl;


	}
	else if(_invertMethod==2)
	{
		temp33.invertGaussJordan(&temp22);
		result.multiply(&temp33, &temp22);
		//float err = result.distance2(&identity);

		//std::cout << "err CPU " << err << std::endl;
	}
	else {
	
		temp33.invertEigen(&temp22);
		result.multiply(&temp33, &temp22);
		//float err = result.distance2(&identity);

		//std::cout << "err Eigen " << err << std::endl;
	}

	temp3.setBloc(1, _nBus, 1, _nBus, &temp33);
	_SensiBusLine.multiply(&temp1, &temp3);
	//temp4.display();
	
}

float StudyCase::rand1() const
{
	float a = (float)(rand()) / ((float)(RAND_MAX));
	return a;
}

int StudyCase::getNFileline(std::string nameFile)
{
	int number_of_lines = 0;
	std::string line;
	std::ifstream myfile(nameFile);

	while (std::getline(myfile, line))
		++number_of_lines;
	return number_of_lines;
}

void StudyCase::genConnec(MatrixCPU* connec, int nCons,int nGen, int nPro)
{
	
	int nAgent = nPro + nGen + nCons;
#ifdef DEBUG_CONSTRUCTOR
	std::cout << " nPro =" << nPro << " nGen =" << nGen << " nCons =" << nCons << " nAgent =" << nAgent << std::endl;
	std::cout << " row =" << connec->getNLin() << " column =" << connec->getNCol() << std::endl;
#endif
	
	for (int i = 0; i < nCons;i++) {
		for (int j = nCons; j < nAgent;j++) {
			connec->set(i, j, 1);
		}
	}
	for (int i = nCons; i < nCons+nGen; i++) {
		for (int j = 0; j < nCons;j++) {
			connec->set(i, j, 1);
		}
		for (int j = nCons + nGen; j < nAgent;j++) {
			connec->set(i, j, 1);
		}
	}
	for (int i = nCons + nGen; i < nAgent;i++) {
		for (int j = 0; j < nCons; j++) {
			connec->set(i, j, 1);
		}
		for (int j = nCons; j < nCons + nGen;j++) {
			connec->set(i, j, 1);
		}
	}
}

StudyCase::StudyCase()
{
	 _nAgent = 0;
	 _nPro = 0;
	 _nGen = 0;
	 _nCons = 0;
	 _nLine = 0;
	 _nBus = 0;
	 _timeInit = 0;
	 
}

void StudyCase::Set29node()
{
	clock_t t = clock();
	_nAgent = 29;
	_nPro = 2;
	_nGen = 8;
	_nCons = 19;
	DELETEA(_agents);
	_agents = new Agent[_nAgent];
	float Plim1[29] = { -146.4, -483, -750, -350.7, -783, -9.8, -12.8, -480, -493.5, -237, -1020, -411, -371.3, -462.9, -336, -208.5, -421.5, -309, -425.3, 0, 0, 0, 0, 0, 0, 0, 0, -13.8, -1656 }; 
	float Plim2[29] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1040, 725, 652, 508, 687, 580, 564, 865, 646, 1100};
	float Cost1[29] = { 67, 47, 47, 53, 82, 52, 87, 57, 50, 52, 71, 64, 57, 82, 69, 69, 86, 54, 78, 89, 55, 82, 88, 76, 84, 77, 51, 74, 73 };
	float Cost2[29] = { 64, 79, 71, 62, 65, 73, 63, 81, 73, 69, 62, 79, 60, 80, 78, 70, 62, 70, 66, 18, 37, 25, 17, 38, 28, 36, 38, 40, 40 };
	float pLim1;
	float pLim2;
	float cost1;
	float cost2;
	_connect = MatrixCPU(_nAgent, _nAgent);
	_a = MatrixCPU(_nAgent, 1);
	_b = MatrixCPU(_nAgent, 1);
	_Ub = MatrixCPU(_nAgent, 1);
	_Lb = MatrixCPU(_nAgent, 1);
	_Pmin = MatrixCPU(_nAgent, 1);
	_Pmax = MatrixCPU(_nAgent, 1);
	_nVoisin = MatrixCPU(_nAgent, 1);
	_BETA = MatrixCPU(_nAgent, _nAgent);
	genConnec(&_connect, _nCons, _nGen, _nPro);
	genBetaUniforme(1);
	int nVoisin;
	
	for (int id = 0; id < _nAgent; id++)
	{
		if (id < _nCons) { // consumer
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id] /1000;
			cost2 = Cost2[id] ;
			nVoisin = _nGen + _nPro;
			
			(_agents[id]).setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 1);
	
			_Ub.set(id, 0, 0);
			_Lb.set(id, 0, pLim1);
			
		}
		else if (id < (_nCons + _nGen)) { // generator
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id] / 1000;
			cost2 = Cost2[id] ;
			nVoisin = _nCons + _nPro;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 2);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, 0);
		}
		else { // prosumer
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id] / 1000;
			cost2 = Cost2[id];
			nVoisin = _nGen + _nCons;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 3);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, pLim1);
		}
		_a.set(id, 0, cost1);
		_b.set(id, 0, cost2);

		_Pmin.set(id, 0, pLim1);
		_Pmax.set(id, 0, pLim2);
		_nVoisin.set(id, 0, nVoisin);
	}
	_nBus = 2;
	_nLine = 0;
	_CoresBusAgent = MatrixCPU(_nBus, _nAgent);

	_LineImpedance = MatrixCPU(_nLine, _nLine);
	_CoresBusLine = MatrixCPU(_nBus, _nLine);
	_SensiPower = MatrixCPU(_nLine, _nAgent); // G = 0 !!!!
	_lineLimits = MatrixCPU(_nLine, 1, 0);
	_nLineConstraint = _nLine;
	_SensiPowerReduce = MatrixCPU(_SensiPower);
	_lineLimitsReduce = MatrixCPU(_lineLimits);
	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}

void StudyCase::Set39Bus(std::string path, bool alreadyDefine)
{
	clock_t t = clock();
	_nAgent = 31;
	_nPro = 0;
	_nGen = 10;
	_nCons = 21;
	DELETEA(_agents);
	_agents = new Agent[_nAgent];
	float Plim1[31] = { -146.4, -483, -750, -350.7, -783, -9.8, -12.8, -480, -493.5, -237, -1020, -411, -371.3, -462.9, -336, -208.5, -421.5, -309, -425.3,-13.8, -1656 , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	float Plim2[31] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1040, 646, 725, 652, 508, 687, 580, 564, 865, 1100 };
	float Cost1[31] = { 67, 47, 47, 53, 82, 52, 87, 57, 50, 52, 71, 64, 57, 82, 69, 69, 86, 54, 78, 81, 59, 89, 67, 55, 82, 88, 76, 84, 77, 51, 87 };
	float Cost2[31] = { 64, 79, 71, 62, 65, 83, 63, 81, 73, 69, 62, 79, 60, 80, 78, 70, 62, 70, 66, 70, 71, 18, 21, 37, 25, 17, 38, 28, 36, 38, 19 };
	int BusAgent[31] = { 1, 3, 4, 7, 8, 9, 12, 15, 16, 18, 20, 21, 23, 24, 25, 26, 27, 28, 29, 31, 39, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39 };
	


	float pLim1;
	float pLim2;
	float cost1;
	float cost2;
	_connect = MatrixCPU(_nAgent, _nAgent);
	_a = MatrixCPU(_nAgent, 1);
	_b = MatrixCPU(_nAgent, 1);
	_Ub = MatrixCPU(_nAgent, 1);
	_Lb = MatrixCPU(_nAgent, 1);
	_Pmin = MatrixCPU(_nAgent, 1);
	_Pmax = MatrixCPU(_nAgent, 1);
	_nVoisin = MatrixCPU(_nAgent, 1);
	_BETA = MatrixCPU(_nAgent, _nAgent);
	genConnec(&_connect, _nCons, _nGen, _nPro);
	int nVoisin;

	for (int id = 0; id < _nAgent; id++)
	{
		if (id < _nCons) { // consumer
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id] / 1000;
			cost2 = Cost2[id];
			nVoisin = _nGen + _nPro;

			(_agents[id]).setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 1);

			_Ub.set(id, 0, 0);
			_Lb.set(id, 0, pLim1);

		}
		else if (id < (_nCons + _nGen)) { // generator
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id] / 1000;
			cost2 = Cost2[id];
			nVoisin = _nCons + _nPro;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 2);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, 0);
		}
		else { // prosumer
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id] / 1000;
			cost2 = Cost2[id];
			nVoisin = _nGen + _nCons;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 3);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, pLim1);
		}
		_a.set(id, 0, cost1);
		_b.set(id, 0, cost2);

		_Pmin.set(id, 0, pLim1);
		_Pmax.set(id, 0, pLim2);
		_nVoisin.set(id, 0, nVoisin);
	}


	// grid 
	_nBus = 39;
	_nLine = 46;
	float zoneBus[39] = { 0, 0, 0, 2, 2, 2, 2, 2, 2, 2,
						  2, 2, 2, 2, 3, 3, 3, 0, 3, 3,
						  3, 3, 3, 3, 0, 1, 1, 1, 1, 0,
						  2, 2, 3, 3, 3, 3, 0, 1, 0 };
	std::string filename = path + "Network39.txt";
	_CoresBusAgent = MatrixCPU(_nBus, _nAgent);
	_LineImpedance = MatrixCPU(_nLine, _nLine); // B
	_CoresBusLine = MatrixCPU(_nBus, _nLine); // C
	_CoresLineBus = MatrixCPU(_nLine, 2); // from, to
	_lineLimits = MatrixCPU(_nLine, 1);
	_SensiPower = MatrixCPU(_nLine, _nAgent); // G
	_zoneBus = MatrixCPU(_nBus, 1);
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	std::string fileName = path + "SensiPower39node.txt";
	std::string fileName2 = path + "lineLimit39node.txt";
	std::string fileName3 = path + "SensiPowerReduce39node.txt";
	std::string fileName4 = path + "lineLimitReduce39node.txt";

	if (alreadyDefine) {
		_nLineConstraint = getNFileline(fileName4);
		_SensiPowerReduce = MatrixCPU(_nLineConstraint, _nAgent);
		_lineLimitsReduce = MatrixCPU(_nLineConstraint, 1);
		_SensiPower.setFromFile(fileName);
		_lineLimits.setFromFile(fileName2);
		_SensiPowerReduce.setFromFile(fileName3);
		_lineLimitsReduce.setFromFile(fileName4);
	}
	else {
		MatrixCPU fileCoresBus(_nBus + 1, 1);
		for (int i = 0; i < _nBus + 1; i++) {
			fileCoresBus.set(i, 0, i - 1);
		}
		setGridFromFile(filename, &fileCoresBus);


		for (int id = 0; id < _nAgent; id++) {
			_CoresBusAgent.set(fileCoresBus.get(BusAgent[id], 0), id, 1);
			_zoneBus.set(id, 0, zoneBus[BusAgent[id]-1]);
		}
		CalcGridSensi();
		_SensiPower.multiply(&_SensiBusLine, &_CoresBusAgent);

		_lineLimitsReduce = MatrixCPU(_nLineConstraint, 1);
		_SensiPowerReduce = MatrixCPU(_nLineConstraint, _nAgent); // Gred
		_CoresLineBusReduce = MatrixCPU(_nLineConstraint, 2);
		int indice = 0;
		for (int i = 0; i < _nLine; i++) {
			int lim = _lineLimits.get(i, 0);
			if (lim != 0) {
				_lineLimitsReduce.set(indice, 0, lim);
				_CoresLineBusReduce.set(indice, 0, _CoresLineBus.get(i, 0));
				_CoresLineBusReduce.set(indice, 1, _CoresLineBus.get(i, 1));
				for (int j = 0; j < _nAgent; j++) {
					_SensiPowerReduce.set(indice, j, _SensiPower.get(i, j));
				}
				indice++;
			}
			else {
				_lineLimits.set(i, 0, LINELIMITMAX);
			}

		}
		_SensiPower.saveCSV(fileName, mode);
		_lineLimits.saveCSV(fileName2, mode);
		_SensiPowerReduce.saveCSV(fileName3, mode);
		_lineLimitsReduce.saveCSV(fileName4, mode);
	}
	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}


void StudyCase::Set3Bus(std::string path) {
	clock_t t = clock();
	_nAgent = 3;
	_nPro = 0;
	_nGen = 2;
	_nCons = 1;
	DELETEA(_agents);
	_agents = new Agent[_nAgent];
	float Plim1[3] = { -1.51, 0, 0};
	float Plim2[3] = { -1.50, 1.00, 2.00};
	float Cost1[3] = { 1000, 0.01, 0.01 }; // on aimerait a = 0 mais on va �viter
	float Cost2[3] = { 1.50, 60, 120};
	float BusAgent[3] = { 0, 1, 2};
	float pLim1;
	float pLim2;
	float cost1;
	float cost2;
	_connect = MatrixCPU(_nAgent, _nAgent);
	_a = MatrixCPU(_nAgent, 1);
	_b = MatrixCPU(_nAgent, 1);
	_Ub = MatrixCPU(_nAgent, 1);
	_Lb = MatrixCPU(_nAgent, 1);
	_Pmin = MatrixCPU(_nAgent, 1);
	_Pmax = MatrixCPU(_nAgent, 1);
	_nVoisin = MatrixCPU(_nAgent, 1);
	_BETA = MatrixCPU(_nAgent, _nAgent);
	genConnec(&_connect, _nCons, _nGen, _nPro);
	genBetaUniforme(0);
	int nVoisin;

	for (int id = 0; id < _nAgent; id++)
	{
		if (id < _nCons) { // consumer
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id] / 1000;
			cost2 = Cost2[id];
			nVoisin = _nGen + _nPro;

			(_agents[id]).setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 1);

			_Ub.set(id, 0, 0);
			_Lb.set(id, 0, pLim1);

		}
		else if (id < (_nCons + _nGen)) { // generator
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id] / 1000;
			cost2 = Cost2[id];
			nVoisin = _nCons + _nPro;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 2);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, 0);
		}
		else { // prosumer
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id] / 1000;
			cost2 = Cost2[id];
			nVoisin = _nGen + _nCons;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 3);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, pLim1);
		}
		_a.set(id, 0, cost1);
		_b.set(id, 0, cost2);

		_Pmin.set(id, 0, pLim1);
		_Pmax.set(id, 0, pLim2);
		_nVoisin.set(id, 0, nVoisin);
	}


	// grid 
	_nBus = 3;
	_nLine = 3;
	std::string filename = path + "Network3.txt";
	_CoresBusAgent = MatrixCPU(_nBus, _nAgent);
	_LineImpedance = MatrixCPU(_nLine, _nLine); // B
	_CoresBusLine = MatrixCPU(_nBus, _nLine); // C
	_CoresLineBus = MatrixCPU(_nLine, 2); // from, to
	_lineLimits = MatrixCPU(_nLine, 1);
	_SensiPower = MatrixCPU(_nLine, _nAgent); // G
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	

	
	MatrixCPU fileCoresBus(_nBus, 1);
	for (int i = 0; i < _nBus; i++) {
		fileCoresBus.set(i, 0, i);
	}
	setGridFromFile(filename, &fileCoresBus);


	for (int id = 0; id < _nAgent; id++) {
		_CoresBusAgent.set(fileCoresBus.get(BusAgent[id], 0), id, 1);
	}
	CalcGridSensi();
	_SensiPower.multiply(&_SensiBusLine, &_CoresBusAgent);

	_lineLimitsReduce = MatrixCPU(_nLineConstraint, 1);
	_SensiPowerReduce = MatrixCPU(_nLineConstraint, _nAgent); // Gred
	int indice = 0;
	for (int i = 0; i < _nLine; i++) {
		float lim = _lineLimits.get(i, 0);
		if (lim != 0) {
			_lineLimitsReduce.set(indice, 0, lim);
			for (int j = 0; j < _nAgent; j++) {
				_SensiPowerReduce.set(indice, j, _SensiPower.get(i, j));
			}
			indice++;
		}
		else {
			_lineLimits.set(i, 0, LINELIMITMAX);
		}

	}
	
	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}

void StudyCase::Set4node()
{
	clock_t t = clock();
	_nAgent = 4;
	_nPro = 1;
	_nGen = 1;
	_nCons = 2;
	DELETEA(_agents);
	_agents = new Agent[_nAgent];
	float Plim1[4] = {-30, -30, 0, -20 };
	float Plim2[4] = { -1, -2, 60, 30 };
	float Cost1[4] = { 1, 1, 0.7, 0.7 };
	float Cost2[4] = { 70, 70, 40, 60 };
	float pLim1;
	float pLim2;
	float cost1;
	float cost2;
	_connect = MatrixCPU(_nAgent, _nAgent);
	_a = MatrixCPU(_nAgent, 1);
	_b = MatrixCPU(_nAgent, 1);
	_Ub = MatrixCPU(_nAgent, 1);
	_Lb = MatrixCPU(_nAgent, 1);
	_Pmin = MatrixCPU(_nAgent, 1);
	_Pmax = MatrixCPU(_nAgent, 1);
	_nVoisin = MatrixCPU(_nAgent, 1);
	_BETA = MatrixCPU(_nAgent, _nAgent);
	genConnec(&_connect, _nCons, _nGen, _nPro);
	genBetaUniforme(1);
	int nVoisin;

	for (int id = 0; id < _nAgent; id++)
	{
		if (id < _nCons) { // consumer
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id];
			cost2 = Cost2[id];
			nVoisin = _nGen + _nPro;

			(_agents[id]).setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 1);

			_Ub.set(id, 0, 0);
			_Lb.set(id, 0, pLim1);

		}
		else if (id < (_nCons + _nGen)) { // generator
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id];
			cost2 = Cost2[id];
			nVoisin = _nCons + _nPro;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 2);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, 0);
		}
		else { // prosumer
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id];
			cost2 = Cost2[id];
			nVoisin = _nGen + _nCons;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 3);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, pLim1);
		}
		_a.set(id, 0, cost1);
		_b.set(id, 0, cost2);

		_Pmin.set(id, 0, pLim1);
		_Pmax.set(id, 0, pLim2);
		_nVoisin.set(id, 0, nVoisin);
	}
	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}

void StudyCase::Set2node() 
{
	clock_t t = clock();
	_nAgent = 2;
	_nPro = 0;
	_nGen = 1;
	_nCons = 1;
	DELETEA(_agents);
	_agents = new Agent[_nAgent];
	float Plim1[2] = { -30, 0 };
	float Plim2[2] = { 0, 60 };
	float Cost1[2] = { 1, 1 };
	float Cost2[2] = { 8, 4 };
	float pLim1;
	float pLim2;
	float cost1;
	float cost2;
	_connect = MatrixCPU(_nAgent, _nAgent);
	_a = MatrixCPU(_nAgent, 1);
	_b = MatrixCPU(_nAgent, 1);
	_Ub = MatrixCPU(_nAgent, 1);
	_Lb = MatrixCPU(_nAgent, 1);
	_Pmin = MatrixCPU(_nAgent, 1);
	_Pmax = MatrixCPU(_nAgent, 1);
	_nVoisin = MatrixCPU(_nAgent, 1);
	_BETA = MatrixCPU(_nAgent, _nAgent);
	genConnec(&_connect, _nCons, _nGen, _nPro);
	genBetaUniforme(1);
	int nVoisin;

	for (int id = 0; id < _nAgent; id++)
	{
		if (id < _nCons) { // consumer
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id];
			cost2 = Cost2[id];
			nVoisin = _nGen + _nPro;

			(_agents[id]).setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 1);

			_Ub.set(id, 0, 0);
			_Lb.set(id, 0, pLim1);

		}
		else if (id < (_nCons + _nGen)) { // generator
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id];
			cost2 = Cost2[id];
			nVoisin = _nCons + _nPro;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 2);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, 0);
		}
		else { // prosumer
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id];
			cost2 = Cost2[id];
			nVoisin = _nGen + _nCons;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 3);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, pLim1);
		}
		_a.set(id, 0, cost1);
		_b.set(id, 0, cost2);

		_Pmin.set(id, 0, pLim1);
		_Pmax.set(id, 0, pLim2);
		_nVoisin.set(id, 0, nVoisin);
	}
	// grid by default : all agent on 2 bus, one infinite line between them (l=1, but b =0)
	_nBus = 2;
	_nLine = 1;
	
	_CoresBusAgent = MatrixCPU(_nBus, _nAgent);

	_LineImpedance = MatrixCPU(_nLine, _nLine);
	_CoresBusLine = MatrixCPU(_nBus, _nLine);
	_CoresLineBus = MatrixCPU(_nLine, 2); // from, to
	_SensiPower = MatrixCPU(_nLine, _nAgent); // G = 0 !!!!
	
	_lineLimits = MatrixCPU(_nLine, 1, 1);

	_nLineConstraint = _nLine;
	_SensiPowerReduce = MatrixCPU(_SensiPower);
	_lineLimitsReduce = MatrixCPU(_lineLimits);
	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}

void StudyCase::Set4nodeBis(std::string path)
{
	// cas d'�tude pour simuler le cas d'EVA pendant son stage
	clock_t t = clock();
	_nAgent = 4;
	_nPro = 0;
	_nGen = 2;
	_nCons = 2;
	DELETEA(_agents);
	_agents = new Agent[_nAgent];
	float Plim1[4] = { -0.8001, -2.001, 0.4 , 0 };
	float Plim2[4] = { -0.8, -2, 10, 7 };
	float Cost1[4] = { 1, 1, 0.0001, 0.0001 };
	float Cost2[4] = { 0.8, 2, 7, 3 };
	float BusAgent[4] = { 1, 2, 0, 1 };
	float pLim1;
	float pLim2;
	float cost1;
	float cost2;
	_connect = MatrixCPU(_nAgent, _nAgent);
	_a = MatrixCPU(_nAgent, 1);
	_b = MatrixCPU(_nAgent, 1);
	_Ub = MatrixCPU(_nAgent, 1);
	_Lb = MatrixCPU(_nAgent, 1);
	_Pmin = MatrixCPU(_nAgent, 1);
	_Pmax = MatrixCPU(_nAgent, 1);
	_nVoisin = MatrixCPU(_nAgent, 1);
	_BETA = MatrixCPU(_nAgent, _nAgent);
	genConnec(&_connect, _nCons, _nGen, _nPro);
	genBetaUniforme(1);
	int nVoisin;

	for (int id = 0; id < _nAgent; id++)
	{
		if (id < _nCons) { // consumer
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id];
			cost2 = Cost2[id];
			nVoisin = _nGen + _nPro;

			(_agents[id]).setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 1);

			_Ub.set(id, 0, 0);
			_Lb.set(id, 0, pLim1);

		}
		else if (id < (_nCons + _nGen)) { // generator
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id];
			cost2 = Cost2[id];
			nVoisin = _nCons + _nPro;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 2);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, 0);
		}
		else { // prosumer
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id];
			cost2 = Cost2[id];
			nVoisin = _nGen + _nCons;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 3);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, pLim1);
		}
		_a.set(id, 0, cost1);
		_b.set(id, 0, cost2);

		_Pmin.set(id, 0, pLim1);
		_Pmax.set(id, 0, pLim2);
		_nVoisin.set(id, 0, nVoisin);
	}
	
	// grid 
	_nBus = 4;
	_nLine = 4;
	std::string filename = path + "Network4.txt";
	_CoresBusAgent = MatrixCPU(_nBus, _nAgent);
	_LineImpedance = MatrixCPU(_nLine, _nLine); // B
	_CoresBusLine = MatrixCPU(_nBus, _nLine); // C
	_CoresLineBus = MatrixCPU(_nLine, 2); // from, to
	_lineLimits = MatrixCPU(_nLine, 1);
	_SensiPower = MatrixCPU(_nLine, _nAgent); // G
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;



	MatrixCPU fileCoresBus(_nBus, 1);
	for (int i = 0; i < _nBus; i++) {
		fileCoresBus.set(i, 0, i);
	}
	setGridFromFile(filename, &fileCoresBus);


	for (int id = 0; id < _nAgent; id++) {
		_CoresBusAgent.set(fileCoresBus.get(BusAgent[id], 0), id, 1);
	}
	CalcGridSensi();
	_SensiPower.multiply(&_SensiBusLine, &_CoresBusAgent);

	_lineLimitsReduce = MatrixCPU(_nLineConstraint, 1);
	_SensiPowerReduce = MatrixCPU(_nLineConstraint, _nAgent); // Gred
	int indice = 0;
	for (int i = 0; i < _nLine; i++) {
		float lim = _lineLimits.get(i, 0);
		if (lim != 0) {
			_lineLimitsReduce.set(indice, 0, lim);
			for (int j = 0; j < _nAgent; j++) {
				_SensiPowerReduce.set(indice, j, _SensiPower.get(i, j));
			}
			indice++;
		}
		else {
			_lineLimits.set(i, 0, LINELIMITMAX);
		}

	}

	

	
	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;


}

void StudyCase::Set2nodeConstraint(float lim)
{
	clock_t t = clock();
	_nAgent = 2;
	_nPro = 0;
	_nGen = 1;
	_nCons = 1;
	DELETEA(_agents);
	_agents = new Agent[_nAgent];
	float Plim1[2] = { -30, 0 };
	float Plim2[2] = { 0, 60 };
	float Cost1[2] = { 1, 1 };
	float Cost2[2] = { 8, 4 };
	float pLim1;
	float pLim2;
	float cost1;
	float cost2;
	_connect = MatrixCPU(_nAgent, _nAgent);
	_a = MatrixCPU(_nAgent, 1);
	_b = MatrixCPU(_nAgent, 1);
	_Ub = MatrixCPU(_nAgent, 1);
	_Lb = MatrixCPU(_nAgent, 1);
	_Pmin = MatrixCPU(_nAgent, 1);
	_Pmax = MatrixCPU(_nAgent, 1);
	_nVoisin = MatrixCPU(_nAgent, 1);
	_BETA = MatrixCPU(_nAgent, _nAgent);
	genConnec(&_connect, _nCons, _nGen, _nPro);
	genBetaUniforme(1);
	int nVoisin;

	for (int id = 0; id < _nAgent; id++)
	{
		if (id < _nCons) { // consumer
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id];
			cost2 = Cost2[id];
			nVoisin = _nGen + _nPro;

			(_agents[id]).setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 1);

			_Ub.set(id, 0, 0);
			_Lb.set(id, 0, pLim1);

		}
		else if (id < (_nCons + _nGen)) { // generator
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id];
			cost2 = Cost2[id];
			nVoisin = _nCons + _nPro;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 2);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, 0);
		}
		else { // prosumer
			pLim1 = Plim1[id];
			pLim2 = Plim2[id];
			cost1 = Cost1[id];
			cost2 = Cost2[id];
			nVoisin = _nGen + _nCons;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 3);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, pLim1);
		}
		_a.set(id, 0, cost1);
		_b.set(id, 0, cost2);

		_Pmin.set(id, 0, pLim1);
		_Pmax.set(id, 0, pLim2);
		_nVoisin.set(id, 0, nVoisin);
	}
	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;

	// grid 
	_nBus = 2;
	_nLine = 1;
	

	_LineImpedance = MatrixCPU(_nLine, _nLine); // B
	_CoresBusLine = MatrixCPU(_nBus, _nLine); // C
	_lineLimits = MatrixCPU(_nLine, 1);
	_SensiPower = MatrixCPU(_nLine, _nAgent); // G
	_zoneBus = MatrixCPU(_nBus, 1);
	_CoresLineBus = MatrixCPU(_nLine, 2); // from, to
	_CoresBusAgent = MatrixCPU(_nBus, _nAgent); // I

	_CoresBusAgent.set(0, 0, 1);
	_CoresBusAgent.set(1, 1, 1);
	
	
	_LineImpedance.set(0, 0, 1); //bii = 1; se simplifie
	


	//CalcGridSensi(); ne marche pas la matrice n'est pas inversible !!!

	_SensiPower.set(0, 1, -1);
	_lineLimits.set(0, 0, lim);

	t = clock() - t;
	_nLineConstraint = _nLine;
	_SensiPowerReduce = MatrixCPU(_SensiPower); 
	_lineLimitsReduce = MatrixCPU(_lineLimits);

}




void StudyCase::SetEuropeP0(const std::string& path, MatrixCPU* P0, bool alreadyDefine)
{
	clock_t t = clock();
	_nGen = 969;
	_nCons = 1494;
	_nAgent = _nGen + _nCons;
	_nPro = 0;
	_name = "Europe";
	float dP = 0.1; // P = P0 +/- dP * P0 for the consumers
	DELETEA(_agents);
	_agents = new Agent[_nAgent];
	_connect = MatrixCPU(_nAgent, _nAgent);
	_a = MatrixCPU(_nAgent, 1);
	_b = MatrixCPU(_nAgent, 1);
	_Ub = MatrixCPU(_nAgent, 1);
	_Lb = MatrixCPU(_nAgent, 1);
	_Pmin = MatrixCPU(_nAgent, 1);
	_Pmax = MatrixCPU(_nAgent, 1);
	_nVoisin = MatrixCPU(_nAgent, 1);
	_BETA = MatrixCPU(_nAgent, _nAgent);

	genConnec(&_connect, _nCons, _nGen, _nPro);
	//genBetaUniforme(0);


	int nVoisin;
	float pLim1;
	float pLim2;
	float cost1;
	float cost2;
	MatrixCPU Pgen(_nGen, 1);
	MatrixCPU Cost(_nGen, 1);
	MatrixCPU GenBus(_nGen, 1);
	std::string pathGen = path + "genCarac.txt";
	setGenFromFile(pathGen, &Pgen, &Cost, &GenBus);
	


	for (int id = 0; id < _nAgent; id++)
	{
		if (id < _nCons) { // consumer

			pLim1 = -(1 + dP) * P0->get(id, 0);
			pLim2 = -(1 - dP) * P0->get(id, 0);
			cost1 = 1;
			cost2 = P0->get(id, 0) * cost1;

			nVoisin = _nGen + _nPro;
			(_agents[id]).setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 1);
			_Ub.set(id, 0, 0);
			_Lb.set(id, 0, pLim1);

		}
		else { // generator

			pLim1 = 0;
			pLim2 = Pgen.get(id - _nCons, 0);
			cost1 = 0.1;
			cost2 = Cost.get(id - _nCons, 0);

			nVoisin = _nCons + _nPro;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 2);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, 0);
		}
		_Pmin.set(id, 0, pLim1);
		_Pmax.set(id, 0, pLim2);
		_a.set(id, 0, cost1);
		_b.set(id, 0, cost2);
		_nVoisin.set(id, 0, nVoisin);

	}

	// grid 
	_nBus = 1494;
	_nLine = 2156;
	
	_LineImpedance = MatrixCPU(_nLine, _nLine); // B
	_CoresBusLine = MatrixCPU(_nBus, _nLine); // C
	_CoresLineBus = MatrixCPU(_nLine, 2); // from, to
	_lineLimits = MatrixCPU(_nLine, 1);
	_SensiPower = MatrixCPU(_nLine, _nAgent); // G
	_zoneBus = MatrixCPU(_nBus, 1);
	_CoresBusAgent = MatrixCPU(_nBus, _nAgent); // I
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	std::string fileName = path + "SensiPowerEurope.txt";
	std::string fileName2 = path + "lineLimitEurope.txt";
	std::string fileName3 = path + "SensiPowerReduceEurope.txt";
	std::string fileName4 = path + "lineLimitReduceEurope.txt";

	if (alreadyDefine) {
		_nLineConstraint = getNFileline(fileName4);
		_SensiPowerReduce = MatrixCPU(_nLineConstraint, _nAgent);
		_lineLimitsReduce = MatrixCPU(_nLineConstraint, 1);
		_SensiPower.setFromFile(fileName);
		_lineLimits.setFromFile(fileName2);
		_SensiPowerReduce.setFromFile(fileName3);
		_lineLimitsReduce.setFromFile(fileName4);
	}
	else {

		std::string pathGrid = path + "Network.txt";
		MatrixCPU fileCoresBus(_nBus, 1);
		std::string pathBus = path + "BusAgent.txt"; // corespondance entre les "idBus" du fichier et celui du code (exemple commence � 0 ou � 1)
		setBusFromFile(pathBus, &fileCoresBus);
		int idBusMax = fileCoresBus.max2();
		MatrixCPU fileBusAgent(idBusMax + 1, 1, -1); // si reste � -1, le bus n'existe pas
		//std::cout << idBusMax << std::endl;
		for (int i = 0; i < _nBus; i++) {
			int bus = fileCoresBus.get(i, 0);
			fileBusAgent.set(bus, 0, i);
		}
		setGridFromFile(pathGrid, &fileBusAgent);
		//std::cout << _nLineConstraint << std::endl;
		for (int i = 0; i < _nAgent; i++) {
			if (i < _nCons) { // le bus correspond directement pour les conso
				int bus = i;
				_CoresBusAgent.set(bus, i, 1);
			}
			else {
				int idGen = i - _nCons;
				int bus = fileBusAgent.get(GenBus.get(idGen, 0), 0);
				_CoresBusAgent.set(bus, i, 1);
			}
		}
		CalcGridSensi();
		_SensiPower.multiply(&_SensiBusLine, &_CoresBusAgent);
		_lineLimitsReduce = MatrixCPU(_nLineConstraint, 1);
		_SensiPowerReduce = MatrixCPU(_nLineConstraint, _nAgent); // Gred
		int indice = 0;
		for (int i = 0; i < _nLine; i++) {
			int lim = _lineLimits.get(i, 0);
			if (lim != 0) {
				_lineLimitsReduce.set(indice, 0, lim);
				for (int j = 0; j < _nAgent; j++) {
					_SensiPowerReduce.set(indice, j, _SensiPower.get(i, j));
				}
				indice++;
			}
			else {
				_lineLimits.set(i, 0, LINELIMITMAX);
			}

		}
		_SensiPower.saveCSV(fileName, mode);
		_lineLimits.saveCSV(fileName2, mode);
		_SensiPowerReduce.saveCSV(fileName3, mode);
		_lineLimitsReduce.saveCSV(fileName4, mode);
	}
	//_SensiPower.display();
	t = clock() - t;
	//_timeInit = (float)t / CLOCKS_PER_SEC;
}

void StudyCase::genBetaUniforme(float beta)
{
	// Cons de 0 � nCons exclus
	// Prod de nCons � nCons+nGen exclus
	// Pro de nCons+nGen � nAgent exclus
	for (int i = 0; i < _nCons; i++) { // conso
		for (int j = _nCons;j < _nAgent;j++) { // le reste
			_BETA.set(i, j, -beta);
		}
	}
	for (int i = _nCons; i < _nCons +_nGen; i++) { // gen
		for (int j = 0; j < _nCons; j++) { // conso
			_BETA.set(i, j, beta);
		}
		for (int j = _nCons + _nGen; j < _nAgent; j++) { // prosumer
			_BETA.set(i, j, beta);
		}
	}
	for (int i = _nCons + _nGen;i < _nAgent; i++) { // prosumer
		for (int j = 0; j < _nCons; j++) { // conso
			_BETA.set(i, j, beta);
		}
		for (int j = _nCons;j < _nCons + _nGen;j++) { // gen
			_BETA.set(i, j, -beta);
		}
	}

}

void StudyCase::genBetaDistance(float s)
{// Cons de 0 � nCons exclus
	// Prod de nCons � nCons+nGen exclus
	// Pro de nCons+nGen � nAgent exclus
	if (_Distance.getNLin() != _nAgent) {
		setDistance();
	}
	for (int i = 0; i < _nCons; i++) { // conso
		for (int j = _nCons; j < _nAgent; j++) { // le reste
			_BETA.set(i, j, -s*_Distance.get(i,j) / 2);
		}
	}
	for (int i = _nCons; i < _nCons + _nGen; i++) { // gen
		for (int j = 0; j < _nCons; j++) { // conso
			_BETA.set(i, j, s * _Distance.get(i, j) / 2);
		}
		for (int j = _nCons + _nGen; j < _nAgent; j++) { // prosumer
			_BETA.set(i, j, s * _Distance.get(i, j) / 2);
		}
	}
	for (int i = _nCons + _nGen; i < _nAgent; i++) { // prosumer
		for (int j = 0; j < _nCons; j++) { // conso
			_BETA.set(i, j, s * _Distance.get(i, j) / 2);
		}
		for (int j = _nCons; j < _nCons + _nGen; j++) { // gen
			_BETA.set(i, j, -s * _Distance.get(i, j) / 2);
		}
	}
}

void StudyCase::genBetaDistanceByZone(MatrixCPU* Mats)
{
	if (_Distance.getNLin() != _nAgent) {
		setDistance();
	}
	// verification que c'est l�gal : 
	int maxZone = _zoneBus.max2();
	if (maxZone >= Mats->getNCol() || maxZone >= Mats->getNLin()) {
		throw std::invalid_argument("Mats has not the a good size");
	}

	for (int i = 0; i < _nCons; i++) { // conso
		for (int j = _nCons; j < _nAgent; j++) { // le reste
			int zonei = _zoneBus.get(i, 0);
			int zonej = _zoneBus.get(j, 0);
			float s = Mats->get(zonei, zonej);
			_BETA.set(i, j, -s * _Distance.get(i, j) / 2);
		}
	}
	for (int i = _nCons; i < _nCons + _nGen; i++) { // gen
		for (int j = 0; j < _nCons; j++) { // conso
			int zonei = _zoneBus.get(i, 0);
			int zonej = _zoneBus.get(j, 0);
			float s = Mats->get(zonei, zonej);
			_BETA.set(i, j, s * _Distance.get(i, j) / 2);
		}
		for (int j = _nCons + _nGen; j < _nAgent; j++) { // prosumer
			int zonei = _zoneBus.get(i, 0);
			int zonej = _zoneBus.get(j, 0);
			float s = Mats->get(zonei, zonej);
			_BETA.set(i, j, s * _Distance.get(i, j) / 2);
		}
	}
	for (int i = _nCons + _nGen; i < _nAgent; i++) { // prosumer
		for (int j = 0; j < _nCons; j++) { // conso
			int zonei = _zoneBus.get(i, 0);
			int zonej = _zoneBus.get(j, 0);
			float s = Mats->get(zonei, zonej);
			_BETA.set(i, j, s * _Distance.get(i, j) / 2);
		}
		for (int j = _nCons; j < _nCons + _nGen; j++) { // gen
			int zonei = _zoneBus.get(i, 0);
			int zonej = _zoneBus.get(j, 0);
			float s = Mats->get(zonei, zonej);
			_BETA.set(i, j, -s * _Distance.get(i, j) / 2);
		}
	}
}

void StudyCase::genGrid(int _nBus, int _nMajorLine, int _minorLine, float ReacMajor, float DeltaReacMajor, float ReacMinor, float DeltaReacMinor, float LlimitMajor, float dLlimitMajor, float LlimitMinor, float dLlimitMinor)
{
	std::cout << "work in progress" << std::endl;
}


void StudyCase::genGridFromFile(std::string path, bool alreadyDefine)
{
	// grid 
	_nBus = 1494;
	_nLine = 2156;
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	std::string fileName = path + "SensiBusLineEurope.txt";
	std::string fileName2 = path + "lineLimitEurope.txt";
	std::string fileName4 = path + "lineLimitReduceEurope.txt";

	_lineLimits = MatrixCPU(_nLine, 1);
	_SensiBusLine = MatrixCPU(_nLine, _nBus);

	if (alreadyDefine) {
		_nLineConstraint = getNFileline(fileName4);
		_lineLimitsReduce = MatrixCPU(_nLineConstraint, 1);
		_SensiBusLine.setFromFile(fileName);
		_lineLimits.setFromFile(fileName2);
		_lineLimitsReduce.setFromFile(fileName4);
	}
	else {
		_LineImpedance = MatrixCPU(_nLine, _nLine); // B
		_CoresBusLine = MatrixCPU(_nBus, _nLine); // C
		_CoresLineBus = MatrixCPU(_nLine, 2); // from, to
		_lineLimits = MatrixCPU(_nLine, 1);
		
		_zoneBus = MatrixCPU(_nBus, 1);
		std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;


		std::string pathGrid = path + "Network.txt";

		MatrixCPU fileCoresBus(_nBus, 1);
		std::string pathBus = path + "BusAgent.txt"; // corespondance entre les "idBus" du fichier et celui du code (exemple commence � 0 ou � 1)


		setBusFromFile(pathBus, &fileCoresBus);

		int idBusMax = fileCoresBus.max2();
		MatrixCPU fileBusAgent(idBusMax + 1, 1, -1); // si reste � -1, le bus n'existe pas

													 //std::cout << idBusMax << std::endl;


		for (int i = 0; i < _nBus; i++) {
			int bus = fileCoresBus.get(i, 0);
			fileBusAgent.set(bus, 0, i);
		}


		setGridFromFile(pathGrid, &fileBusAgent);



		CalcGridSensi();
		_SensiBusLine.saveCSV(fileName, mode);
	}
}

void StudyCase::genAgents(int nAgent, float propCons, float Pconso, float dPconso, float bProd, float dbProd, float Pprod, float dPprod, float Gamma, float dGamma)
{
	clock_t t = clock();
	srand(time(nullptr));
	if (propCons < 0 || (1 - propCons) < 0) {
		throw std::invalid_argument("propCons is a proportion <1 and >0");
	} if (dPconso > Pconso || dbProd > bProd || dPprod > Pprod || dGamma > Gamma) {
		throw std::invalid_argument("variation must be smaller than average");
	}
	if (Pconso < 0) {
		Pconso = - Pconso;
	}
	_nAgent = nAgent;
	_nCons = nAgent * propCons;
	_nPro = 0;
	_nGen = nAgent - _nCons - _nPro;

	_agents = new Agent[nAgent];
	float pLim1;
	float pLim2;
	float cost1;
	float cost2;

	_a = MatrixCPU(nAgent, 1);
	_b = MatrixCPU(nAgent, 1);
	_Ub = MatrixCPU(nAgent, 1);
	_connect = MatrixCPU(nAgent, nAgent);
	_Lb = MatrixCPU(nAgent, 1);
	_Pmin = MatrixCPU(nAgent, 1);
	_Pmax = MatrixCPU(nAgent, 1);
	_nVoisin = MatrixCPU(nAgent, 1);
	_BETA = MatrixCPU(_nAgent, _nAgent);

	genConnec(&_connect, _nCons, _nGen, _nPro);
	float gamma = Gamma + dGamma * 2 * (rand1() - 0.5);
	genBetaUniforme(gamma);

	int nVoisin;

	bool impossible = true;

	while (impossible)
	{
		for (int id = 0; id < nAgent; id++)
		{
			if (id < _nCons) { // consumer
				float P0 = Pconso + dPconso * 2 * (rand1() - 0.5);
				pLim1 = - 1.1 * P0;
				pLim2 = - 1.1 * P0;
				cost1 = 1;
				cost2 = P0;
				nVoisin = _nGen + _nPro;
				_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, nAgent, 1);
				_Ub.set(id, 0, 0);
				_Lb.set(id, 0, pLim1);
			}
			else { // generator
				float P0 = Pprod + dPprod * 2 * (rand1() - 0.5);
				pLim1 = 0;
				pLim2 = P0;
				cost1 = 0.1;
				cost2 = bProd + dbProd * 2 * (rand1() - 0.5);
				nVoisin = _nCons + _nPro;
				_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, nAgent, 2);
				_Ub.set(id, 0, pLim2);
				_Lb.set(id, 0, 0);
			}
			_a.set(id, 0, cost1);
			_b.set(id, 0, cost2);

			_Pmin.set(id, 0, pLim1);
			_Pmax.set(id, 0, pLim2);
			_nVoisin.set(id, 0, nVoisin);
		}
		impossible = false;
	}
	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}

void StudyCase::genLinkGridAgent()
{
	//std::cout << "genLink " << _nBus << " " << _nAgent << std::endl;
	_CoresBusAgent = MatrixCPU(_nBus, _nAgent);
	for (int n = 0; n < _nAgent; n++) {
		int bus = rand() % _nBus;
		_CoresBusAgent.set(bus, n, 1);
	}
	//std::cout << "sensiPower" << _nLine << " " << _nLineConstraint << std::endl;
	_SensiPower = MatrixCPU(_nLine, _nAgent);
	_SensiPower.multiply(&_SensiBusLine, &_CoresBusAgent);
	
	_lineLimitsReduce = MatrixCPU(_nLineConstraint, 1);
	_SensiPowerReduce = MatrixCPU(_nLineConstraint, _nAgent); // Gred

	int indice = 0;
	for (int i = 0; i < _nLine; i++) {
		int lim = _lineLimits.get(i, 0);
		if (lim != 0 && lim != LINELIMITMAX) {
			_lineLimitsReduce.set(indice, 0, lim);
			_indiceLineConstraint.push_back(i);
			for (int j = 0; j < _nAgent; j++) {
				_SensiPowerReduce.set(indice, j, _SensiPower.get(i, j));
			}
			indice++;
		}
		else {
			_indiceLineNonConstraint.push_back(i);
			_lineLimits.set(i, 0, LINELIMITMAX);
		}

	}
		
}

void StudyCase::genLineLimit(int nLine, float limit, float dlLimit)
{
	//std::cout << "genLineLimit" << std::endl;
	if (nLine > _nLine) {
		throw std::invalid_argument("nLine is too big");
	}
	if (_nLine < 0) {
		throw std::invalid_argument("nLine must be positive");
	}
	if (nLine > _nLineConstraint) // doit augmenter le nombre de ligne contrainte
	{
		int dl = nLine - _nLineConstraint;
		for (int i = 0; i < dl; i++) {
			int indice = rand() % (_indiceLineNonConstraint.size());
			int j = _indiceLineNonConstraint[indice];
			_indiceLineNonConstraint.erase(_indiceLineNonConstraint.begin() + indice);
			_indiceLineConstraint.push_back(j);
			float l = limit + 2 * dlLimit*(rand1() - 0.5);
			_lineLimits.set(j, 0, l);
		}

	}
	else { // doit diminuer le nombre de ligne contrainte
		if (nLine == 0) {
			int dl = _nLineConstraint;
			for (int i = 0; i < dl; i++) {
				int j = _indiceLineConstraint[dl - i - 1];
				_indiceLineConstraint.pop_back();
				_indiceLineNonConstraint.push_back(j);
				_lineLimits.set(j, 0, 0);
			}
		}
		else {
			int dl = _nLineConstraint - nLine;
			for (int i = 0; i < dl; i++) {
				//std::cout << "-";
				int indice = rand() % _indiceLineConstraint.size();
				int j = _indiceLineConstraint[indice];
			
				_indiceLineConstraint.erase(_indiceLineConstraint.begin() + indice);
				_indiceLineNonConstraint.push_back(j);
				_lineLimits.set(j, 0, 0);
			}
		}
		
	}
	//std::cout << std::endl;
	_nLineConstraint = _indiceLineConstraint.size();
	_lineLimitsReduce = MatrixCPU(_nLineConstraint, 1);
	_SensiPowerReduce = MatrixCPU(_nLineConstraint, _nAgent); // Gred
	int indice = 0;
	for (int i = 0; i < _nLine; i++) {
		int lim = _lineLimits.get(i, 0);
		if (lim != 0 && lim != LINELIMITMAX) {
			_lineLimitsReduce.set(indice, 0, lim);
			for (int j = 0; j < _nAgent; j++) {
				_SensiPowerReduce.set(indice, j, _SensiPower.get(i, j));
			}
			indice++;
		}
		else {
			_lineLimits.set(i, 0, LINELIMITMAX);
		}
	}
	
}


StudyCase::StudyCase(int nAgent, float P, float dP, float a, float da, float b, float db, float propCons, float propPro)
{
	clock_t t = clock();
	srand(time(nullptr));
	if (propCons < 0 || propPro < 0 || (1 - propCons - propPro) < 0) {
		throw std::invalid_argument("propCons and propPro are proportion <1 and >0");
	}
	_nAgent = nAgent;
	_nCons = nAgent *propCons;
	_nPro = nAgent*propPro;
	_nGen = nAgent - _nCons - _nPro;
	
	_agents = new Agent[nAgent];
	float pLim1;
	float pLim2;
	float cost1;
	float cost2;

	_a = MatrixCPU(nAgent, 1);
	_b = MatrixCPU(nAgent, 1);
	_Ub = MatrixCPU(nAgent, 1);
	_connect = MatrixCPU(nAgent,nAgent);
	_Lb = MatrixCPU(nAgent, 1);
	_Pmin = MatrixCPU(nAgent, 1);
	_Pmax = MatrixCPU(nAgent, 1);
	_nVoisin = MatrixCPU(nAgent, 1);
	_BETA = MatrixCPU(_nAgent, _nAgent);

	genConnec(&_connect, _nCons, _nGen, _nPro);
	genBetaUniforme(0);

	int nVoisin;

	bool impossible = true;

	
	for (int id = 0; id < nAgent; id++)
	{
		if (id < _nCons) { // consumer
			pLim1 = -P + dP * 2 * (rand1() - 0.5);
			pLim2 = -dP * rand1();
			cost1 = a + da * 2 * (rand1() - 0.5);
			cost2 = b + db * 2 * (rand1() - 0.5);
			nVoisin = _nGen + _nPro;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, nAgent, 1);
			_Ub.set(id, 0, 0);
			_Lb.set(id, 0, pLim1);
		}
		else if (id < (_nCons + _nGen)) { // generator
			pLim1 = dP * rand1();
			pLim2 = P + dP * 2 * (rand1() - 0.5);
			cost1 = a + da * 2 * (rand1() - 0.5);
			cost2 = b + db * 2 * (rand1() - 0.5);
			nVoisin = _nCons + _nPro;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, nAgent, 2);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, 0);
		}
		else { // prosumer
			pLim1 = -P + dP * 2 * (rand1() - 0.5);
			pLim2 = P + dP * 2 * (rand1() - 0.5);
			cost1 = a + da * 2 * (rand1() - 0.5);
			cost2 = b + db * 2 * (rand1() - 0.5);
			nVoisin = _nGen + _nCons;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, nAgent, 3);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, pLim1);
		}
		_a.set(id, 0, cost1);
		_b.set(id, 0, cost2);

		_Pmin.set(id, 0, pLim1);
		_Pmax.set(id, 0, pLim2);
		_nVoisin.set(id, 0, nVoisin);
	}
	//
	// grid by default : all agent on 2 bus, one infinite line between them (l=1, but b =0)
	_nBus = 2;
	_nLine = 1;
	//_CoresBusAgent = MatrixCPU(_nBus, nAgent);
	
	//_LineImpedance = MatrixCPU(_nLine, _nLine);
	//_CoresBusLine = MatrixCPU(_nBus, _nLine);
	_SensiPower = MatrixCPU(_nLine, _nAgent); // G = 0 !!!!
	_lineLimits = MatrixCPU(_nLine, 1, 1);
	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;

	
}

StudyCase::StudyCase(int nAgent, float P0, float dP, float b, float db, float propCons)
{
	clock_t t = clock();
	srand(time(nullptr));
	if (propCons < 0  || (1 - propCons) < 0) {
		throw std::invalid_argument("propCons is a proportion <1 and >0");
	}
	_nAgent = nAgent;
	_nCons = nAgent * propCons;
	_nPro = 0;
	_nGen = nAgent - _nCons - _nPro;

	_agents = new Agent[nAgent];
	float pLim1;
	float pLim2;
	float cost1;
	float cost2;

	_a = MatrixCPU(nAgent, 1);
	_b = MatrixCPU(nAgent, 1);
	_Ub = MatrixCPU(nAgent, 1);
	_connect = MatrixCPU(nAgent, nAgent);
	_Lb = MatrixCPU(nAgent, 1);
	_Pmin = MatrixCPU(nAgent, 1);
	_Pmax = MatrixCPU(nAgent, 1);
	_nVoisin = MatrixCPU(nAgent, 1);
	_BETA = MatrixCPU(_nAgent, _nAgent);

	genConnec(&_connect, _nCons, _nGen, _nPro);
	genBetaUniforme(0);

	int nVoisin;

	bool impossible = true;

	while (impossible)
	{
		for (int id = 0; id < nAgent; id++)
		{
			if (id < _nCons) { // consumer
				pLim1 = -P0 - dP * (rand1() + 0.01);
				pLim2 = -P0 + dP * (rand1() + 0.01);
				cost1 = 1;
				cost2 = P0;
				nVoisin = _nGen + _nPro;
				_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, nAgent, 1);
				_Ub.set(id, 0, 0);
				_Lb.set(id, 0, pLim1);
			}
			else { // generator
				pLim1 = 0;
				pLim2 = P0 + dP * 2 * (rand1() - 0.5);
				cost1 = 0.1;
				cost2 = b + db * 2 * (rand1() - 0.5);
				nVoisin = _nCons + _nPro;
				_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, nAgent, 2);
				_Ub.set(id, 0, pLim2);
				_Lb.set(id, 0, 0);
			}
			_a.set(id, 0, cost1);
			_b.set(id, 0, cost2);

			_Pmin.set(id, 0, pLim1);
			_Pmax.set(id, 0, pLim2);
			_nVoisin.set(id, 0, nVoisin);
		}
		impossible = false;
	}
	// grid by default : all agent on 2 bus, one infinite line between them (l=1, but b =0)
	_nBus = 2;
	_nLine = 1;
	//_CoresBusAgent = MatrixCPU(_nBus, nAgent);

	//_LineImpedance = MatrixCPU(_nLine, _nLine);
	//_CoresBusLine = MatrixCPU(_nBus, _nLine);
	_SensiPower = MatrixCPU(_nLine, _nAgent); // G = 0 !!!!
	_lineLimits = MatrixCPU(_nLine, 1, 1);
	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}

StudyCase::StudyCase(const StudyCase& s)
{
	clock_t t = clock();
	_nAgent = s._nAgent;
	_nPro = s._nPro;
	_nGen = s._nGen;
	_nCons = s._nCons;
	_nLine = s._nLine;
	_nBus = s._nBus;
	_nLineConstraint = s._nLineConstraint;
	_lineLimitsReduce = s._lineLimitsReduce;
	_SensiPowerReduce = s._SensiPowerReduce;
	_name = s._name;
	toReduce = s.toReduce;

	_agents = new Agent[_nAgent];
	for (int i = 0;i < _nAgent;i++) {
		_agents[i] = s._agents[i];
	}

	_a = MatrixCPU(s._a);
	_b = MatrixCPU(s._b);
	_Ub = MatrixCPU(s._Ub);
	_connect = MatrixCPU(s._connect);
	_Lb = MatrixCPU(s._Lb);
	_Pmin = MatrixCPU(s._Pmin);
	_Pmax = MatrixCPU(s._Pmax);
	_nVoisin = MatrixCPU(s._nVoisin);
	_BETA = MatrixCPU(s._BETA);
	_CoresBusAgent = MatrixCPU(s._CoresBusAgent); // I
	_LineImpedance = MatrixCPU(s._LineImpedance); // B
	_CoresBusLine = MatrixCPU(s._CoresBusLine); // C
	_SensiPower = MatrixCPU(s._SensiPower); // G
	_lineLimits = MatrixCPU(s._lineLimits); // l

	// min
	_lineLimitsChange = s._lineLimitsChange;
	lineMin = s.lineMin;
	lineoffset = s.lineoffset;

	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
}

StudyCase::StudyCase(std::string fileName)
{
	clock_t t = clock();
	std::ifstream myFile(fileName, std::ios::in);
	std::cout << fileName << std::endl;
	if (myFile)
	{
		myFile >> _nAgent >> _nCons >> _nGen >> _nPro;
		int nbLineToRead = 7 + 2 * _nAgent;
		_agents = new Agent[_nAgent];
		_a = MatrixCPU(_nAgent, 1);
		_b = MatrixCPU(_nAgent, 1);
		_Lb = MatrixCPU(_nAgent, 1);
		_Ub = MatrixCPU(_nAgent, 1);
		_Pmin = MatrixCPU(_nAgent, 1);
		_Pmax = MatrixCPU(_nAgent, 1);
		_nVoisin = MatrixCPU(_nAgent, 1);
		_BETA = MatrixCPU(_nAgent, _nAgent);
		_connect = MatrixCPU(_nAgent, _nAgent);
		float c = 0;

		for (int i = 0; i < nbLineToRead; i++) 
		{
			switch (i)
			{
			case 0:
				for (int j = 0; j < _nAgent;j++) {
					myFile >> c;
					_a.set(j, 0, c);
				}
				break;
			case 1:
				for (int j = 0; j < _nAgent;j++) {
					myFile >> c;
					_b.set(j, 0, c);
				}
				break;
			case 2:
				for (int j = 0; j < _nAgent;j++) {
					myFile >> c;
					_Lb.set(j, 0, c);
				}
				break;
			case 3:
				for (int j = 0; j < _nAgent;j++) {
					myFile >> c;
					_Ub.set(j, 0, c);
				}
				break;
			case 4:
				for (int j = 0; j < _nAgent;j++) {
					myFile >> c;
					_Pmin.set(j, 0, c);
				}
				break;
			case 5:
				for (int j = 0; j < _nAgent;j++) {
					myFile >> c;
					_Pmax.set(j, 0, c);
				}
				break;
			case 6:
				for (int j = 0; j < _nAgent;j++) {
					myFile >> c;
					_nVoisin.set(j, 0, c);
				}
				break;
			default:
				if (i < (6 + _nAgent)) {
					for (int j = 0; j < _nAgent;j++) {
						myFile >> c;
						_BETA.set(i, j, c);
					}
				}
				else {
					for (int j = 0; j < _nAgent;j++) {
						myFile >> c;
						_connect.set(i, j, c);
					}
				}
				break;
			}
			
		}
		myFile.close();

		for (int id = 0; id < _nAgent; id++)
		{
			if (id < _nCons) { // consumer
				_agents[id].setAgent(id, _Pmin.get(id, 0), _Pmax.get(id, 0), _a.get(id, 0), _b.get(id, 0), _nVoisin.get(id, 0), &_connect, _nAgent, 1);
	
			}
			else if (id < (_nCons + _nGen)) { // generator
				_agents[id].setAgent(id, _Pmin.get(id, 0), _Pmax.get(id, 0), _a.get(id, 0), _b.get(id, 0), _nVoisin.get(id, 0), &_connect, _nAgent, 2);
			}
			else { // prosumer
				_agents[id].setAgent(id, _Pmin.get(id, 0), _Pmax.get(id, 0), _a.get(id, 0), _b.get(id, 0), _nVoisin.get(id, 0), &_connect, _nAgent, 3);
			}
		}
		// grid by default : all agent on 2 bus, one infinite line between them (l=1, but b =0)
		_nBus = 2;
		_nLine = 1;
		//_CoresBusAgent = MatrixCPU(_nBus, nAgent);

		//_LineImpedance = MatrixCPU(_nLine, _nLine);
		//_CoresBusLine = MatrixCPU(_nBus, _nLine);
		_SensiPower = MatrixCPU(_nLine, _nAgent); // G = 0 !!!!
		_lineLimits = MatrixCPU(_nLine, 1, 1);
	}
	else {
		throw std::invalid_argument("can't open this file");
	}
	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;

}

void StudyCase::SetStudyCase(std::string path, std::string name, MatrixCPU* P0, bool alreadyDefine)
{
	// recuperation des tailles des fichiers
	std::string pathGen = path + "genCarac" + name + ".txt";
	std::string fileName = path + "SensiPower" + name + ".txt";
	std::string fileName2 = path + "lineLimit" + name + ".txt";
	std::string fileName3 = path + "SensiPowerReduce" + name + ".txt";
	std::string fileName4 = path + "lineLimitReduce" + name + ".txt";
	
	_name = name;
	
	

	_nGen = getNFileline(pathGen);
	_nCons = P0->getNLin();
	
	clock_t t = clock();

	_nAgent = _nGen + _nCons;
	_nPro = 0;

	float dP = 0.1; // P = P0 +/- dP * P0 for the consumers
	DELETEA(_agents);
	_agents = new Agent[_nAgent];
	_connect = MatrixCPU(_nAgent, _nAgent);
	_a = MatrixCPU(_nAgent, 1);
	_b = MatrixCPU(_nAgent, 1);
	_Ub = MatrixCPU(_nAgent, 1);
	_Lb = MatrixCPU(_nAgent, 1);
	_Pmin = MatrixCPU(_nAgent, 1);
	_Pmax = MatrixCPU(_nAgent, 1);
	_nVoisin = MatrixCPU(_nAgent, 1);
	_BETA = MatrixCPU(_nAgent, _nAgent);

	genConnec(&_connect, _nCons, _nGen, _nPro);
	genBetaUniforme(0);


	int nVoisin;
	float pLim1;
	float pLim2;
	float cost1;
	float cost2;
	MatrixCPU Pgen(_nGen, 1);
	MatrixCPU Cost(_nGen, 1);
	MatrixCPU GenBus(_nGen, 1);
	
	setGenFromFile(pathGen, &Pgen, &Cost, &GenBus);

	for (int id = 0; id < _nAgent; id++)
	{
		if (id < _nCons) { // consumer

			pLim1 = -(1 + dP) * P0->get(id, 0);
			pLim2 = -(1 - dP) * P0->get(id, 0);
			cost1 = 1;
			cost2 = P0->get(id, 0) * cost1;

			nVoisin = _nGen + _nPro;
			(_agents[id]).setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 1);
			_Ub.set(id, 0, 0);
			_Lb.set(id, 0, pLim1);

		}
		else { // generator

			pLim1 = 0;
			pLim2 = Pgen.get(id - _nCons, 0);
			cost1 = 0.1;
			cost2 = Cost.get(id - _nCons, 0);

			nVoisin = _nCons + _nPro;
			_agents[id].setAgent(id, pLim1, pLim2, cost1, cost2, nVoisin, &_connect, _nAgent, 2);
			_Ub.set(id, 0, pLim2);
			_Lb.set(id, 0, 0);
		}
		_Pmin.set(id, 0, pLim1);
		_Pmax.set(id, 0, pLim2);
		_a.set(id, 0, cost1);
		_b.set(id, 0, cost2);
		_nVoisin.set(id, 0, nVoisin);

	}

	// grid 
	_nBus = _nCons;
	std::string pathGrid = path + "Network" + name + ".txt";
	_nLine = getNFileline(pathGrid);

	_LineImpedance = MatrixCPU(_nLine, _nLine); // B
	_CoresBusLine = MatrixCPU(_nBus, _nLine); // C
	_lineLimits = MatrixCPU(_nLine, 1);
	_SensiPower = MatrixCPU(_nLine, _nAgent); // G
	_zoneBus = MatrixCPU(_nBus, 1);
	_CoresBusAgent = MatrixCPU(_nBus, _nAgent); // I
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	

	if (alreadyDefine) {
		_nLineConstraint = getNFileline(fileName4);
		_SensiPowerReduce = MatrixCPU(_nLineConstraint, _nAgent);
		_lineLimitsReduce = MatrixCPU(_nLineConstraint, 1);
		_SensiPower.setFromFile(fileName);
		_lineLimits.setFromFile(fileName2);
		_SensiPowerReduce.setFromFile(fileName3);
		_lineLimitsReduce.setFromFile(fileName4);
	}
	else {

		std::string pathGrid = path + "Network" + name + ".txt";
		MatrixCPU fileCoresBus(_nBus, 1);
		std::string pathBus = path + "BusAgent" + name + ".txt"; // corespondance entre les "idBus" du fichier et celui du code (exemple commence � 0 ou � 1)
		setBusFromFile(pathBus, &fileCoresBus);
		int idBusMax = fileCoresBus.max2();
		MatrixCPU fileBusAgent(idBusMax + 1, 1, -1); // si reste � -1, le bus n'existe pas
		//std::cout << idBusMax << std::endl;
		for (int i = 0; i < _nBus; i++) {
			int bus = fileCoresBus.get(i, 0);
			fileBusAgent.set(bus, 0, i);
		}
		setGridFromFile(pathGrid, &fileBusAgent);
		std::cout << _nLineConstraint << std::endl;
		for (int i = 0; i < _nAgent; i++) {
			if (i < _nCons) { // le bus correspond directement pour les conso
				int bus = i;
				_CoresBusAgent.set(bus, i, 1);
			}
			else {
				int idGen = i - _nCons;
				int bus = fileBusAgent.get(GenBus.get(idGen, 0), 0);
				_CoresBusAgent.set(bus, i, 1);
			}
		}
		CalcGridSensi();
		_SensiPower.multiply(&_SensiBusLine, &_CoresBusAgent);
		_lineLimitsReduce = MatrixCPU(_nLineConstraint, 1);
		_SensiPowerReduce = MatrixCPU(_nLineConstraint, _nAgent); // Gred
		int indice = 0;
		for (int i = 0; i < _nLine; i++) {
			int lim = _lineLimits.get(i, 0);
			if (lim != 0) {
				_lineLimitsReduce.set(indice, 0, lim);
				for (int j = 0; j < _nAgent; j++) {
					_SensiPowerReduce.set(indice, j, _SensiPower.get(i, j));
				}
				indice++;
			}
			else {
				_lineLimits.set(i, 0, LINELIMITMAX);
			}

		}
		_SensiPower.saveCSV(fileName, mode);
		_lineLimits.saveCSV(fileName2, mode);
		_SensiPowerReduce.saveCSV(fileName3, mode);
		_lineLimitsReduce.saveCSV(fileName4, mode);
	}
	//_SensiPower.display();
	t = clock() - t;
	//_timeInit = (float)t / CLOCKS_PER_SEC;
}

StudyCase& StudyCase::operator= (const StudyCase& s) 
{
	clock_t t = clock();
	_nAgent = s._nAgent;
	_nPro = s._nPro;
	_nGen = s._nGen;
	_nCons = s._nCons;
	_nLine = s._nLine;
	_nBus = s._nBus;
	
	_name = s._name;


	DELETEA(_agents);
	_agents = new Agent[_nAgent];
	for (int i = 0;i < _nAgent;i++) {
		_agents[i] = s._agents[i];
	}

	_a = s._a;
	_b = s._b;
	_Ub = s._Ub;
	_connect = s._connect;
	_Lb = s._Lb;
	_Pmin = s._Pmin;
	_Pmax = s._Pmax;
	_nVoisin = s._nVoisin;
	_BETA = s._BETA;
	
	_CoresBusAgent = s._CoresBusAgent; // I
	_LineImpedance = s._LineImpedance; // B
	_CoresBusLine = s._CoresBusLine;// C
	_SensiPower = s._SensiPower; // G
	_lineLimits = s._lineLimits; // l

	// reduce
	_nLineConstraint = s._nLineConstraint;
	_lineLimitsReduce = s._lineLimitsReduce;
	_SensiPowerReduce = s._SensiPowerReduce;
	toReduce = s.toReduce;

	// min
	_lineLimitsChange = s._lineLimitsChange;
	lineMin = s.lineMin;
	lineoffset = s.lineoffset;

	t = clock() - t;
	_timeInit = (float)t / CLOCKS_PER_SEC;
	return *this;
}

void StudyCase::UpdateP0(MatrixCPU* P0) 
{

	float dP = 0.1; // P = P0 +/- dP * P0 for the consumers
	
	if (P0->getNLin() != _nCons) {
		throw std::invalid_argument("P0 hasn't the good number of column");
	}
	for (int id = 0; id < _nCons; id++)
	{
		_agents[id].updateP0(P0->get(id, 0), dP);
		_Pmin.set(id, 0, _agents[id].getPmin());
		_Pmax.set(id, 0, _agents[id].getPmax());
		_a.set(id, 0, _agents[id].getA());
		_b.set(id, 0, _agents[id].getB());
		_Ub.set(id, 0, 0);
		_Lb.set(id, 0, _agents[id].getLb());
	}
	
}

MatrixCPU StudyCase::getBeta() const
{
	return _BETA;
}

MatrixCPU StudyCase::getC() const
{
	return _connect;
}

MatrixCPU StudyCase::geta() const
{
	return _a;
}

MatrixCPU StudyCase::getb() const
{
	return _b;
}

MatrixCPU StudyCase::getUb() const
{
	return _Ub;
}

MatrixCPU StudyCase::getLb() const
{
	return _Lb;
}

MatrixCPU StudyCase::getPmin() const
{
	return _Pmin;
}

MatrixCPU StudyCase::getPmax() const
{
	return _Pmax;
}

MatrixCPU StudyCase::getNvoi() const
{
	return _nVoisin;
}

MatrixCPU StudyCase::getPowerSensi() const
{
	if (toReduce) {
		return _SensiPowerReduce;
	}
	else {
		return _SensiPower;
	}
	
}

MatrixCPU StudyCase::getLineLimit() const
{
	if (lineMin || lineoffset) {
		return _lineLimitsChange;
	} 
	else if (toReduce) {
		return _lineLimitsReduce;
	}
	else {
		return _lineLimits;
	}
	
}

MatrixCPU StudyCase::getCoresLineBus() const
{
	if (toReduce) {
		return _CoresLineBusReduce;
	}
	else {
		return _CoresLineBus;
	}
}

float StudyCase::getTimeInit() const
{
	return _timeInit;
}

int StudyCase::getNagent() const
{
	return _nAgent;
}

int StudyCase::getNCons() const
{
	return _nCons;
}

int StudyCase::getNLine() const
{
	if (toReduce) {
		return _nLineConstraint;
	}
	else {
		return _nLine;
	}
	
}

int StudyCase::getNBus() const
{
	return _nBus;
}

MatrixCPU StudyCase::getVoisin(int agent) const
{
	if (agent > _nAgent) {
		throw std::invalid_argument("this agent doesn't exists");
	}
	return _agents[agent].getVoisin();
}
Agent StudyCase::getAgent(int agent) const
{
	return _agents[agent];
}

std::string StudyCase::getName() const
{
	return _name;
}

void StudyCase::removeLink(int i, int j)
{
	
	if (i>=_nAgent && j>=_nAgent) {
		throw std::invalid_argument("indice out of range");
	}
	if (_connect.get(i, j) == 0) {
		
		throw std::invalid_argument("agent already not linked"); 
	}
	_connect.set(i, j, 0);
	_connect.set(j, i, 0);


	_nVoisin.set(i, 0, _nVoisin.get(i, 0) - 1);
	_nVoisin.set(j, 0, _nVoisin.get(j, 0) - 1);

	int type = 3;

	if (i < _nCons) {
		type = 1;
	} 
	else if (i >= (_nCons + _nGen)) {
		type = 2;
	}
	_agents[i].setAgent(i, _Pmin.get(i, 0), _Pmax.get(i, 0), _a.get(i, 0), _b.get(i, 0), _nVoisin.get(i, 0), &_connect, _nAgent, type);
	type = 3;
	
	if (j < _nCons) {
		type = 1;
	}
	else if (j >= (_nCons + _nGen)) {
		type = 2;
	}
	_agents[j].setAgent(j, _Pmin.get(j, 0), _Pmax.get(j, 0), _a.get(j, 0), _b.get(j, 0), _nVoisin.get(j, 0), &_connect, _nAgent, type);

}


void StudyCase::addLink(int i, int j)
{
	//std::cout << "link " << i << " " << j << std::endl;
	if (i >= _nAgent && j >= _nAgent) {
		throw std::invalid_argument("indice out of range");
	}
	if (i == j) {
		throw std::invalid_argument("agent must not be link to itself");
	}
	if (_connect.get(i, j) == 1) {
		throw std::invalid_argument("agent already linked");
	}
	int type[2] = { 3 , 3 };
	if (i < _nCons) {
		type[0] = 1;
	}
	else if (i >= (_nCons + _nGen)) {
		type[0] = 2;
	}
	if (j < _nCons) {
		type[1] = 1;
	}
	else if (j >= (_nCons + _nGen)) {
		type[1] = 2;
	}
	if (type[0] == type[1]) {
		throw std::invalid_argument("agent must not be the same type");
	}

	_connect.set(i, j, 1);
	_connect.set(j, i, 1);


	_nVoisin.set(i, 0, _nVoisin.get(i, 0) + 1);
	_nVoisin.set(j, 0, _nVoisin.get(j, 0) + 1);

	_agents[i].setAgent(i, _Pmin.get(i, 0), _Pmax.get(i, 0), _a.get(i, 0), _b.get(i, 0), _nVoisin.get(i, 0), &_connect, _nAgent, type[0]);
	_agents[j].setAgent(j, _Pmin.get(j, 0), _Pmax.get(j, 0), _a.get(j, 0), _b.get(j, 0), _nVoisin.get(j, 0), &_connect, _nAgent, type[1]);

}

void StudyCase::setLineLimit(int line, float limit)
{
	if (line > _nLine) {
		throw std::invalid_argument("this line doesn't exist");
	}
	else {
		float oldLimit = _lineLimits.get(line, 0);
		if (oldLimit == LINELIMITMAX) {
			_nLineConstraint++;
		}
		_lineLimits.set(line, 0, limit);
		_lineLimitsReduce = MatrixCPU(_nLineConstraint, 1);
		_SensiPowerReduce = MatrixCPU(_nLineConstraint, _nAgent); // Gred
		_CoresLineBusReduce = MatrixCPU(_nLineConstraint, 2);
		int indice = 0;
		for (int i = 0; i < _nLine; i++) {
			int lim = _lineLimits.get(i, 0);
			if (lim != 0 && lim != LINELIMITMAX) {
				_lineLimitsReduce.set(indice, 0, lim);
				_CoresLineBusReduce.set(indice, 0, _CoresLineBus.get(i, 0));
				_CoresLineBusReduce.set(indice, 1, _CoresLineBus.get(i, 1));
				for (int j = 0; j < _nAgent; j++) {
					_SensiPowerReduce.set(indice, j, _SensiPower.get(i, j));
				}
				indice++;
			}
			else {
				_lineLimits.set(i, 0, LINELIMITMAX);
			}
		}
	}
}

Agent StudyCase::removeAgent(int agent)
{
	if (agent > _nAgent || agent<0) {
		throw std::invalid_argument("this agent doesn't exist");
	}
	Agent agentRemove(_agents[agent]);
	MatrixCPU omega(getVoisin(agent));
	int Nvoisinmax = _nVoisin.get(agent, 0);
	for (int voisin = 0; voisin < Nvoisinmax; voisin++) {
		int idVoisin = omega.get(voisin, 0);
		removeLink(agent, idVoisin);
	}

	_Pmin.set(agent, 0, 0);
	_Pmax.set(agent, 0, 0);
	int type =  3;
	if (agent < _nCons) {
		type = 1;
	}
	else if (agent >= (_nCons + _nGen)) {
		type = 2;
	}
	_agents[agent].setAgent(agent, _Pmin.get(agent, 0), _Pmax.get(agent, 0), _a.get(agent, 0), _b.get(agent, 0), _nVoisin.get(agent, 0), &_connect, _nAgent, type);

	return agentRemove;

}

void StudyCase::restoreAgent(Agent& agent, bool all) {


	int id = agent.getId();
	float Pmin = agent.getPmin();
	float Pmax = agent.getPmax();
	int type = agent.getType();

	_Pmin.set(id, 0, Pmin);
	_Pmax.set(id, 0, Pmax);
	if (!all) {
		std::cout << "Beware restore agent without giving it neighbor can lead to a non solvable problem" << std::endl;
	}
	else {
		MatrixCPU omega(agent.getVoisin());
		int nVoisin = agent.getNVoisin();
		for (int i = 0;i < nVoisin;i++) {
			int idVoisin = omega.get(i, 0);
			addLink(id, idVoisin);
		}
	}
	_agents[id].setAgent(id, _Pmin.get(id, 0), _Pmax.get(id, 0), _a.get(id, 0), _b.get(id, 0), _nVoisin.get(id, 0), &_connect, _nAgent, type);
}

void StudyCase::saveCSV(const std::string& fileName, bool all)
{

	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	MatrixCPU nombre(1, 7);
	nombre.set(0, 0, _nAgent);
	nombre.set(0, 1, _nCons);
	nombre.set(0, 2, _nGen);
	nombre.set(0, 3, _nPro);
	nombre.set(0, 4, _nLine);
	nombre.set(0, 5, _nLineConstraint);
	nombre.set(0, 6, _nBus);
	nombre.saveCSV(fileName, mode);


	MatrixCPU temp(1, _nAgent);
	MatrixCPU zero(1, _nAgent);
	temp.addTrans(&_a);
	temp.saveCSV(fileName, mode);
	temp.set(&zero);
	temp.addTrans(&_b);
	temp.saveCSV(fileName, mode);
	temp.set(&zero);
	temp.addTrans(&_Lb);
	temp.saveCSV(fileName, mode);
	temp.set(&zero);
	temp.addTrans(&_Ub);
	temp.saveCSV(fileName, mode);
	temp.set(&zero);
	temp.addTrans(&_Pmin);
	temp.saveCSV(fileName, mode);
	temp.set(&zero);
	temp.addTrans(&_Pmax);
	temp.saveCSV(fileName, mode);
	temp.set(&zero);
	temp.addTrans(&_nVoisin);
	temp.saveCSV(fileName, mode);
	temp.set(&zero);

	MatrixCPU temp2(1, _nLineConstraint);
	temp2.addTrans(&_lineLimitsReduce);
	temp2.saveCSV(fileName, mode);
	_SensiPowerReduce.saveCSV(fileName, mode);

	if (all) {
		_BETA.saveCSV(fileName, mode);
		_connect.saveCSV(fileName, mode);
	}


}

void StudyCase::display(int type) 
{
	if (type == 0) {
		std::cout << "Study Case : " << _nAgent << " agents " << std::endl;
		//_connect.display();
		std::cout << " a :" << std::endl;
		_a.display();
		std::cout << " b :" << std::endl;
		_b.display();
		std::cout << " lower bound: " << std::endl;
		_Lb.display();
		std::cout << " uper bound: " << std::endl;
		_Ub.display();
		std::cout << " Pmin :" << std::endl;
		_Pmin.display();
		std::cout << " Pmax :" << std::endl;
		_Pmax.display();
		std::cout << " Peers count :" << std::endl;
		_nVoisin.display();
		std::cout << " Line limit :" << std::endl;
		_lineLimitsReduce.display();
		std::cout << " Sensibility :" << std::endl;
		_SensiPowerReduce.display();
		std::cout << "end case " << std::endl;
	}
	else if (type == 1) {
		
		std::cout << " B :" << std::endl;
		_LineImpedance.display();
		std::cout << " C :" << std::endl;
		_CoresBusLine.display();


		MatrixCPU temp1(_nLine, _nBus); // BC^T 
		MatrixCPU temp2(_nBus, _nBus); // CBC^T
		MatrixCPU temp33(_nBus - 1, _nBus - 1); //(CBC ^ T) ^ -1 sans la ligne et colonne du noued de ref
		MatrixCPU temp22(_nBus - 1, _nBus - 1); // on enl�ve la ligne du noeud de ref�rence


		

		temp1.multiplyTrans(&_LineImpedance, &_CoresBusLine);
		temp2.multiply(&_CoresBusLine, &temp1);
		temp2.getBloc(&temp22, 1, _nBus, 1, _nBus);
		temp33.invertEigen(&temp22);
		std::cout << " BC^T :" << std::endl;
		temp1.display();
		std::cout << " CBC^T :" << std::endl;
		temp2.display();
		std::cout << " (CBC^T)^-1 :" << std::endl;
		temp33.display();
		std::cout << " CoresBusAgent:" << std::endl;
		_CoresBusAgent.display();
		std::cout << " Line limit :" << std::endl;
		_lineLimits.display();
		std::cout << " Sensibility :" << std::endl;
		_SensiPower.display();

		std::cout << " Pr�f�rence (beta) :" << std::endl;
		_BETA.display();
		
	}
	
}

void StudyCase::displayLineCores(MatrixCPU* g, bool all)
{
	std::cout << "Line correspendance : " << std::endl;
	
	MatrixCPU Cores(getCoresLineBus());
	//Cores.display();
	
	MatrixCPU Limit(getLineLimit());
	//Limit.display();
	if (all) {
		if (Cores.getNLin() == 0) {
			for (int l = 0; l < getNLine(); l++) {
				if (fabs(g->get(l, 0)) > Limit.get(l, 0)) {
					std::cout << "*******Limites depasse : Line n " << l << " line limit " << Limit.get(l, 0)
						<< " flow " << g->get(l, 0) << "********" << std::endl;
				}
				else if (Limit.get(l, 0) - fabs(g->get(l, 0)) < 0.1) {
					std::cout << "+++Limites proche : Line n " << l << " line limit " << Limit.get(l, 0)
						<< " flow " << g->get(l, 0) << "+++" << std::endl;
				}
				else {
					std::cout << "Line n " << l << " line limit " << Limit.get(l, 0)
						<< " flow " << g->get(l, 0) << std::endl;
				}
				
			}
		}
		else {
			for (int l = 0; l < getNLine(); l++) {
				if (fabs(g->get(l, 0)) > Limit.get(l, 0)) {
					std::cout << "*********Limites depasse :Line n " << l << " from node " << Cores.get(l, 0)
						<< " to node " << Cores.get(l, 1) << " line limit " << Limit.get(l, 0)
						<< " flow " << g->get(l, 0) << "********" << std::endl;
				}
				else if (Limit.get(l, 0) - fabs(g->get(l, 0)) < 0.1) {
					std::cout << "+++Limites proche :Line n " << l << " from node " << Cores.get(l, 0)
						<< " to node " << Cores.get(l, 1) << " line limit " << Limit.get(l, 0)
						<< " flow " << g->get(l, 0) << std::endl;
				}
				else {
					std::cout << "Line n " << l << " from node " << Cores.get(l, 0)
						<< " to node " << Cores.get(l, 1) << " line limit " << Limit.get(l, 0)
						<< " flow " << g->get(l, 0) << std::endl;
				}
				
			}
		}
	}
	else {
		if (Cores.getNLin() == 0) {
			for (int l = 0; l < getNLine(); l++) {
				if (fabs(g->get(l, 0)) > Limit.get(l, 0)) {
					std::cout << "*********Limites depasse : Line n " << l << " line limit " << Limit.get(l, 0)
						<< " flow " << g->get(l, 0) << "*********"<< std::endl;
				}
				else if (Limit.get(l, 0) - fabs(g->get(l, 0)) < 0.1) {
					std::cout << "+++Limites proche : Line n " << l << " line limit " << Limit.get(l, 0)
						<< " flow " << g->get(l, 0) << "+++" << std::endl;
				}
				
			}
		}
		else {
			for (int l = 0; l < getNLine(); l++) {
				if (fabs(g->get(l, 0)) > Limit.get(l, 0)) {
					std::cout << "*********Limites depass� :Line n " << l << " from node " << Cores.get(l, 0)
						<< " to node " << Cores.get(l, 1) << " line limit " << Limit.get(l, 0)
						<< " flow " << g->get(l, 0) << "*********" << std::endl;
				}
				else if (Limit.get(l, 0) - fabs(g->get(l, 0)) < 0.1) {
					std::cout << "+++Limites proche :Line n " << l << " from node " << Cores.get(l, 0)
						<< " to node " << Cores.get(l, 1) << " line limit " << Limit.get(l, 0)
						<< " flow " << g->get(l, 0) << std::endl;
				}
				
			}
		}
	}
	
	
}

StudyCase::~StudyCase()
{
#ifdef DEBUG_DESTRUCTOR
	std::cout << "case destructor" << std::endl;
#endif

	DELETEA(_agents);

}


