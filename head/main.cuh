#pragma once

#include "MatrixCPU.h"
#include "MatrixGPU.cuh"
#include "StudyCase.h"
#include "Simparam.h"
#include "System.h"


#include <stdio.h>
#include <iostream>
#include <time.h>
#include <cuda_runtime.h>
#include <osqp.h>
#include <cudaProfiler.h>

// fichier de test
#include "TestMatrixCPU.h"
#include "TestAgent.h"
#include "TestStudyCase.h"
#include "TestADMMConst.h"
#include "TestADMMConst1.h"

#include "TestADMMGPUConst1.cuh"
#include "TestADMMGPUConst1T.cuh"
#include "TestADMMGPUConst2.cuh"
#include "TestADMMGPUConst3.cuh"
#include "TestSimparam.h"
#include "TestSysteme.h"
#include "TestMatrixGPU.cuh"
#include "TestMatrixCPU.h"
#include "TestKernel.cuh"





// test on the global and local step impact
void SimulationTempStepOpti();

void comparaisonArticle();
void testExemple();
void blague();

// Performance test, alea case or europeen case.
void SimuTemporal();

void SimuTemporal(std::string name);
void SimuTemporalRho(std::string name = "Europe");
void SimuTemporalLlimit(std::string name = "Europe");
void SimuTemporalConvergence(std::string name = "Europe");
void SimuCompare();
void SimuStatPowerTech();
void SimuStat();
void SimuStatADMMGPU();


void testADMMGPUtemp();
std::string generateDate(int year, int month, int day, int hour);

float pow10(int n);

void getErrorSensiPwerLine();

/*
On ne peut inclure qu'un seul osqp � la fois, (car m�me nom)
C:\Program Files \OSQP\osqp\include\osqp


*/