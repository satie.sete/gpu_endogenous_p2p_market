#pragma once
#include "../head/Method.h"
#define MAX(X, Y) X * (X >= Y) + Y * (Y > X)


Method::Method()
{
#if DEBUG_CONSTRUCTOR
	std::cout << "method constructor" << std::endl;
#endif // DEBUG_CONSTRUCTOR
	timePerBlock = MatrixCPU(1, 11, 0); // Fb0, Fb1abc, Fb2, Fb3, Fb4, Fb5, Fb0'
	// si les sous ensemble ne sont pas accessible, tout est dans le premier.
	occurencePerBlock = MatrixCPU(1, 11, 0); //nb de fois utilis� pendant la simu
	
}

Method::~Method()
{
}



float Method::calcFc(MatrixCPU* cost1, MatrixCPU* cost2, MatrixCPU* trade, MatrixCPU* Pn, MatrixCPU* BETA, MatrixCPU* tempN1, MatrixCPU* tempNN)
{

	float fc = 0;
	tempN1->set(cost1);
	tempN1->multiply(0.5);
	tempN1->multiplyT(Pn);
	tempN1->add(cost2);
	tempN1->multiplyT(Pn);
	
	fc = fc + tempN1->sum();
	tempNN->set(trade);
	tempNN->multiplyT(BETA);
	

	fc = fc + tempNN->sum();

	std::cout << "fc " << fc << std::endl;

	return fc;

}

float Method::calcFc(MatrixCPU* cost1, MatrixCPU* cost2, MatrixCPU* Pn, MatrixCPU* tempN1)
{
	float fc = 0;
	tempN1->set(cost1);
	tempN1->multiply(0.5);
	tempN1->multiplyT(Pn);
	tempN1->add(cost2);
	tempN1->multiplyT(Pn);

	return tempN1->sum();
}

float Method::calcFc(MatrixGPU* cost1, MatrixGPU* cost2, MatrixGPU* trade, MatrixGPU* Pn, MatrixGPU* BETA, MatrixGPU* tempN1, MatrixGPU* tempNN)
{
	
	tempN1->set(cost1);
	
	tempN1->multiply(0.5);
	tempN1->multiplyT(Pn);
	
	tempN1->add(cost2);
	
	tempN1->multiplyT(Pn);
	
	float fc = tempN1->sum();
	

	tempNN->set(trade);
	
	tempNN->multiplyT(BETA);
	
	fc = fc + tempNN->sum();



	//std::cout << "fc " << fc << std::endl;
	return fc;

}



void Method::resetId()
{
	_id = 0;
}


