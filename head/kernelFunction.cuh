#pragma once
#include <cuda_runtime.h>
#include "MatrixGPU.cuh"

#define NSTEPLOCAL 5
#define NMAXPEERPERTRHREAD 5
#define MAX(X, Y) X * (X >= Y) + Y * (Y > X)

////IMPLEMENTATION APPEL KERNEL avec template

template <unsigned int blockSize>
__global__ void updateTradePGPULocal(float* Tlocal, float* Tlocal_pre, float* Tmoy, float* P, float* MU, float* nVoisin, float at1, float at2, float* Bt1, float* Ct,
	float* matLb, float* matUb, float* Ap1, float* Ap12, float* Cp, float* Pmin, float* Pmax, float* CoresAgentLin) {

	//Definition de toutes les variables locales
	int i = blockIdx.x; // c'est aussi l'identifiant de l'agent !
	unsigned int thIdx = threadIdx.x;
	const int step = blockSize;
	// ne change pas
	const float Ap1Local = Ap1[i];
	const float CpLocal = Cp[i];
	const float Ap12Local = Ap12[i];
	const float Pub = Pmax[i];
	const float Plb = Pmin[i];
	const int nVoisinLocal = nVoisin[i];
	const int CoresAgentLinLocal = CoresAgentLin[i];
	const int beginLocal = CoresAgentLinLocal + thIdx;
	const int endLocal = CoresAgentLinLocal + nVoisinLocal;

	const float at1local = at1;
	const float at2local = at2;
	const float at12local = at1local + at2local;


	float Bt1local[NMAXPEERPERTRHREAD];
	float Ctlocal[NMAXPEERPERTRHREAD];
	float matUblocal[NMAXPEERPERTRHREAD];
	float matLblocal[NMAXPEERPERTRHREAD];

	float Tlocallocal[NMAXPEERPERTRHREAD]; // change
	float Tlocalprelocal[NMAXPEERPERTRHREAD]; // change
	float MULOCAL;
	float moy;
	float p;
	float sum;
	float bp;
	float m, r, ub, lb, t;
	// le changement doit �tre partag� par tous les threads du bloc

	__shared__ float MuShared;
	__shared__ float TMoyShared;
	__shared__ float PShared;
	if (thIdx == 0) {
		MuShared = MU[i];
		TMoyShared = Tmoy[i];
		PShared = P[i];
	}
	int k = 0;
	for (int j = beginLocal; j < endLocal; j += step) {
		Bt1local[k] = Bt1[j];
		Ctlocal[k] = Ct[j];
		matUblocal[k] = matUb[j];
		matLblocal[k] = matLb[j];
		//Tlocalprelocal[k] = Tlocal_pre[j];
		Tlocallocal[k] = Tlocal_pre[j];
		k = k + 1;
	}

	__shared__ float shArr[blockSize];


	//Calcul des it�rations
	__syncthreads();
	for (int iter = 0; iter < NSTEPLOCAL; iter++) {

		MULOCAL = MuShared; // tous lisent le m�me : broadcast !
		moy = TMoyShared;
		p = PShared;
		sum = 0;
		k = 0;
		for (int j = beginLocal; j < endLocal; j += step) {
			Tlocalprelocal[k] = Tlocallocal[k];
			m = Tlocallocal[k] - moy + p - MULOCAL;
			r = (Bt1local[k] * at1local + m * at2local - Ctlocal[k]) / (at12local);
			ub = matUblocal[k];
			lb = matLblocal[k];
			t = (ub - r) * (r > ub) + (lb - r) * (r < lb) + r;
			Tlocallocal[k] = t;
			sum += t;
			k = k + 1;
		}

		shArr[thIdx] = sum;
		__syncthreads();
		if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
		if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
		if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
		if (thIdx < 32) {
			warpReduce<blockSize>(shArr, thIdx);
		}
		__syncthreads();

		if (thIdx == 0) {
			moy = shArr[0] / nVoisinLocal;
			TMoyShared = moy;
			bp = moy + MuShared;
			p = (Ap1Local * bp - CpLocal) / Ap12Local;
			p = (Pub - p) * (p > Pub) + (Plb - p) * (p < Plb) + p;
			PShared = p;
			MuShared = MULOCAL + moy - p;
		}
		__syncthreads();
	}
	//Ecriture des it�rations
	__syncthreads();
	k = 0;
	for (int j = beginLocal; j < endLocal; j += step) {
		Tlocal[j] = Tlocallocal[k];
		Tlocal_pre[j] = Tlocalprelocal[k];
		k = k + 1;
	}
	if (thIdx == 0) {
		Tmoy[blockIdx.x] = TMoyShared;// TMoyShared;
		P[blockIdx.x] = PShared;// PShared;
		MU[blockIdx.x] = MuShared;// MuShared;
	}

}

template <unsigned int blockSize>
__global__ void updateTradePGPU(float* Tlocal, float* Tlocal_pre, float* Tmoy, float* P, float* MU, float* nVoisin, float at1, float at2, float* Bt1, float* Ct,
	float* matLb, float* matUb, float* Ap1, float* Ap12, float* Cp, float* Pmin, float* Pmax, float* CoresAgentLin, float* CoresLinAgent, int const n) {

	__shared__ float shArr[blockSize];
	unsigned int thIdx = threadIdx.x;
	const int step = blockSize;
	float sum = 0;
	int i = blockIdx.x;
	int nVoisinLocal = nVoisin[i];
	int beginLocal = CoresAgentLin[i];
	int endLocal = beginLocal + nVoisinLocal; // = CoresAgentLin[blockIdx.x + 1]

	for (int j = thIdx + beginLocal; j < endLocal; j += step) {
		float m = Tlocal_pre[j] - Tmoy[i] + P[i] - MU[i]; // hum trouver la valeur de Tlocal_pre ?
		float r = (Bt1[j] * at1 + m * at2 - Ct[j]) / (at1 + at2); // Bt1 et Ct ne change pas entre 2 iterations, comment les garder ?
		float ub = matUb[j]; //idem
		float lb = matLb[j]; //idem
		float t = (ub - r) * (r > ub) + (lb - r) * (r < lb) + r;
		Tlocal[j] = t; // ne le faire que la derni�re it�ration
		sum += t;
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduce<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		float moy = shArr[0] / nVoisinLocal;
		Tmoy[blockIdx.x] = moy;
		float muLocal = MU[blockIdx.x];
		float b = moy + muLocal;
		float p = (Ap1[blockIdx.x] * b - Cp[blockIdx.x]) / Ap12[blockIdx.x];
		float Pub = Pmax[blockIdx.x];
		float Plb = Pmin[blockIdx.x];
		p = (Pub - p) * (p > Pub) + (Plb - p) * (p < Plb) + p;
		P[blockIdx.x] = p;
		MU[blockIdx.x] = muLocal + moy - p;
	}

}

template <unsigned int blockSize>
__global__ void updateTradePGPUShared(float* Tlocal, float* Tlocal_pre, float* Tmoy, float* P, float* MU, float* nVoisin, float at1, float at2, float* Bt1, float* Ct,
	float* matLb, float* matUb, float* Ap1, float* Ap12, float* Cp, float* Pmin, float* Pmax, float* CoresAgentLin) {

	//Definition de toutes les variables locales
	int i = blockIdx.x; // c'est aussi l'identifiant de l'agent !
	unsigned int thIdx = threadIdx.x;
	const int step = blockSize;
	// ne change pas


	float Bt1local[NMAXPEERPERTRHREAD];
	float Ctlocal[NMAXPEERPERTRHREAD];
	float matUblocal[NMAXPEERPERTRHREAD];
	float matLblocal[NMAXPEERPERTRHREAD];

	float Tlocallocal[NMAXPEERPERTRHREAD]; // change
	float Tlocalprelocal[NMAXPEERPERTRHREAD]; // change
	float sum;
	float bp, MULOCAL, moy, p;
	float m, r, ub, lb, t;
	// le changement doit �tre partag� par tous les threads du bloc

	__shared__ float MuShared;
	__shared__ float TMoyShared;
	__shared__ float PShared;


	// constant et commun � tous les thread d'un bloc
	__shared__ float Ap1Shared;
	__shared__ float CpShared;
	__shared__ float Ap12Shared;
	__shared__ float PmaxShared;
	__shared__ float PminShared;
	__shared__ float nVoisinShared;
	__shared__ float at1Shared;
	__shared__ float at2Shared;
	__shared__ float at12Shared;


	if (thIdx == 0) {
		Ap1Shared = Ap1[i];
		CpShared = Cp[i];
		Ap12Shared = Ap12[i];
		PmaxShared = Pmax[i];
		PminShared = Pmin[i];
		nVoisinShared = nVoisin[i];
		at1Shared = at1;
		at2Shared = at2;
		at12Shared = at1 + at2;
		MuShared = MU[i];
		TMoyShared = Tmoy[i];
		PShared = P[i];
	}
	int k = 0;
	__syncthreads();
	const int CoresAgentLinLocal = CoresAgentLin[i];
	const int beginLocal = CoresAgentLinLocal + thIdx;
	const int endLocal = CoresAgentLinLocal + nVoisinShared;
	for (int j = beginLocal; j < endLocal; j += step) {
		Bt1local[k] = Bt1[j];
		Ctlocal[k] = Ct[j];
		matUblocal[k] = matUb[j];
		matLblocal[k] = matLb[j];
		//Tlocalprelocal[k] = Tlocal_pre[j];
		Tlocallocal[k] = Tlocal_pre[j];
		k = k + 1;
	}

	__shared__ float shArr[blockSize];

	//Calcul des it�rations

	for (int iter = 0; iter < NSTEPLOCAL; iter++) {

		MULOCAL = MuShared; // tous lisent le m�me : broadcast !
		moy = TMoyShared;
		p = PShared;
		sum = 0;
		k = 0;
		for (int j = beginLocal; j < endLocal; j += step) {
			Tlocalprelocal[k] = Tlocallocal[k];
			m = Tlocallocal[k] - moy + p - MULOCAL;
			r = (Bt1local[k] * at1Shared + m * at2Shared - Ctlocal[k]) / (at12Shared);
			ub = matUblocal[k];
			lb = matLblocal[k];
			t = (ub - r) * (r > ub) + (lb - r) * (r < lb) + r;
			Tlocallocal[k] = t;
			sum += t;
			k = k + 1;
		}

		shArr[thIdx] = sum;
		__syncthreads();
		if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
		if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
		if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
		if (thIdx < 32) {
			warpReduce<blockSize>(shArr, thIdx);
		}
		__syncthreads();

		if (thIdx == 0) {
			moy = shArr[0] / nVoisinShared;
			TMoyShared = moy;
			bp = moy + MuShared;
			p = (Ap1Shared * bp - CpShared) / Ap12Shared;
			p = (PmaxShared - p) * (p > PmaxShared) + (PminShared - p) * (p < PminShared) + p;
			PShared = p;
			MuShared = MULOCAL + moy - p;
		}
		__syncthreads();
	}
	//Ecriture des it�rations
	__syncthreads();
	k = 0;
	for (int j = beginLocal; j < endLocal; j += step) {
		Tlocal[j] = Tlocallocal[k];
		Tlocal_pre[j] = Tlocalprelocal[k];
		k = k + 1;
	}
	if (thIdx == 0) {
		Tmoy[blockIdx.x] = TMoyShared;// TMoyShared;
		P[blockIdx.x] = PShared;// PShared;
		MU[blockIdx.x] = MuShared;// MuShared;
	}

}


template <unsigned int blockSize>
__global__ void updateTradePGPUSharedResidual(float* Tlocal, float* Tlocal_pre, float* Tmoy, float* P, float* MU, float* nVoisin, float at1, float at2, float* Bt1, float* Ct,
	float* matLb, float* matUb, float* Ap1, float* Ap12, float* Cp, float* Pmin, float* Pmax, float* CoresAgentLin, float eps, int nStepL) {

	//Definition de toutes les variables locales
	int i = blockIdx.x; // c'est aussi l'identifiant de l'agent !
	unsigned int thIdx = threadIdx.x;
	const int step = blockSize;
	// ne change pas


	float Bt1local[NMAXPEERPERTRHREAD];
	float Ctlocal[NMAXPEERPERTRHREAD];
	float matUblocal[NMAXPEERPERTRHREAD];
	float matLblocal[NMAXPEERPERTRHREAD];

	float Tlocallocal[NMAXPEERPERTRHREAD]; // change
	float Tlocalprelocal[NMAXPEERPERTRHREAD]; // change
	float sum;
	float bp, MULOCAL, moy, p;
	float m, r, ub, lb, t;
	// le changement doit �tre partag� par tous les threads du bloc

	__shared__ float MuShared;
	__shared__ float TMoyShared;
	__shared__ float PShared;


	// constant et commun � tous les thread d'un bloc
	__shared__ float Ap1Shared;
	__shared__ float CpShared;
	__shared__ float Ap12Shared;
	__shared__ float PmaxShared;
	__shared__ float PminShared;
	__shared__ float nVoisinShared;
	__shared__ float at1Shared;
	__shared__ float at2Shared;
	__shared__ float at12Shared;
	__shared__ bool mustContinue;


	if (thIdx == 0) {
		Ap1Shared = Ap1[i];
		CpShared = Cp[i];
		Ap12Shared = Ap12[i];
		PmaxShared = Pmax[i];
		PminShared = Pmin[i];
		nVoisinShared = nVoisin[i];
		at1Shared = at1;
		at2Shared = at2;
		at12Shared = at1 + at2;
		MuShared = MU[i];
		TMoyShared = Tmoy[i];
		PShared = P[i];
		mustContinue = false;
	}
	int k = 0;
	__syncthreads();
	const int CoresAgentLinLocal = CoresAgentLin[i];
	const int beginLocal = CoresAgentLinLocal + thIdx;
	const int endLocal = CoresAgentLinLocal + nVoisinShared;
	float res;
	for (int j = beginLocal; j < endLocal; j += step) {
		Bt1local[k] = Bt1[j];
		Ctlocal[k] = Ct[j];
		matUblocal[k] = matUb[j];
		matLblocal[k] = matLb[j];
		//Tlocalprelocal[k] = Tlocal_pre[j];
		Tlocallocal[k] = Tlocal_pre[j];
		k = k + 1;
	}

	__shared__ float shArr[blockSize];

	//Calcul des it�rations

	for (int iter = 0; iter < nStepL; iter++) {

		MULOCAL = MuShared; // tous lisent le m�me : broadcast !
		moy = TMoyShared;
		p = PShared;
		sum = 0;
		k = 0;
		for (int j = beginLocal; j < endLocal; j += step) {
			Tlocalprelocal[k] = Tlocallocal[k];
			m = Tlocallocal[k] - moy + p - MULOCAL;
			r = (Bt1local[k] * at1Shared + m * at2Shared - Ctlocal[k]) / (at12Shared);
			ub = matUblocal[k];
			lb = matLblocal[k];
			t = (ub - r) * (r > ub) + (lb - r) * (r < lb) + r;
			Tlocallocal[k] = t;
			sum += t;
			res = (t - Tlocalprelocal[k]);
			res = (double) res*res;
			if (res > eps) {
				mustContinue = true; // pas de race condition, car l'ordre n'importe pas,
				//mais est ce que cela ne va pas physiquement bloquer ?
			}
			k = k + 1;
		}

		shArr[thIdx] = sum;
		__syncthreads();
		if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
		if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
		if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
		if (thIdx < 32) {
			warpReduce<blockSize>(shArr, thIdx);
		}
		__syncthreads();

		if (thIdx == 0) {
			moy = shArr[0] / nVoisinShared;
			TMoyShared = moy;
			bp = moy + MuShared;
			p = (Ap1Shared * bp - CpShared) / Ap12Shared;
			p = (PmaxShared - p) * (p > PmaxShared) + (PminShared - p) * (p < PminShared) + p;
			PShared = p;
			res = p - moy;
			res = (double) res* res;
			if (res > eps) {
				mustContinue = true;
			}
			MuShared = MULOCAL + moy - p;
		}
		__syncthreads();
		if (!mustContinue) {
			break;
		}
		else {
			__syncthreads();
			if (thIdx == 0) {
				mustContinue = false;
			}
		}
	}
	//Ecriture des it�rations
	__syncthreads();
	k = 0;
	for (int j = beginLocal; j < endLocal; j += step) {
		Tlocal[j] = Tlocallocal[k];
		Tlocal_pre[j] = Tlocalprelocal[k];
		k = k + 1;
	}
	if (thIdx == 0) {
		Tmoy[blockIdx.x] = TMoyShared;// TMoyShared;
		P[blockIdx.x] = PShared;// PShared;
		MU[blockIdx.x] = MuShared;// MuShared;
	}

}




template <unsigned int blockSize>
__global__ void updateTradePGPUSharedResidualCons(float* Tlocal, float* Tlocal_pre, float* Tmoy, float* P, float* MU, float* nVoisin, float at1, float at2, float* Bt1, float* Ct,
	float* matLb, float* matUb, float* Ap1, float* Ap3, float* Ap123, float* Bp3, float* Cp, float* Pmin, float* Pmax, float* CoresAgentLin, float eps, int nStepL) {

	//Definition de toutes les variables locales
	int i = blockIdx.x; // c'est aussi l'identifiant de l'agent !
	unsigned int thIdx = threadIdx.x;
	const int step = blockSize;
	// ne change pas


	float Bt1local[NMAXPEERPERTRHREAD];
	float Ctlocal[NMAXPEERPERTRHREAD];
	float matUblocal[NMAXPEERPERTRHREAD];
	float matLblocal[NMAXPEERPERTRHREAD];

	float Tlocallocal[NMAXPEERPERTRHREAD]; // change
	float Tlocalprelocal[NMAXPEERPERTRHREAD]; // change
	float sum;
	float bp, MULOCAL, moy, p;
	float m, r, ub, lb, t;
	// le changement doit �tre partag� par tous les threads du bloc

	__shared__ float MuShared;
	__shared__ float TMoyShared;
	__shared__ float PShared;


	// constant et commun � tous les thread d'un bloc
	__shared__ float Ap1Shared;
	__shared__ float Ap3Shared;
	__shared__ float Bp3Shared;
	__shared__ float CpShared;
	__shared__ float Ap123Shared;
	__shared__ float PmaxShared;
	__shared__ float PminShared;
	__shared__ float nVoisinShared;
	__shared__ float at1Shared;
	__shared__ float at2Shared;
	__shared__ float at12Shared;
	__shared__ bool mustContinue;


	if (thIdx == 0) {
		Ap1Shared = Ap1[i];
		CpShared = Cp[i];
		Ap3Shared = Ap3[i];
		Bp3Shared = Bp3[i];
		Ap123Shared = Ap123[i];
		PmaxShared = Pmax[i];
		PminShared = Pmin[i];
		nVoisinShared = nVoisin[i];
		at1Shared = at1;
		at2Shared = at2;
		at12Shared = at1 + at2;
		MuShared = MU[i];
		TMoyShared = Tmoy[i];
		PShared = P[i];
		mustContinue = false;
	}
	int k = 0;
	__syncthreads();
	const int CoresAgentLinLocal = CoresAgentLin[i];
	const int beginLocal = CoresAgentLinLocal + thIdx;
	const int endLocal = CoresAgentLinLocal + nVoisinShared;
	float res;
	for (int j = beginLocal; j < endLocal; j += step) {
		Bt1local[k] = Bt1[j];
		Ctlocal[k] = Ct[j];
		matUblocal[k] = matUb[j];
		matLblocal[k] = matLb[j];
		//Tlocalprelocal[k] = Tlocal_pre[j];
		Tlocallocal[k] = Tlocal_pre[j];
		k = k + 1;
	}

	__shared__ float shArr[blockSize];

	//Calcul des it�rations

	for (int iter = 0; iter < nStepL; iter++) {

		MULOCAL = MuShared; // tous lisent le m�me : broadcast !
		moy = TMoyShared;
		p = PShared;
		sum = 0;
		k = 0;
		for (int j = beginLocal; j < endLocal; j += step) {
			Tlocalprelocal[k] = Tlocallocal[k];
			m = Tlocallocal[k] - moy + p - MULOCAL;
			r = (Bt1local[k] * at1Shared + m * at2Shared - Ctlocal[k]) / (at12Shared);
			ub = matUblocal[k];
			lb = matLblocal[k];
			t = (ub - r) * (r > ub) + (lb - r) * (r < lb) + r;
			Tlocallocal[k] = t;
			sum += t;
			res = (t - Tlocalprelocal[k]);
			res = (double)res * res;
			if (res > eps) {
				mustContinue = true; // pas de race condition, car l'ordre n'importe pas,
				//mais est ce que cela ne va pas physiquement bloquer ?
			}
			k = k + 1;
		}

		shArr[thIdx] = sum;
		__syncthreads();
		if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
		if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
		if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
		if (thIdx < 32) {
			warpReduce<blockSize>(shArr, thIdx);
		}
		__syncthreads();

		if (thIdx == 0) {
			moy = shArr[0] / nVoisinShared;
			TMoyShared = moy;
			bp = moy + MuShared;
			p = (Ap1Shared * bp + Ap3Shared * Bp3Shared - CpShared) / Ap123Shared;
			p = (PmaxShared - p) * (p > PmaxShared) + (PminShared - p) * (p < PminShared) + p;
			PShared = p;
			res = p - moy;
			res = (double)res * res;
			if (res > eps) {
				mustContinue = true;
			}
			MuShared = MULOCAL + moy - p;
		}
		__syncthreads();
		if (!mustContinue) {
			break;
		}
		else {
			__syncthreads();
			if (thIdx == 0) {
				mustContinue = false;
			}
		}
	}
	//Ecriture des it�rations
	__syncthreads();
	k = 0;
	for (int j = beginLocal; j < endLocal; j += step) {
		Tlocal[j] = Tlocallocal[k];
		Tlocal_pre[j] = Tlocalprelocal[k];
		k = k + 1;
	}
	if (thIdx == 0) {
		Tmoy[blockIdx.x] = TMoyShared;// TMoyShared;
		P[blockIdx.x] = PShared;// PShared;
		MU[blockIdx.x] = MuShared;// MuShared;
	}

}




template <unsigned int blockSize>
__global__ void updateCp2b(float* tempN1, float* G, float* Qpart, const int nLine, const int nAgent)
{
	// un bloc par agent
	int thIdx = threadIdx.x;
	int step = blockDim.x;
	int n = blockIdx.x;
	float sum = 0;
	__shared__ float shArr[blockSize];
	for (int j = thIdx; j < nLine; j += step) {

		float t = G[j * nAgent + n] * Qpart[j * nAgent + n];
		sum += t;
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduce<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		tempN1[n] = 2 * shArr[0];
	}

}

template <unsigned int blockSize>
__global__ void updateCp2bTrans(float* tempN1, float* G, float* Qpart, const int nLine, const int nAgent)
{
	// un bloc par agent
	int thIdx = threadIdx.x;
	int step = blockDim.x;
	int n = blockIdx.x;
	float sum = 0;
	__shared__ float shArr[blockSize];
	for (int j = thIdx; j < nLine; j += step) {

		float t = G[n * nLine + j] * Qpart[n * nLine + j];
		sum += t;
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduce<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		tempN1[n] = 2 * shArr[0];
	}

}


template <unsigned int blockSize>
__global__ void updateCp2a(float* Cp2, float* diffKappa, float* G, const int nLine, const int nAgent) {
	// un bloc par agent
	int thIdx = threadIdx.x;
	int step = blockDim.x;
	int n = blockIdx.x;
	float sum = 0;
	__shared__ float shArr[blockSize];
	for (int j = thIdx; j < nLine; j += step) {

		float t = G[j * nAgent + n] * diffKappa[j];
		sum += t;
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduce<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		Cp2[n] = shArr[0];
	}
}

template <unsigned int blockSize>
__global__ void updateCp2aTrans(float* Cp2, float* diffKappa, float* G, const int nLine, const int nAgent) {
	// un bloc par agent
	int thIdx = threadIdx.x;
	int step = blockDim.x;
	int n = blockIdx.x;
	float sum = 0;
	__shared__ float shArr[blockSize];
	for (int j = thIdx; j < nLine; j += step) {

		float t = G[n * nLine + j] * diffKappa[j];
		sum += t;
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduce<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		Cp2[n] = shArr[0];
	}
}


template <unsigned int blockSize>
__global__ void updateCp2GPU(float* Cp2, float* diffKappa, float* G, float* Qpart, float* nVoisin, float rho1, const int nLine, const int nAgent) {
	// un bloc par agent
	int thIdx = threadIdx.x;
	int step = blockDim.x;
	int n = blockIdx.x;
	float sum = 0;
	__shared__ float shArr[blockSize];
	for (int j = thIdx; j < nLine; j += step) {

		float t = G[j * nAgent + n] * (diffKappa[j] + 2 * Qpart[j * nAgent + n]);
		sum += t;
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduce<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		Cp2[n] = rho1 * nVoisin[n] * shArr[0];
	}
}

template <unsigned int blockSize>
__global__ void updateCp2GPUTrans(float* Cp2, float* diffKappa, float* G, float* Qpart, float* nVoisin, float rho1, const int nLine, const int nAgent) {
	// un bloc par agent
	int thIdx = threadIdx.x;
	int step = blockDim.x;
	int n = blockIdx.x;
	float sum = 0;
	__shared__ float shArr[blockSize];
	for (int j = thIdx; j < nLine; j += step) {
		float Gloc = G[n * nLine + j];
		float dKloc = diffKappa[j];
		float Q = Qpart[n * nLine + j];
		float t = Gloc * (dKloc + 2 * Q);
		sum += t;
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduce<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		Cp2[n] = rho1 * nVoisin[n] * shArr[0];
	}
}



/*
template <unsigned int blockSize>
__global__ void updateG(float* g, float* Aiq, float* Pso, float* Biq, int N) {

	// un bloc par ligne
	int thIdx = threadIdx.x;
	int step = blockDim.x;
	int l = blockIdx.x;
	float sum = 0;
	__shared__ float shArr[blockSize];
	for (int n = thIdx; n < N; n += step) {

		float t = Aiq[l * N + n] * Pso[n];
		sum += t;
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduce<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		g[l] = shArr[0] - Biq[l];
	}

}

template <unsigned int blockSize>
__global__ void updateErr1(float* err1, float* g, float* u, int L2) {

	// mono block
	int thIdx = threadIdx.x;
	int step = blockDim.x;
	float sum = 0;
	__shared__ float shArr[blockSize];
	for (int i = thIdx; i < L2; i += step) {

		float t = -g[i] * u[i];
		sum += t;
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduce<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		*err1 = shArr[0];
	}


}
// Rx = H*Pn + q + AiqTrans * u


template <unsigned int blockSize>
__global__ void updateRx1(float* Rx, float* H, float* Pn, float* q, int N) {

	// un bloc par ligne
	int thIdx = threadIdx.x;
	int step = blockDim.x;
	int n = blockIdx.x;
	float sum = 0;
	__shared__ float shArr[blockSize];
	for (int i = thIdx; i < N; i += step) {

		float t = H[n * N + i] * Pn[i];
		sum += t;
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduce<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		Rx[n] = q[n] +  shArr[0];
	}

}
template <unsigned int blockSize>
__global__ void updateRx2(float* Rx, float* AiqTrans, float* u, float* v, int L2) {

	// un bloc par ligne
	int thIdx = threadIdx.x;
	int step = blockDim.x;
	int n = blockIdx.x;
	float sum = 0;
	__shared__ float shArr[blockSize];
	for (int i = thIdx; i < L2; i += step) {

		float t = AiqTrans[n * L2 + i] * u[i];
		sum += t;
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduce<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		Rx[n] = shArr[0] + Rx[n] + *v;
	}

}

template <unsigned int blockSize>
__global__ void updateRv(float* Rv, float* Pso, int offset, int N) {

	// mono-block
	int thIdx = threadIdx.x;
	int step = blockDim.x;
	
	float sum = 0;
	__shared__ float shArr[blockSize];
	for (int i = thIdx; i < N; i += step) {

		sum += Pso[i];
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduce<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		Rv[offset] = shArr[0];
	}

}

template <unsigned int blockSize>
__global__ void updateRv2(float* Rv, float* Pso, float _epsi, int offset, int N) {

	// mono-block
	int thIdx = threadIdx.x;
	int step = blockDim.x;

	float sum = 0;
	__shared__ float shArr[blockSize];
	for (int i = thIdx; i < N; i += step) {

		sum += Pso[i];
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduce<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		Rv[offset] = Rv[offset] * _epsi + shArr[0];
	}

}
//resolution de pas = -(M ^ -1) * Rxu




template <unsigned int blockSize>
__global__ void updatePas(float* pas, float* Minv, float* Rxu, int M) {

	// un bloc par ligne
	int thIdx = threadIdx.x;
	int step = blockDim.x;
	int m = blockIdx.x;
	float sum = 0;
	__shared__ float shArr[blockSize];
	for (int i = thIdx; i < M; i += step) {

		float t = -Minv[m * M + i] * Rxu[i];
		sum += t;
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduce<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		pas[m] = shArr[0];
	}
}*/


template <unsigned int blockSize>
__global__ void updateAPas(float* Apas, float* A, float* pas, int N) {

	// un bloc par ligne
	int thIdx = threadIdx.x;
	int step = blockDim.x;
	int l = blockIdx.x;
	float sum = 0;
	__shared__ float shArr[blockSize];
	for (int i = thIdx; i < N; i += step) {

		float t = A[l * N + i] * pas[i];
		sum += t;
	}

	shArr[thIdx] = sum;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { shArr[thIdx] += shArr[thIdx + 256]; } __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) { shArr[thIdx] += shArr[thIdx + 128]; } __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) { shArr[thIdx] += shArr[thIdx + 64]; } __syncthreads(); }
	if (thIdx < 32) {
		warpReduce<blockSize>(shArr, thIdx);
	}

	if (thIdx == 0) {
		Apas[l] = shArr[0];
	}
}

template <unsigned int blockSize>
__global__ void updateAlpha(float* alpha, float* u, float* pas, float* c, float* Apas, int N, int L2) {

	// mono-block
	int thIdx = threadIdx.x;
	int step = blockDim.x;
	int m = blockIdx.x;
	float min = 1;
	__shared__ float sdata[blockSize];
	for (int i = thIdx; i < L2; i += step) {

		float t =  pas[i + N] < 0 ? (- u[i] / pas[i+N]) : 1;
		min = min < t ? min : t;
		t = Apas[i + N] < 0 ? -(c[i] / Apas[i]) : 1;
		min = min < t ? min : t;
	}

	sdata[thIdx] = min;
	__syncthreads();

	if (blockSize >= 512) { if (thIdx < 256) { 
		sdata[thIdx] = sdata[thIdx] < sdata[thIdx + 256] ? sdata[thIdx] : sdata[thIdx + 256];
	} __syncthreads(); }
	if (blockSize >= 256) { if (thIdx < 128) {
		sdata[thIdx] = sdata[thIdx] < sdata[thIdx + 128] ? sdata[thIdx] : sdata[thIdx + 128];
	} __syncthreads(); }
	if (blockSize >= 128) { if (thIdx < 64) {
		sdata[thIdx] = sdata[thIdx] < sdata[thIdx +  64] ? sdata[thIdx] : sdata[thIdx +  64];
	} __syncthreads(); }
	if (thIdx < 32) {
		warpReduceMin<blockSize>(sdata, thIdx);
	}

	if (thIdx == 0) {
		*alpha = 0.9 * sdata[0];
	}
}



