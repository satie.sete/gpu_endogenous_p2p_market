# GPU_Endogenous_P2P_Market

This repository gathers codes illustrating the algorithms presented in the conference article "GPU Optimisation of an Endogenous Peer-to-Peer Market with Product Differentiation" for PowerTech2023, submitted. The simulation is made with Cuda/C++ (https://developer.nvidia.com/cuda-downloads). This document is here to give some hint for the code understanding. To have more details on the notations, and the equation, readers may refer to the article.
This code use data (charge and generator capacity) extrated from a European Data set :
DTU-ELMA/European Dataset (Jensen T, Pinson P. RE-Europe, a large-scale dataset for modeling a highly renewable European electricity system. Sci Data 4, 170175 .2017. https://doi.org/10.1038/sdata.2017.175,
that the complete version is available here https://github.com/DTU-ELMA/European_Dataset.
The code needs the solver OSQP :
Stellato B, Banjac G, Goulart P, Bemporad A, Boyd, S. OSQP: an operator splitting solver for quadratic programs.  Mathematical Programming Computation https://doi.org/10.1007/s12532-020-00179-2
which is available here https://github.com/osqp/osqp
and can also use the cuda OSQP solver on C++ (not the two OSQP code at the same time) with the preprossessor variable OSQPGPU.
https://github.com/ZhenshengLee/cuosqp

The code may also need the installation of the Eigen library https://eigen.tuxfamily.org/index.php?title=Main_Page

## Method Presentation

ADMM(GPU)Const are the methods to compute the Endogenous P2P market. DCOPFOSQP is a DC-OPF using an OSQP solver. 
ADMM(GPU)ConstCons compute the Endogenous P2P market by using a consensus between the SO and the market as in : Dong A,  Baroche T,  Le Goff Latimier R,  Ben Ahmed H. Asynchronous algorithm of an endogenous peer-to-peer electricity market. 2021 IEEE Madrid PowerTech. IEEE, 2021



###### ADMMConst
Method on CPU not optimized

###### ADMMCons1t
Method on CPU with sparcity, with less memory transfert

###### ADMMGPUConst1
Method with P2P market and SO parallelized on GPU. One iteration of the P2P Market is made in one kernel call.

###### ADMMGPUConst1T
Same as the previous method but the transpose of the matrix is used fot the SO computation

###### ADMMGPUConst2
Same as the previous method, but several iterations of the P2P Market is made in one kernel call thanks to local memory.

###### ADMMGPUConst3
Same as the previous method, but here shared memory is used.

###### ADMMGPUConst4
Same as the previous method, but all iterations of the P2P Market is made in one kernel call.

###### ADMMGPUConstCons1
Method with P2P market on GPU. The SO computation of the DC-OPF and the consensus is made on CPU with a OSQP

###### ADMMGPUConstCons2
Same but the SO use a personal dual ascent method on CPU

###### ADMMGPUConstCons3
Same but the SO use a personal dual ascent method on GPU


