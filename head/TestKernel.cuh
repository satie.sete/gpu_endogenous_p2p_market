#pragma once
#include <iostream>
#include <math.h>
#include <chrono>
#include <fstream>

#include "MatrixGPU.cuh"
#include "MatrixCPU.h"


float testCalculQpart(int method);

__global__ void calculQpartLineBloc(float* Qpart, float* alpha, const int N);

__global__ void calculQpartAgentBloc(float* Qpart, float* alpha, const int L, const int N);

__global__ void calculQpartLineBlocTrans(float* Qpart, float* alpha, const int N, const int L);

__global__ void calculQpartAgentBlocTrans(float* Qpart, float* alpha, const int L, const int N);

__global__ void calculQpartLineBlocReverse(float* Qpart, float* alpha, const int N);

__global__ void calculQpartLineBlocReverseTrans(float* Qpart, float* alpha, const int N, const int nLine);

__global__ void calculQpartLineBlocReverseBis(float* Qpart, float* alpha, const int N);

__global__ void calculQpartLineBlocReverseBisTrans(float* Qpart, float* alpha, const int N, const int nLine);

float testCalculAlpha(int method);

__global__ void updateAlphaSh(float* alpha, float* G, float* Pn, const int nLine, const int nAgent);

__global__ void updateAlpha2D(float* alpha, float* G, float* Pn, const int nLine, const int nAgent);

__global__ void updateAlpha1D(float* alpha, float* G, float* Pn, const int nLine, const int nAgent);

__global__ void updateAlphaShTrans(float* alpha, float* G, float* Pn, const int nLine, const int nAgent);

__global__ void updateAlpha2DTrans(float* alpha, float* G, float* Pn, const int nLine, const int nAgent);

__global__ void updateAlpha1DTrans(float* alpha, float* G, float* Pn, const int nLine, const int nAgent);


float testCalculCpa(int method);

template <unsigned int blockSize>
__global__ void updateCp2aTest(float* Cp2, float* diffKappa, float* G, const int nLine, const int nAgent);

template <unsigned int blockSize>
__global__ void updateCp2aTestTrans(float* Cp2, float* diffKappa, float* G, const int nLine, const int nAgent);


float testCalculCpb(int method);

template <unsigned int blockSize>
__global__ void updateCp2bTest(float* tempN1, float* G, float* Qpart, const int nLine, const int nAgent);

template <unsigned int blockSize>
__global__ void updateCp2bTestTrans(float* tempN1, float* G, float* Qpart, const int nLine, const int nAgent);

float testCalculQtot(int method);

__global__ void updateQtotTest(float* Qtot, float* Qpart, float* alpha, const int nLine, const int nAgent);

__global__ void updateQtotTestTrans(float* Qtot, float* Qpart, float* alpha, const int nLine);

template <unsigned int blockSize>
__device__ void warpReduceTest(volatile float* sdata, unsigned int tid);



float testCalculCp(int method);


template <unsigned int blockSize>
__global__ void updateCp2Test(float* Cp2, float* diffKappa, float* G, float* Qpart, float* nVoisin, float rho1, const int nLine, const int nAgent);

template <unsigned int blockSize>
__global__ void updateCp2TestTrans(float* Cp2, float* diffKappa, float* G, float* Qpart, float* nVoisin, float rho1, const int nLine, const int nAgent);


float testCalculResX(int method);

__global__ void updateResXTest(float* res, float* Kappa1, float* Kappa2, float* KappaPre1, float* KappaPre2, const int nLine);



float testCalculLAMBDABt1(int method);
__global__ void updateLAMBDAGPUTest(float* LAMBDA, float* trade, float rho, float* CoresLinTrans, int const N);

__global__ void updateBt1GPUTest(float* Bt1, float* tradeLin, float rho, float* LAMBDA, float* CoresLinTrans, int const N);


__global__ void updateLAMBDABt1GPUTest(float* Bt1, float* LAMBDA, float* tradeLin, float rho,  float* CoresLinTrans, int const N);