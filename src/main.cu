
#include "../head/main.cuh"


// mingw32 - make.exe

// Simulation

int main(int argc, char* argv[]){
#ifdef DEBUG_TEST
	std::cout << "test Agent err =" << testAgent() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test StudyCase err =" << testStudyCase() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test Simparam err =" << testSimparam() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test ADMMConst err =" << testADMM() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test ADMMGPU err =" << testADMMGPUConst1() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test ADMMGPU5 err =" << testADMMGPU5() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test ADMMGPU6 err =" << testADMMGPU6() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test ADMMGPUConst2 err =" << testADMMGPUConst2() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test ADMMGPUConst3 err =" << testADMMGPUConst3() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test systeme err =" << testSysteme() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test systeme err =" << result << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test Matrix err =" << testMatrix() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test MatrixGPU err =" << testMGPU() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "test OSQP err =" << testOSQP() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	testADMMGPUtemp();
	std::cout << "-------------------------------------------------------- " << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
#endif // DEBUG_TEST
	srand(time(nullptr));
	std::cout.precision(6);
	//std::cout << "test StudyCase err =" << testStudyCase() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	//std::cout << "test Matrix err =" << testMatrix() << std::endl;
	//std::cout << "test MatrixGPU err =" << testMGPU() << std::endl;
	//std::cout << "test ADMMConst err =" << testADMM() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	//std::cout << "test ADMMConst err =" << testADMM1() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	//std::cout << "test ADMMGPU err =" << testADMMGPUConst1() << std::endl;
	std::cout << "-------------------------------------------------------- " << std::endl;
	//std::cout << testCalculResX(1) << std::endl;

	//testADMMGPUtemp();
	//getErrorSensiPwerLine();
	//SimuStatPowerTech();


	//comparaisonArticle();
	//testExemple();
	//SimulationTempStepOpti();
	//SimuTemporalLlimit("Europe");
	SimuTemporalConvergence("Europe");
	//SimuTemporal();
	//SimuTemporal("France");
	//std::cout << testCalculLAMBDABt1(1) / 1000000000 << " s" << std::endl;
	//std::cout << testCalculLAMBDABt1(1) / 1000000000 << " s" << std::endl;
	//std::cout << testCalculQpart(7) / 1000000000 << " s" << std::endl;
	//std::cout << testCalculQpart(2) / 1000000000 << " s" << std::endl;
	//SimuTemporalRho();
	
	


	

	return 0;
}




void SimulationTempStepOpti()
{
	// date 0->4 1 janvier 2012
	// date 0->4 1 juin 2013
	// date 10-15 20 octobre 2014

	std::string path = "data/";
	std::string fileName = "SimutemporalStep5a.csv";
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	
	std::string methode = "ADMMGPUConst5a";
	
	float epsG = 0.001f;
	float epsGC = 0.1f;
	float epsL = 0.00001f;



	MatrixCPU interval(4, 2);
	interval.set(0, 0, 2013);
	interval.set(0, 1, 2013);
	interval.set(1, 0, 6);
	interval.set(1, 1, 6);
	interval.set(2, 0, 10);
	interval.set(2, 1, 10);
	interval.set(3, 0, 0);
	interval.set(3, 1, 4);

	int iterG = 80000;
	int iterL = 1500;

	//int nCons = 25;
	//int nGen = 25;
	int nGen = 969;
	int nCons = 1494;
	int nAgent = nGen + nCons;



	float rho = 12; //rhoAgent * nAgent; //0.056 * nAgent;
	float rho1 = 0.0003; //0.0004;
	float rhoL = 12;

	
	const int nStep = 9;
	int Steps[nStep] = { 1, 2, 5, 10, 25, 50, 100, 200, 500 };
	MatrixCPU StepLocals(1, nStep);
	MatrixCPU StepGlobal(1, nStep);
	for (int i = 0; i < nStep; i++) {
		StepLocals.set(0, i, Steps[i]);
		StepGlobal.set(0, i, Steps[i]);
	}

	System sys;
	sys.setIter(iterG, iterL);
	sys.setEpsG(epsG);
	sys.setEpsL(epsL);
	sys.setEpsGC(epsGC);
	sys.setMethod(methode);
	sys.setRho(rho);
	sys.setRhoL(rhoL);
	sys.setRho1(rho1);


	MatrixCPU Param(1, 18);
	Param.set(0, 0, nAgent);
	Param.set(0, 1, epsGC);
	Param.set(0, 2, epsG);
	Param.set(0, 3, epsL);
	Param.set(0, 4, iterG);
	Param.set(0, 5, iterL);
	Param.set(0, 6, 0);
	Param.set(0, 7, rho);
	Param.set(0, 8, rhoL);
	Param.set(0, 9, rho1);
	Param.set(0, 10, interval.get(0, 0));
	Param.set(0, 11, interval.get(1, 0));
	Param.set(0, 12, interval.get(2, 0));
	Param.set(0, 13, interval.get(3, 0));
	Param.set(0, 14, interval.get(0, 1));
	Param.set(0, 15, interval.get(1, 1));
	Param.set(0, 16, interval.get(2, 1));
	Param.set(0, 17, interval.get(3, 1));
	Param.saveCSV(fileName, mode);
	
	StepLocals.saveCSV(fileName, mode);
	StepGlobal.saveCSV(fileName, mode);


	for (int local = 0; local < 1; local++) {
		for (int global = 0; global < nStep; global++) {
			sys.setStep(StepGlobal.get(0, global), StepLocals.get(0, local));
			
		
			clock_t t = clock();
			sys.solveIntervalle(path, &interval, nCons, nGen);
			t = clock() - t;

			std::cout << "temps simulation : " << t / CLOCKS_PER_SEC << std::endl;


			MatrixCPU temps(sys.getTemps());
			MatrixCPU iter(sys.getIter());
			MatrixCPU conv(sys.getConv());
			MatrixCPU fc(sys.getFc());
			//MatrixCPU ResR(sys.getResR());
			//MatrixCPU ResS(sys.getResS());

			temps.display();
			iter.display();

			temps.saveCSV(fileName, mode);
			iter.saveCSV(fileName, mode);
			fc.saveCSV(fileName, mode);
			//ResR.saveCSV(fileName, mode);
			//ResS.saveCSV(fileName, mode);
			conv.saveCSV(fileName, mode);

			std::cout << "-------------------------------------------------------- " << std::endl;
			sys.resetParam();
		}
	}



}

void comparaisonArticle()
{
	System sys;
	std::string fileName = "CompareArticle2.csv";
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;



	std::string method = "ADMMGPUConst1T";
	//std::string method = "ADMMGPUConst4";
	sys.setMethod(method);

	float epsG  = 0.00001f;
	float epsGC = 0.001f;
	float epsL  = 0.000005f;
	int   iterG = 5000;
	int   iterL = 10000;
	float stepG = 1;
	float stepL = 1;
	float rho = 1.5;
	//float rho1 = 0.00028;
	float rho1  = 0.5; 
	float rhoL  = 1.5; 
	float distCost = 10;
	/*MatrixCPU MatDistCost = MatrixCPU(4, 4, distCost);
	for (int i = 0; i < 4; i++) {
		MatDistCost.set(i, 3, distCost / 2); // moins de co�t pour les �changes avec la zone 4
		MatDistCost.set(3, i, distCost / 2); // moins de co�t pour les �changes avec la zone 4
	}*/
	//MatDistCost.set(3, 3, distCost / 2); // moins de co�t pour les �changes dans la zone 4 uniquement
		
	sys.setIter(iterG, iterL);
	sys.setEpsG(epsG);
	sys.setEpsL(epsL);
	sys.setEpsGC(epsGC);
	sys.setStep(stepG, stepL);
	StudyCase cas;
	cas.Set39Bus();


	cas.toReduce = true;
	cas.setLineLimit(26, 200);
	cas.setDistance(1, "data/Distance39.txt");
	cas.genBetaDistance(distCost);
	
	//cas.genBetaDistanceByZone(&MatDistCost);
	MatrixCPU beta(cas.getBeta());
	//beta.display();


	sys.setStudyCase(cas);
	sys.setRho(rho);
	sys.setRho1(rho1);
	sys.setRhoL(rhoL);


	MatrixCPU Param(1, 13);
	Param.set(0, 0, cas.getNagent());
	Param.set(0, 1, epsGC);
	Param.set(0, 2, epsG);
	Param.set(0, 3, epsL);
	Param.set(0, 4, iterG);
	Param.set(0, 5, iterL);
	Param.set(0, 6, stepG);
	Param.set(0, 7, stepL);
	Param.set(0, 8, rho);
	Param.set(0, 9, rhoL);
	Param.set(0, 10, rho1);
	Param.set(0, 11, distCost);
	Param.set(0, 12, 200);
	std::cout << " Save Y:1, N:0" << std::endl;
	int inputUser =0;
	std::cin >> inputUser;
	if (inputUser) {
		Param.saveCSV(fileName, mode);
	}

	MatrixCPU Result(1, 7);




	Simparam res = sys.solve();
	int iter =  res.getIter();
	float fc = res.getFc();

	MatrixCPU Res(res.getRes());
	//Res.saveCSV("ResRhoRhoRho.csv", std::fstream::in | std::fstream::out | std::fstream::app);
	float resR = Res.get(0, (iter - 1) / stepG);
	float resS = Res.get(1, (iter - 1) / stepG);
	float resX = Res.get(2, (iter - 1) / stepG);
	float temps = res.getTime();
	std::cout << "----------------" << std::endl;
	Result.set(0, 0, iter);
	Result.set(0, 1, temps);
	Result.set(0, 2, fc);
	Result.set(0, 3, resR);
	Result.set(0, 4, resS);
	Result.set(0, 5, resX);
	

	std::cout << "----------------" << std::endl;

	std::cout << "Resultat de simulation fc " << fc << std::endl;
	std::cout << "temps de simulation " << temps << std::endl;
	std::cout << "Nombre d'it�ration " << iter << std::endl;
	std::cout << " Residus R,S,X : " << resR << ", " << resS << ", " << resX << std::endl;
	std::cout << "----------------" << std::endl;
	std::cout << "Echange entre les agents " << std::endl;
	sys.displayTradesAgent();
	std::cout << "----------------" << std::endl;
	std::cout << "flux dans les lignes " << std::endl;
	if (cas.getNLine() != 0) {
		MatrixCPU Sensi(cas.getPowerSensi());
		
			
		MatrixCPU Pn(res.getPn());
		if (inputUser) {
			Pn.saveCSV(fileName, mode, 1);
		}
		MatrixCPU g(cas.getNLine(), 1, 0);
		g.multiply(&Sensi, &Pn);
		cas.displayLineCores(&g);
		Result.set(0, 6, g.get(0, 0));
	}/**/
	if (inputUser) {
		Result.saveCSV(fileName, mode);
	}
	
}

void testExemple()
{
	System sys;
	std::string method = "ADMMGPUConstCons5";
	//std::string method = "ADMMConst";
	sys.setMethod(method);
	std::string path = "data/";
	
	float epsG = 0.0001f;
	float epsL = 0.00005f;
	int iterG = 20000;
	int iterL = 3000;
	int stepG = 100;
	int stepL = 5;
	sys.setStep(stepG, stepL);
	sys.setIter(iterG, iterL);
	sys.setEpsG(epsG);
	sys.setEpsL(epsL);
	StudyCase cas;
	cas.Set4nodeBis(path);
	cas.toReduce = false;
	cas.display(0);
	float rho = 1.5;
	float rho1 = 0.01;
	float rhol = 2;
	sys.setStudyCase(cas);
	sys.setRho(rho);
	sys.setRho1(rho1);
	sys.setRhoL(rhol);
	Simparam res = sys.solve();
	std::cout << "---------------------------------------------------" << std::endl;
	res.display(0);
	
	MatrixCPU Sensi(cas.getPowerSensi());
	MatrixCPU Pn(res.getPn());
	MatrixCPU g(cas.getNLine(), 1, 0);
	std::cout << "----------------" << std::endl;
	std::cout << "Puissance des agents " << std::endl;
	Pn.display();
	std::cout << "----------------" << std::endl;
	//std::cout << "Echange entre les agents " << std::endl;
	//sys.displayTradesAgent();
	std::cout << "----------------" << std::endl;
	std::cout << "flux dans les lignes " << std::endl;
	if (cas.getNLine() != 0) {
		g.multiply(&Sensi, &Pn);
		cas.displayLineCores(&g);
	}
	///MatrixCPU residual(res.getRes());
}

void blague()
{
	float values[] = { 76923076923.0, 1428.5714285714284, 1250000.0, 8333.333333333332, 125000000.0, 7142857.142857143 };
	float sorted_values[] = { 1428.5714285714284, 8333.333333333332, 1250000.0, 7142857.142857143, 125000000.0, 76923076923.0 };
	float sum1 = 0;
	float sum2 = 0;

	for (int i = 0; i < 6; i++) {
		sum1 += values[i];
		sum2 += sorted_values[i];
	}
	std::cout << sum1 << " " << sum2 << " " << (sum1 == sum2) << " " << sum1 - sum2 << std::endl;
}



void SimuTemporal(std::string name)
{
	std::string path = "data/"+ name +"/";
	std::string fileName = "SimutemporalFB" + name + ".csv";
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	const int nMethode = 1;
	//std::string methodes[nMethode] = { "ADMMGPUConst5", "ADMMGPUConstCons5" };
	std::string methodes[nMethode] = { "ADMMConst1"  };

	//std::string methodes[nMethode] = { "ADMMGPUConst1","ADMMGPUConst2","ADMMGPUConst3", "ADMMGPUConst3" };
	//std::string methodes[nMethode] = { "ADMMConst1", "ADMMGPUConst1T","ADMMGPUConst3", "ADMMGPUConst4"};
	float epsG = 0.001f;
	float epsGC = 1.0f;
	float epsL = 0.0001f;
	float offset = 1;


	MatrixCPU interval(4, 2);
	interval.set(0, 0, 2013);
	interval.set(0, 1, 2013);
	interval.set(1, 0, 6);
	interval.set(1, 1, 6);
	interval.set(2, 0, 1);
	interval.set(2, 1, 10);
	interval.set(3, 0, 0);
	interval.set(3, 1, 23);

	int iterG = 200000;
	int iterL = 1500;


	

	float rho = 125;
	float rho1 = 0.0000001;
	//float rho1 = 125;
	float rhoL = 125;

	float stepG = 2;
	float stepL = 1;

	System sys;
	
	sys.setIter(iterG, iterL);
	sys.setStep(stepG, stepL);
	sys.setEpsGC(epsGC);
	sys.setEpsG(epsG);
	sys.setEpsL(epsL);
	
	

	MatrixCPU Param(1, 18);
	Param.set(0, 0, 0);
	Param.set(0, 1, epsGC);
	Param.set(0, 2, epsG);
	Param.set(0, 3, epsL);
	Param.set(0, 4, iterG);
	Param.set(0, 5, iterL);
	Param.set(0, 6, stepG);
	Param.set(0, 7, stepL);
	Param.set(0, 8, rho);
	Param.set(0, 9, rho1);
	Param.set(0, 10, interval.get(0, 0));
	Param.set(0, 11, interval.get(1, 0));
	Param.set(0, 12, interval.get(2, 0));
	Param.set(0, 13, interval.get(3, 0));
	Param.set(0, 14, interval.get(0, 1));
	Param.set(0, 15, interval.get(1, 1));
	Param.set(0, 16, interval.get(2, 1));
	Param.set(0, 17, interval.get(3, 1));
	Param.saveCSV(fileName, mode);


	for (int i = 0; i < nMethode; i++) {
		std::string methode = methodes[i];
		sys.setMethod(methode);

		sys.setRho(rho);
		sys.setRho1(rho1);
		sys.setRhoL(rhoL);
		std::cout << "----------------Simu---------------------------- " << std::endl;
		// Debut simu

		clock_t t = clock();

		sys.solveIntervalle(path, name, &interval);
		t = clock() - t;

		std::cout << "calculation time : " << (float) t / CLOCKS_PER_SEC << std::endl;

		MatrixCPU temps(sys.getTemps());
		MatrixCPU iter(sys.getIter());
		MatrixCPU conv(sys.getConv());
		MatrixCPU fc(sys.getFc());
		MatrixCPU ResR(sys.getResR());
		MatrixCPU ResS(sys.getResS());
		MatrixCPU ResX(sys.getResX());

		/*temps.display();
		ResR.display();
		ResS.display();
		ResX.display();
		iter.display();
		temps.saveCSV(fileName, mode);
		iter.saveCSV(fileName, mode);
		fc.saveCSV(fileName, mode);
		ResR.saveCSV(fileName, mode);
		ResS.saveCSV(fileName, mode);
		conv.saveCSV(fileName, mode);*/

		std::cout << "-------------------------------------------------------- " << std::endl;
		sys.displayTime(fileName);
		sys.resetParam();
	}

}

void SimuTemporalRho(std::string name)
{
	int type = 0;
	std::string path;
	int nGen;
	int nCons;
	int nAgent;
	int nLine;
	if (!name.compare("Europe")) {
		type = 1;
		path = "data/";
		nGen = 969;
		nCons = 1494;
		nAgent = nGen + nCons;
		nLine = 2156;
	}else {
		path = "data/" + name + "/";
	}
	std::string fileName = "SimutemporalRho" + name + ".csv";
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	const int nMethode = 1;
	std::string methodes[nMethode] = { "ADMMGPUConst1T" };
	//std::string method = "ADMMGPUConst1";
	//std::string method = "ADMMConst";
	
	//std::string methodes[nMethode] = { "ADMMGPUConst1","ADMMGPUConst2","ADMMGPUConst3", "ADMMGPUConst3" };
	float epsG = 0.01f;
	float epsGC = 0.1f;
	float epsL = 0.005f;

	MatrixCPU interval(4, 2);
	interval.set(0, 0, 2013);
	interval.set(0, 1, 2013);
	interval.set(1, 0, 6);
	interval.set(1, 1, 6);
	interval.set(2, 0, 1);
	interval.set(2, 1, 1);
	interval.set(3, 0, 0);
	interval.set(3, 1, 0);

	int iterG = 20000;
	int iterL = 2000;//500;
	

	const int nRho = 7; // rhoAgent = 1 semble une bonne valeur
	MatrixCPU rhoTab(2, nRho, 7);
	rhoTab.set(1, 0, 0.0002);

	for (int i = 1; i < nRho; i++) {
		rhoTab.set(0, i, rhoTab.get(0, i - 1) + 1);
		rhoTab.set(1, i, rhoTab.get(1, i - 1) + 0.0001);
	}
		
	float stepG = 10;
	float stepL = 5;
	
	System sys; 
	StudyCase cas;
	sys.setIter(iterG, iterL);
	sys.setStep(stepG, stepL);
	sys.setEpsG(epsG);
	sys.setEpsGC(epsGC);
	sys.setEpsL(epsL);


	MatrixCPU Param(1, 16);
	Param.set(0, 0, 1);
	Param.set(0, 1, epsG);
	Param.set(0, 2, epsGC);
	Param.set(0, 3, epsL);
	Param.set(0, 4, iterG);
	Param.set(0, 5, iterL);
	Param.set(0, 6, stepG);
	Param.set(0, 7, stepL);
	Param.set(0, 8, interval.get(0, 0));
	Param.set(0, 9, interval.get(1, 0));
	Param.set(0, 10, interval.get(2, 0));
	Param.set(0, 11, interval.get(3, 0));
	Param.set(0, 12, interval.get(0, 1));
	Param.set(0, 13, interval.get(1, 1));
	Param.set(0, 14, interval.get(2, 1));
	Param.set(0, 15, interval.get(3, 1));
	std::cout << " Save Y:1, N:0" << std::endl;
	int inputUser;
	std::cin >> inputUser;
	if (inputUser) {
		Param.saveCSV(fileName, mode);
		rhoTab.saveCSV(fileName, mode);
	}
	
	
	for (int i = 0; i < nMethode; i++) {
		std::string methode = methodes[i];
		sys.setMethod(methode);
		for (int k = 0; k < nRho; k++) {
			float rho = rhoTab.get(0, k);
			sys.setRho(rho);
			for (int j = 0; j < nRho; j++) {
				float rho1 = rhoTab.get(1, j);
				float rhoL = 1 * MAX(rho, rho1);
				sys.setRho1(rho1);
				sys.setRhoL(rhoL);

				std::cout << "----------------Simu---------------------------- " << std::endl;
				// Debut simu
				clock_t t = clock();
				if (type) {
					sys.solveIntervalle(path, &interval, nCons, nGen);
				}else {
					sys.solveIntervalle(path, name, &interval);
				}
				
				t = clock() - t;

				std::cout << "temps simulation : " << t / CLOCKS_PER_SEC << std::endl;

				MatrixCPU iter(sys.getIter());
				MatrixCPU ResR(sys.getResR());
				MatrixCPU ResS(sys.getResS());
				MatrixCPU ResX(sys.getResX());

				if (inputUser) {
					iter.saveCSV(fileName, mode);
					ResR.saveCSV(fileName, mode);
					ResS.saveCSV(fileName, mode);
					ResX.saveCSV(fileName, mode);
				}


				std::cout << "-------------------------------------------------------- " << std::endl;
				sys.resetParam();
				sys.resetMethod();
			}
		}
	}

}


void SimuTemporal()
{
	std::string path = "data/";
	//std::string fileName2 = "SimutemporalFBTimeOffset.csv";
	std::string fileName = "SimutemporalCompareHugeOffsetPowerTech.csv";
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	const int nMethode = 1;
	std::string methodes[nMethode] = { "ADMMGPUConst1T" };

	//std::string methodes[nMethode] = { "ADMMGPUConst1","ADMMGPUConst1T","ADMMGPUConst2", "ADMMGPUConst3", "ADMMGPUConst4", "ADMMGPUConst5", "ADMMGPUConst5a" };
	//std::string methodes[nMethode] = { "ADMMConst1", "ADMMGPUConst1T" };
	float epsG = 0.01f;
	float epsGC = 10.0f;
	float epsL = 0.001f;
	float lineLimMin = 0; // MW
	int offset = 2;
	int warmstart = 1;

	MatrixCPU interval(4, 2);
	interval.set(0, 0, 2013);
	interval.set(0, 1, 2013);
	interval.set(1, 0, 6);
	interval.set(1, 1, 6);
	interval.set(2, 0, 1);
	interval.set(2, 1, 30);
	interval.set(3, 0, 0);
	interval.set(3, 1, 23);

	int iterG = 200000;
	int iterL = 5000;//500;


	int nGen = 969;
	int nCons = 1494;
	int nAgent = nGen + nCons;

	
	
	float rho = 12; //rhoAgent * nAgent; //0.056 * nAgent;
	float rho1 = 0.00037; //0.0004;
	float rhoL = 12;

	float stepG = 10;
	float stepL = 1;

	System sys;
	StudyCase cas;
	sys.setIter(iterG, iterL);
	sys.setStep(stepG, stepL);
	sys.setEpsGC(epsGC);
	sys.setEpsG(epsG);
	sys.setEpsL(epsL);
	sys.setLineLimitMin(lineLimMin);
	sys.setWarmStart(warmstart==1);
	if (offset)
	{
		sys.setConstraintRelaxation(offset);
	}


	MatrixCPU Param(1, 22);
	Param.set(0, 0, nAgent);
	Param.set(0, 1, epsGC);
	Param.set(0, 2, epsG);
	Param.set(0, 3, epsL);
	Param.set(0, 4, iterG);
	Param.set(0, 5, iterL);
	Param.set(0, 6, stepG);
	Param.set(0, 7, stepL);
	Param.set(0, 8, rho);
	Param.set(0, 9, rho1);
	Param.set(0, 10, rhoL);
	Param.set(0, 11, interval.get(0, 0));
	Param.set(0, 12, interval.get(1, 0));
	Param.set(0, 13, interval.get(2, 0));
	Param.set(0, 14, interval.get(3, 0));
	Param.set(0, 15, interval.get(0, 1));
	Param.set(0, 16, interval.get(1, 1));
	Param.set(0, 17, interval.get(2, 1));
	Param.set(0, 18, interval.get(3, 1));
	Param.set(0, 19, lineLimMin);
	Param.set(0, 20, warmstart);
	Param.set(0, 21, offset);
	Param.saveCSV(fileName, mode);
	//Param.saveCSV(fileName2, mode);


	for (int i = 0; i < nMethode; i++) {
		std::string methode = methodes[i];
		sys.setMethod(methode);
		
		sys.setRho(rho);
		sys.setRho1(rho1);
		sys.setRhoL(rhoL);
		std::cout << "----------------Simu---------------------------- " << std::endl;
		// Debut simu

		clock_t t = clock();

		sys.solveIntervalle(path, &interval, nCons, nGen);
		t = clock() - t;

		std::cout << "calculation time : " << (float)t / CLOCKS_PER_SEC << std::endl;


		MatrixCPU temps(sys.getTemps());
		MatrixCPU iter(sys.getIter());
		MatrixCPU conv(sys.getConv());
		MatrixCPU fc(sys.getFc());
		MatrixCPU ResR(sys.getResR());
		MatrixCPU ResS(sys.getResS());
		MatrixCPU ResX(sys.getResX());

		temps.display();
		ResR.display();
		ResS.display();
		ResX.display();
		iter.display();
		temps.saveCSV(fileName, mode);
		iter.saveCSV(fileName, mode);
		fc.saveCSV(fileName, mode);
		ResR.saveCSV(fileName, mode);
		ResS.saveCSV(fileName, mode);
		ResX.saveCSV(fileName, mode);
		conv.saveCSV(fileName, mode);/**/

		std::cout << "-------------------------------------------------------- " << std::endl;
		//sys.displayTime(fileName2);
		sys.resetParam();
	}

}

void SimuCompare()
{
	std::string fileName = "ComparaisonArticle_Release_512_700.csv";
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	std::string methodes[4] = { "OSQP", "ADMMConst", "ADMMGPU", "ADMMGPU5" };
	//std::string methodes[8] = { "OSQP", "ADMMConst","ADMMGPU", "ADMMGPU2","ADMMGPU3","ADMMGPU4", "ADMMGPU5", "ADMMGPU6" };
	float rhoAgent = 0.05;
	float P = 1000;
	float dP = 400;
	float a = 0.07;
	float da = 0.02;
	float b = 50;
	float db = 20;
	int nNAgent = 1;
	int nAgentMax = 700;
	int offset = 0;
	int nMethode = 4;
	//int nMethode = 8;
	int nSimu = 50;
	int iterGlobal = 1500;
	int iterLocal = 700;
	int stepG = 5;
	int stepL = 5;
	float epsG = 0.01f;
	float epsL = 0.005f;

	MatrixCPU Param(1, 10);
	Param.set(0, 0, nAgentMax);
	Param.set(0, 1, nNAgent);
	Param.set(0, 2, rhoAgent);
	Param.set(0, 3, epsG);
	Param.set(0, 4, epsL);
	Param.set(0, 5, iterGlobal);
	Param.set(0, 6, iterLocal);
	Param.set(0, 7, stepG);
	Param.set(0, 8, stepL);
	Param.set(0, 9, nSimu);
	
	Param.saveCSV(fileName, mode);

	MatrixCPU Agents(1, nNAgent);
	MatrixCPU temps(nMethode * nSimu, nNAgent,-1);
	MatrixCPU iters(nMethode * nSimu, nNAgent,-1);
	MatrixCPU fcs(nMethode * nSimu, nNAgent,-1);
	MatrixCPU ResF(2, iterGlobal / stepG);
	MatrixCPU ResR(nMethode  * nSimu, nNAgent,-1);
	MatrixCPU ResS(nMethode  * nSimu, nNAgent,-1);
	System sys;
	sys.setIter(iterGlobal, iterLocal);
	sys.setEpsG(epsG);
	sys.setEpsL(epsL);
	sys.setStep(stepG, stepL);


	for (int agent = 0; agent < nNAgent; agent++) {
		std::cout << "--------- --------- --------- --------- ----------" << std::endl;

		int agents = (agent + 1) * (nAgentMax-offset) / nNAgent + offset;
		std::cout << agents << std::endl;
		
		Agents.set(0, agent, agents);

		for (int j = 0; j < nSimu;j++) {
			StudyCase cas(agents, P, dP, a, da, b, db);
			sys.setStudyCase(cas);
			std::cout << "-";
			float rho = rhoAgent * agents;
			sys.setRho(rho);
			for (int methode = 0; methode < nMethode;methode++) {
				sys.setMethod(methodes[methode]);
				clock_t t = clock();
				Simparam res = sys.solve();
				float temp = clock()-t;
				int iter = res.getIter();
				temps.set(methode * nSimu + j, agent, temp/CLOCKS_PER_SEC);
				iters.set(methode * nSimu + j, agent, iter);
				fcs.set(methode  * nSimu + j, agent, res.getFc());
				ResF = res.getRes();
				ResR.set(methode  * nSimu + j, agent, ResF.get(0, (iter - 1) / stepG));
				ResS.set(methode  * nSimu + j, agent, ResF.get(1, (iter - 1) / stepG));
			}
		}
		std::cout << std::endl;
	}
	Agents.saveCSV(fileName, mode);
	temps.saveCSV(fileName, mode);
	iters.saveCSV(fileName, mode);
	fcs.saveCSV(fileName, mode);
	ResR.saveCSV(fileName, mode);
	ResS.saveCSV(fileName, mode);
}

void SimuStatPowerTech()
{
	std::string fileName = "PowerTechSizeEvolutionReduceOffsetCPU2.csv";
	std::string fileName2 = "PowerTechSizeEvolutionReduceOffsetCPUFB2.csv";
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	std::string methode = "ADMMConst1";
	std::string path = "data/";
	float rhoAgent = 10;
	float rhoLine = 0.0005;
	
	float Pconso = 60;
	float dPconso = 50;
	float Propcons = 0.60;
	float bProd = 20;
	float dbProd = 18;
	float Pprod = 300;
	float dPprod = 250;
	float gamma = 4;
	float dgamma = 2;
	float limit = 1000;
	float dlimit = 300;
	
	int nNAgent = 1;
	int nAgentMax = 600;
	int nNLine = 10;
	int nLineMax = 900;
	int offsetAgent = 0;
	int offsetLine = 0;
	int nSimu = 20;
	
	int offset = 2;
	int iterGlobal = 50000;
	int iterLocal = 5000;
	int stepG = 10;
	int stepL = 1;
	float epsG = 0.01f;
	float epsL = 0.001f;
	float epsGC = 1.0f;
	MatrixCPU Param(1, 18);
	Param.set(0, 0, nAgentMax);
	Param.set(0, 1, nNAgent);
	Param.set(0, 2, rhoAgent);
	Param.set(0, 3, nLineMax);
	Param.set(0, 4, nNLine);
	Param.set(0, 5, rhoLine);
	Param.set(0, 6, epsG);
	Param.set(0, 7, epsGC);
	Param.set(0, 8, epsL);
	Param.set(0, 9, iterGlobal);
	Param.set(0, 10, iterLocal);
	Param.set(0, 11, stepG);
	Param.set(0, 12, stepL);
	Param.set(0, 13, nSimu);
	Param.set(0, 14, offset);
	Param.set(0, 15, Pconso);
	Param.set(0, 16, Pprod);
	Param.set(0, 17, limit);
	Param.saveCSV(fileName, mode);
	//Param.saveCSV(fileName2, mode);

	MatrixCPU Agents(1, nNAgent);
	MatrixCPU Lines(1, nNLine);

	MatrixCPU temps(nNLine * nSimu, nNAgent, -1);
	MatrixCPU iters(nNLine * nSimu, nNAgent, -1);
	MatrixCPU fcs(nNLine * nSimu, nNAgent, -1);
	MatrixCPU ResF(3, iterGlobal / stepG);
	MatrixCPU ResR(nNLine * nSimu, nNAgent, -1);
	MatrixCPU ResS(nNLine * nSimu, nNAgent, -1);
	MatrixCPU ResX(nNLine * nSimu, nNAgent, -1);
	
	System sys;
	sys.setIter(iterGlobal, iterLocal);
	sys.setEpsG(epsG);
	sys.setEpsL(epsL);
	sys.setStep(stepG, stepL);
	sys.setMethod(methode);
	
	
	/*
	*	void genGridFromFile(std::string path, bool alreadyDefine=true);
		void genAgents(int nAgent, float propCons, float Pconso, float dPconso,  float bProd, float dbProd, float Pprod, float dPpord, float gamma, float dGamma); // gamma = -1 distance ?
		void genLinkGridAgent();
		void genLineLimit(int nLine, float limit, float dlimit );
	*/
	for(int line =0; line<nNLine; line++) {
		std::cout << "--------- --------- --------- --------- ----------" << std::endl;

		int lines = line * (nLineMax - offsetLine) / max((nNLine-1),1) + offsetLine;
		std::cout << lines << std::endl;
		//std::cout << "--------- --------- --------- --------- ----------" << std::endl;
		Lines.set(0, line, lines);
		for (int agent = 0; agent < nNAgent; agent++) {
			//std::cout << "--------- --------- --------- --------- ----------" << std::endl;

			int agents = (agent + 1) * (nAgentMax - offsetAgent) / nNAgent + offsetAgent;
			std::cout << agents << std::endl;
			std::cout << "-|-|-|-|-|-|-|-|-  -|-|-|-|-|-|-|-|-|" << std::endl;
			Agents.set(0, agent, agents);

			for (int j = 0; j < nSimu; j++) {
				StudyCase cas;
				std::cout << "-";
				cas.genGridFromFile(path);
				cas.genAgents(agents, Propcons, Pconso, dPconso, bProd, bProd, Pprod, dPprod, gamma, dgamma); // gamma = -1 distance ?
				cas.genLinkGridAgent();
				cas.genLineLimit(lines, limit, dlimit);
				cas.toReduce = true;
				cas.setLineLimitRelaxation(epsGC);
				sys.setStudyCase(cas);
				//cas.display();
				
				//float rho = rhoAgent * agents;
				//float rho1 = rhoLine * lines;
				sys.setRho(rhoAgent);
				sys.setRho1(rhoLine);
				std::cout << "|";
				clock_t t = clock();
				Simparam res = sys.solve();
				float temp = clock() - t;
				
				int iter = res.getIter();
				temps.set(line * nSimu + j, agent, temp / CLOCKS_PER_SEC);
				iters.set(line * nSimu + j, agent, iter);
				fcs.set(line * nSimu + j, agent, res.getFc());
				ResF = res.getRes();
				ResR.set(line * nSimu + j, agent, ResF.get(0, (iter - 1) / stepG));
				ResS.set(line * nSimu + j, agent, ResF.get(1, (iter - 1) / stepG));
				ResX.set(line * nSimu + j, agent, ResF.get(2, (iter - 1) / stepG));
				sys.displayTime(fileName2);
				sys.resetMethod();
			}
			std::cout << std::endl;
		}
	}
	Agents.display();
	temps.display();
	iters.display();
	float temptotal = temps.sum();
	std::cout << "temps total " << temptotal << " temps moyen " << temptotal / (nSimu * nNLine * nNAgent) << std::endl;
	
	Lines.saveCSV(fileName, mode);
	Agents.saveCSV(fileName, mode);
	temps.saveCSV(fileName, mode);
	iters.saveCSV(fileName, mode);
	fcs.saveCSV(fileName, mode);
	ResR.saveCSV(fileName, mode);
	ResS.saveCSV(fileName, mode);
	ResX.saveCSV(fileName, mode);/**/
}



void SimuStat()
{
	float P = 1000;
	float dP = 300;
	float a = 0.07;
	float da = 0.02;
	float b = 50;
	float db = 20;
	int iterG = 1000;
	int iterL = 700;
	int nAgent = 300;
	int nsimu = 10;

	MatrixCPU Agents(1, nsimu);
	MatrixCPU temps(4, nsimu);
	MatrixCPU iters(4, nsimu);
	System sys;
	sys.setIter(iterG, iterL);

	std::string fileName = "test.csv";
	std::string methodes[4] = { "OSQP", "ADMMConst", "ADMMGPU","ADMMGPU6" };
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;

	for (int indice = 0; indice < nsimu; indice++) {
		//int agents = (agent + 1) * nAgent / nsimu;
		//Agents.set(0, agent, agents);
		StudyCase cas(nAgent, P, dP, a, da, b, db);
		sys.setStudyCase(cas);
		float rho = 0.057 * nAgent;
		sys.setRho(rho);
		for (int methode = 3;methode < 4;methode++) {
			sys.setMethod(methodes[methode]);
			Simparam res = sys.solve();
			float temp = res.getTime();
			int iter = res.getIter();
			temps.set(methode, indice, temp);
			iters.set(methode, indice, iter);
		}
	}
	//Agents.display();
	temps.display();
	iters.display();
	//Agents.saveCSV(fileName, mode);
	temps.saveCSV(fileName, mode);
	iters.saveCSV(fileName, mode);

}

void SimuStatADMMGPU()
{
	//std::string fileName = "ADMMGPU5_article_Release.csv";
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	std::string methode = "ADMMGPUConst3";
	float rhoAgent = 0.05;
	float P = 1000;
	float dP = 400;
	float a = 0.07;
	float da = 0.02;
	float b = 50;
	float db = 20;
	int nNAgent = 1;
	int nAgentMax = 3000;
	int offset = 0;
	int nSimu = 50;
	int iterGlobal = 1500;
	int iterLocal = 700;
	int stepG = 5;
	int stepL = 5;
	float epsG = 0.001f;
	float epsL = 0.0001f;

	MatrixCPU Param(1, 10);
	Param.set(0, 0, nAgentMax);
	Param.set(0, 1, nNAgent);
	Param.set(0, 2, rhoAgent);
	Param.set(0, 3, epsG);
	Param.set(0, 4, epsL);
	Param.set(0, 5, iterGlobal);
	Param.set(0, 6, iterLocal);
	Param.set(0, 7, stepG);
	Param.set(0, 8, stepL);
	Param.set(0, 9, nSimu);
	//Param.saveCSV(fileName, mode);

	MatrixCPU Agents(1, nNAgent);
	MatrixCPU temps(nSimu, nNAgent, -1);
	MatrixCPU iters(nSimu, nNAgent, -1);
	MatrixCPU fcs( nSimu, nNAgent, -1);
	MatrixCPU ResF(2, iterGlobal / stepG);
	MatrixCPU ResR(nSimu, nNAgent, -1);
	MatrixCPU ResS(nSimu, nNAgent, -1);
	System sys;
	sys.setIter(iterGlobal, iterLocal);
	sys.setEpsG(epsG);
	sys.setEpsL(epsL);
	sys.setStep(stepG, stepL);
	sys.setMethod(methode);

	for (int agent = 0; agent < nNAgent; agent++) {
		std::cout << "--------- --------- --------- --------- ----------" << std::endl;

		int agents = (agent + 1) * (nAgentMax - offset) / nNAgent + offset;
		std::cout << agents << std::endl;
		std::cout << "--------- --------- --------- --------- ----------" << std::endl;
		Agents.set(0, agent, agents);

		for (int j = 0; j < nSimu; j++) {
			StudyCase cas(agents, P, dP, a, da, b, db);
			sys.setStudyCase(cas);
			std::cout << "-";
			float rho = rhoAgent * agents;
			sys.setRho(rho);
			clock_t t = clock();
			Simparam res = sys.solve();
			float temp = clock() - t;
			int iter = res.getIter();
			temps.set(j, agent, temp / CLOCKS_PER_SEC);
			iters.set(j, agent, iter);
			fcs.set(j, agent, res.getFc());
			ResF = res.getRes();
			ResR.set(j, agent, ResF.get(0, (iter - 1) / stepG));
			ResS.set(j, agent, ResF.get(1, (iter - 1) / stepG));
			
		}
		std::cout << std::endl;
	}
	Agents.display();
	temps.display();
	iters.display();
	float temptotal = temps.sum();
	std::cout << "temps total " << temptotal << " temps moyen " << temptotal / nSimu << std::endl;
	/*Agents.saveCSV(fileName, mode);
	temps.saveCSV(fileName, mode);
	iters.saveCSV(fileName, mode);
	fcs.saveCSV(fileName, mode);
	ResR.saveCSV(fileName, mode);
	ResS.saveCSV(fileName, mode);*/
}

void testADMMGPUtemp()
{
	System sys;
	float epsG = 0.0001f;
	float epsL = 0.00001f;
	int iterG = 200000;
	int iterL = 1500;
	sys.setEpsG(epsG);
	sys.setEpsL(epsL);
	sys.setStep(1, 1);
	sys.setIter(iterG, iterL);
	sys.setRho1(0.8); //10
	const int nMethode = 11;
	std::string methodes[nMethode] = { "ADMMConst", "ADMMConst1", "ADMMGPUConst1", "ADMMGPUConst1T", "ADMMGPUConst2", "ADMMGPUConst3", "ADMMGPUConst4", "ADMMGPUConstCons", "ADMMGPUConstCons2", "ADMMGPUConstCons3", "DCOPFOSQP"};

	std::cout << "--------------------CAS SANS CONTRAINTE----------------------------- " << std::endl;
	float fc = -69677.4;
	float Pn[29] = { -81.5435 ,-435.3918, -265.1791, -65.3475, -78.8221, -9.8000,  -12.8000, -394.0950, -289.2683, -201.2195,  -48.7805, -319.7409,-25.6740, -261.7490, -282.0785, -166.1365,  -40.2723, -212.2855,  -95.6848,  432.9953, 355.2106, 384.5925, 449.2794,  243.9024,  339.7212,  266.7089,  363.4624,  223.4674, 226.5286 };
	MatrixCPU P(29, 1);
	for (int i = 0;i < 29;i++) {
		P.set(i, 0, Pn[i]);
	}

	MatrixCPU result(nMethode, 1, -1);
	MatrixCPU temps(nMethode, 1, -1);
	
	for (int i = 0; i < nMethode-1; i++) {
		std::string methode = methodes[i];
		sys.setMethod(methode);
		Simparam res = sys.solve();
		
		res.display();
		
		MatrixCPU P2 = res.getPn();
		//P2.display();
		int test = P2.isEqual(&P, 0.1) + 2 * (fabs(fc - res.getFc())<= 0.1);
		result.set(i, 0, test);
		temps.set(i, 0, res.getTime());
		std::cout << "-------------------------------------------------------- " << std::endl;
		
	}

	
	std::cout << "--------------------CAS AVEC CONTRAINTE----------------------------- " << std::endl;
	MatrixCPU result2(nMethode, 1, -1);
	MatrixCPU temps2(nMethode, 1, -1);
	StudyCase cas;
	float lim = 0.8;
	cas.Set2nodeConstraint(lim);
	
	sys.setStudyCase(cas);
	
	float value = (1 - lim) * (lim > 1) + lim;
	float fc1 = -0.960524;
	MatrixCPU P1(2, 1);
	P1.set(0, 0, -value);
	P1.set(1, 0, value);
	for (int i = 0 ; i < nMethode-1; i++) {
		std::string methode = methodes[i];
		std::cout << methode << std::endl;
		sys.setMethod(methode);
		
		Simparam res = sys.solve();
		res.display();
		MatrixCPU P2 = res.getPn();
		//P2.display();
		int test = P2.isEqual(&P1, 0.01) + 2 * (fabs(fc1 - res.getFc()) <= 0.1);
		result2.set(i, 0, test);
		temps2.set(i, 0, res.getTime());
		std::cout << "-------------------------------------------------------- " << std::endl;
	}
	std::cout << "--------------------CAS SANS GAMMA POUR COMPARER AVEC OPF----------------------------- " << std::endl;

	MatrixCPU result3(nMethode, 1, -1);
	MatrixCPU temps3(nMethode, 1, -1);
	cas.Set39Bus();
	cas.toReduce = true;
	sys.setStudyCase(cas);
	float rho = 1.5;
	float epsGC = 0.01f;
	epsG = 0.01f;
	epsL = 0.0001f;
	//float rho1 = 0.00028;
	float rho1 = 0.5;
	float rhoL = 1.5;
	sys.setRho1(rho1);
	sys.setRho(rho);
	sys.setEpsGC(epsGC);
	float fcRef = 0;
	float errM = 0;
	MatrixCPU PnRef;
	MatrixCPU err;
	for (int i = 0; i < nMethode; i++) {
		std::string methode = methodes[i];
		sys.setMethod(methode);
		Simparam res = sys.solve();
		res.display();
		MatrixCPU P2 = res.getPn();

		if (i == 0) { // ce sera la ref
			fcRef = res.getFc();
			PnRef = res.getPn();
			err = MatrixCPU(PnRef.getNLin(), 1, 1);
			std::cout << "fcRef " << fcRef << std::endl;
			result3.set(i, 0, 3);
			temps3.set(i, 0, res.getTime());
		}
		else {
			fc = res.getFc();
			MatrixCPU P2 = res.getPn();
			err.RelativeEror(&PnRef, &P2);
			errM = err.max2();
			std::cout << "fc " << fc << " err " << errM << std::endl;
			//err.display();
			int test = (errM < 0.01) + 2 * (fabs((fc - fcRef) / fcRef) <= 0.01);
			result3.set(i, 0, test);
			temps3.set(i, 0, res.getTime());
		}


		std::cout << "-------------------------------------------------------- " << std::endl;
		sys.resetParam();
	}


	std::cout << "--------------------GRAND CAS SANS GAMMA POUR COMPARER AVEC OPF----------------------------- " << std::endl;
	std::string name = "France";
	std::string path = "data/" + name + "/";
	const int nMethodeBig = 3;
	std::string methodesBig[nMethodeBig] = { "ADMMConst1", "ADMMGPUConst4", "DCOPFOSQP"};
	MatrixCPU result4(nMethodeBig, 1, -1);
	MatrixCPU temps4(nMethodeBig, 1, -1);


	MatrixCPU interval(4, 2);
	interval.set(0, 0, 2013);
	interval.set(0, 1, 2013);
	interval.set(1, 0, 6);
	interval.set(1, 1, 6);
	interval.set(2, 0, 1);
	interval.set(2, 1, 1);
	interval.set(3, 0, 0);
	interval.set(3, 1, 0);

	rho = 125;
	rhoL = rho;
	rho1 = 0.0000001;
	epsG = 0.001f;
	epsGC = 1.0f;
	epsL = 0.0001f;
	sys.setEpsGC(epsGC);
	sys.setEpsG(epsG);
	sys.setEpsL(epsL);
	for (int i = 0; i < 0; i++) {
		std::string methode = methodesBig[i];
		sys.setMethod(methode);
		sys.setRho(rho);
		sys.setRho1(rho1);
		sys.setRhoL(rhoL);
		std::cout << "----------------Simu---------------------------- " << std::endl;
		// Debut simu

		clock_t t = clock();

		sys.solveIntervalle(path, name, &interval);
		t = clock() - t;

	
		MatrixCPU fcMat(sys.getFc());
		if (i == 0) { // ce sera la ref
			fcRef = fcMat.get(0, 0);
			PnRef = sys.getPn();
			err = MatrixCPU(PnRef.getNLin(), 1, 1);
			std::cout << "fcRef " << fcRef << std::endl;
			result4.set(i, 0, 3);
			temps4.set(i, 0, (float)t / CLOCKS_PER_SEC);
		}
		else {
			fc = fcMat.get(0, 0);
			MatrixCPU P2 = sys.getPn();
			err.RelativeEror(&PnRef, &P2);
			errM = err.max2();
			std::cout << "fc " << fc << " err " << errM <<std::endl;
			//err.display();
			int test = ( errM < 0.01) + 2 * (fabs((fc - fcRef) / fcRef) <= 0.01);
			result4.set(i, 0, test);
			temps4.set(i, 0, (float) t / CLOCKS_PER_SEC);
		}


		std::cout << "-------------------------------------------------------- " << std::endl;
		sys.resetParam();
	}

	std::cout << "-------------------- RESULTATS ----------------------------- " << std::endl;

	result.display();
	temps.display();
	result2.display();
	temps2.display();
	result3.display();
	temps3.display();
	result4.display();
	temps4.display();

}

std::string generateDate(int year, int month, int day, int hour)
{
	std::string smonth;
	std::string sday;
	std::string shour;
	if (month < 10) {
		smonth = "0" + std::to_string(month);
	}
	else {
		smonth = std::to_string(month);
	}
	if (day < 10) {
		sday = "0" + std::to_string(day);
	}
	else {
		sday = std::to_string(day);
	}
	if (hour < 10) {
		shour = "0" + std::to_string(hour);
	}
	else {
		shour = std::to_string(hour);
	}
		
		

	std::string d = std::to_string(year) + "-" + smonth + "-" + sday + " " + shour +"-00-00";
	
	return d;
}

float pow10(int n)
{
	float v = 1;
	for (int i = 0; i < n; i++) {
		v = v * 10;
	}
	return v;
}

void getErrorSensiPwerLine()
{
	int _nGen = 969;
	int _nCons = 1494;
	int _nAgent = _nGen + _nCons;
	StudyCase cas;
	std::string path = "data/";
	std::string fileName3 = path + "SensiPowerReduceEurope.txt";
	std::string fileName4 = path + "lineLimitReduceEurope.txt";
	int _nLineConstraint = cas.getNFileline(fileName4);
	MatrixCPU _SensiPowerReduce = MatrixCPU(_nLineConstraint, _nAgent);
	MatrixCPU _lineLimitsReduce = MatrixCPU(_nLineConstraint, 1);
	_SensiPowerReduce.setFromFile(fileName3);
	_lineLimitsReduce.setFromFile(fileName4);

	MatrixCPU errorSensi(_nLineConstraint, 1);

	for (int l = 0; l < _nLineConstraint; l++) {
		float sum = 0;
		for (int n = 0; n < _nAgent; n++) {
			sum += abs(_SensiPowerReduce.get(l, n));
		}
		errorSensi.set(l, 0, sum);
	}
	std::cout << "resolution of the PowerLine " << errorSensi.max2() << std::endl;
	std::cout << "Puissance max ligne " << _lineLimitsReduce.max2() << std::endl;

}


void SimuTemporalLlimit(std::string name) {
	int type = 0;
	std::string path;
	int nGen;
	int nCons;
	int nAgent;
	int nLine;
	if (!name.compare("Europe")) {
		type = 1;
		path = "data/";
		nGen = 969;
		nCons = 1494;
		nAgent = nGen + nCons;
		nLine = 2156;
	}
	else {
		path = "data/" + name + "/";
	}
	std::string fileName = "SimutemporalLimit" + name + "June3.csv";
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	const int nMethode = 1;
	std::string methodes[nMethode] = { "ADMMGPUConst5" };
	//std::string method = "ADMMGPUConst1";
	//std::string method = "ADMMConst";

	//std::string methodes[nMethode] = { "ADMMGPUConst1","ADMMGPUConst2","ADMMGPUConst3", "ADMMGPUConst3" };
	float epsG = 0.001f;
	float epsGC = 0.1f;
	float epsL = 0.00001f;
	int warmstart = 0;

	MatrixCPU interval(4, 2);
	interval.set(0, 0, 2013);
	interval.set(0, 1, 2013);
	interval.set(1, 0, 6);
	interval.set(1, 1, 6);
	interval.set(2, 0, 3);
	interval.set(2, 1, 3);
	interval.set(3, 0, 0);
	interval.set(3, 1, 23);

	int iterG = 200000;
	int iterL = 5000;//500;


	const int nLimit = 5; // 
	MatrixCPU LilimitTab(1, nLimit);
	
	for (int i = 0; i < nLimit; i++) {
		LilimitTab.set(0, i, 300 * i);
	}

	float rho = 12; //rhoAgent * nAgent; //0.056 * nAgent;
	float rho1 = 0.00037; //0.0004;
	float rhoL = 12;

	float stepG = 10;
	float stepL = 1;


	System sys;
	StudyCase cas;
	sys.setIter(iterG, iterL);
	sys.setStep(stepG, stepL);
	sys.setEpsG(epsG);
	sys.setEpsGC(epsGC);
	sys.setEpsL(epsL);
	sys.setWarmStart(warmstart == 1);


	MatrixCPU Param(1, 21);
	Param.set(0, 0, 0);
	Param.set(0, 1, epsGC);
	Param.set(0, 2, epsG);
	Param.set(0, 3, epsL);
	Param.set(0, 4, iterG);
	Param.set(0, 5, iterL);
	Param.set(0, 6, stepG);
	Param.set(0, 7, stepL);
	Param.set(0, 8, rho);
	Param.set(0, 9, rho1);
	Param.set(0, 10, rhoL);
	Param.set(0, 11, interval.get(0, 0));
	Param.set(0, 12, interval.get(1, 0));
	Param.set(0, 13, interval.get(2, 0));
	Param.set(0, 14, interval.get(3, 0));
	Param.set(0, 15, interval.get(0, 1));
	Param.set(0, 16, interval.get(1, 1));
	Param.set(0, 17, interval.get(2, 1));
	Param.set(0, 18, interval.get(3, 1));
	Param.set(0, 19, nLimit);
	Param.set(0, 20, warmstart);
	
	
	std::cout << " Save Y:1, N:0" << std::endl;
	int inputUser;
	std::cin >> inputUser;
	if (inputUser) {
		Param.saveCSV(fileName, mode);
		LilimitTab.saveCSV(fileName, mode);
	}


	for (int i = 0; i < nMethode; i++) {
		std::string methode = methodes[i];
		sys.setMethod(methode);
		sys.setRho(rho);
		sys.setRho1(rho1);
		sys.setRhoL(rhoL);
		for (int j = 0; j < nLimit; j++) {
			float limit = LilimitTab.get(0, j);
			
			sys.setLineLimitMin(limit);

			std::cout << "----------------Simu---------------------------- " << std::endl;
			// Debut simu
			clock_t t = clock();
			if (type) {
				sys.solveIntervalle(path, &interval, nCons, nGen);
			}
			else {
				sys.solveIntervalle(path, name, &interval);
			}

			t = clock() - t;

			std::cout << "temps simulation : " << t / CLOCKS_PER_SEC << std::endl;

			MatrixCPU iter(sys.getIter());
			MatrixCPU ResR(sys.getResR());
			MatrixCPU ResS(sys.getResS());
			MatrixCPU ResX(sys.getResX());
			MatrixCPU tempTab(sys.getTemps());

			if (inputUser) {
				iter.saveCSV(fileName, mode);
				ResR.saveCSV(fileName, mode);
				ResS.saveCSV(fileName, mode);
				ResX.saveCSV(fileName, mode);
				tempTab.saveCSV(fileName, mode);
				iter.display();
				ResR.display();
				ResS.display();
				ResX.display();
				tempTab.display();
			}
			else {
				iter.display();
				ResR.display();
				ResS.display();
				ResX.display();
				tempTab.display();
			}
			std::cout << "-------------------------------------------------------- " << std::endl;
			sys.resetParam();
			sys.resetMethod();
		}
		
	}
}


void SimuTemporalConvergence(std::string name) {
	int type = 0;
	std::string path;
	int nGen;
	int nCons;
	int nAgent;
	int nLine;
	if (!name.compare("Europe")) {
		type = 1;
		path = "data/";
		nGen = 969;
		nCons = 1494;
		nAgent = nGen + nCons;
		nLine = 2156;
	}
	else {
		path = "data/" + name + "/";
	}
	std::string fileName = "SGESimutemporalConvergenceRho" + name + ".csv";
	std::ios_base::openmode mode = std::fstream::in | std::fstream::out | std::fstream::app;
	const int nMethode = 1;
	std::string methodes[nMethode] = { "ADMMGPUConst4" };
	//std::string method = "ADMMGPUConst1";
	//std::string method = "ADMMConst";

	//std::string methodes[nMethode] = { "ADMMGPUConst1","ADMMGPUConst2","ADMMGPUConst3", "ADMMGPUConst3" };
	float epsG = 0.001f;
	float epsGC = 1.0f;
	float epsL = 0.00001f;
	int warmstart = 1;
	float offset = 0;

	MatrixCPU interval(4, 2);
	interval.set(0, 0, 2013);
	interval.set(0, 1, 2013);
	interval.set(1, 0, 6);
	interval.set(1, 1, 6);
	interval.set(2, 0, 3);
	interval.set(2, 1, 3);
	interval.set(3, 0, 10);
	interval.set(3, 1, 19);

	int iterG = 100000;
	int iterL = 10000;//500;


	float rho = 10; //rhoAgent * nAgent; //0.056 * nAgent;
	float rho1 = 0.0005; //0.00037;
	float rhoL = 10;

	float stepG = 10;
	float stepL = 1;


	System sys;
	StudyCase cas;
	sys.setIter(iterG, iterL);
	sys.setStep(stepG, stepL);
	sys.setEpsG(epsG);
	sys.setEpsGC(epsGC);
	sys.setEpsL(epsL);
	sys.setWarmStart(warmstart == 1);
	sys.setConstraintRelaxation(offset);

	MatrixCPU Param(1, 21);
	Param.set(0, 0, 0);
	Param.set(0, 1, epsGC);
	Param.set(0, 2, epsG);
	Param.set(0, 3, epsL);
	Param.set(0, 4, iterG);
	Param.set(0, 5, iterL);
	Param.set(0, 6, stepG);
	Param.set(0, 7, stepL);
	Param.set(0, 8, rho);
	Param.set(0, 9, rho1);
	Param.set(0, 10, rhoL);
	Param.set(0, 11, interval.get(0, 0));
	Param.set(0, 12, interval.get(1, 0));
	Param.set(0, 13, interval.get(2, 0));
	Param.set(0, 14, interval.get(3, 0));
	Param.set(0, 15, interval.get(0, 1));
	Param.set(0, 16, interval.get(1, 1));
	Param.set(0, 17, interval.get(2, 1));
	Param.set(0, 18, interval.get(3, 1));
	Param.set(0, 19, offset);
	Param.set(0, 20, warmstart);


	std::cout << " Save Y:1, N:0" << std::endl;
	int inputUser;
	std::cin >> inputUser;
	if (inputUser) {
		Param.saveCSV(fileName, mode);
	}


	for (int i = 0; i < nMethode; i++) {
		std::string methode = methodes[i];
		sys.setMethod(methode);
		sys.setRho(rho);
		sys.setRho1(rho1);
		sys.setRhoL(rhoL);
		
			

		std::cout << "----------------Simu---------------------------- " << std::endl;
		// Debut simu
		clock_t t = clock();
		if (type) {
			sys.solveIntervalle(path, &interval, nCons, nGen);
		}
		else {
			sys.solveIntervalle(path, name, &interval);
		}

		t = clock() - t;

		std::cout << "temps simulation : " << t / CLOCKS_PER_SEC << std::endl;

		MatrixCPU iter(sys.getIter());
		MatrixCPU ResR(sys.getResR());
		MatrixCPU ResS(sys.getResS());
		MatrixCPU ResX(sys.getResX());
		MatrixCPU tempTab(sys.getTemps());
		MatrixCPU fcTab(sys.getFc());

		if (inputUser) {
			tempTab.saveCSV(fileName, mode);
			iter.saveCSV(fileName, mode);
			ResR.saveCSV(fileName, mode);
			ResS.saveCSV(fileName, mode);
			ResX.saveCSV(fileName, mode);
			fcTab.saveCSV(fileName, mode);
			iter.display();
			ResR.display();
			ResS.display();
			ResX.display();
			tempTab.display();
		}
		else {
			iter.display();
			ResR.display();
			ResS.display();
			ResX.display();
			tempTab.display();
		}
		std::cout << "-------------------------------------------------------- " << std::endl;
		sys.resetParam();
		sys.resetMethod();
		

	}
}








