#pragma once


#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include "Agent.h"
#include "MatrixCPU.h"
#include "MatrixGPU.cuh"
#include <math.h>
#include <iostream>
#include <vector>

#define DELETEB(x) if (x!=nullptr) {delete x; x = nullptr;}
#define DELETEA(x) if (x!=nullptr) {delete[] x; x = nullptr;}

class StudyCase
{
    static constexpr float LINELIMITMAX = 100000;
   
    int _nAgent;
    int _nBus;
    int _nLine;
    int _nLineConstraint = 0;
    int _nPro;
    int _nGen;
    int _nCons;
    std::string _name = "None";
    float _timeInit;
    Agent* _agents = nullptr;
    MatrixCPU _BETA;
    MatrixCPU _connect;
    MatrixCPU _a;
    MatrixCPU _b;
    MatrixCPU _Ub;
    MatrixCPU _Lb;
    MatrixCPU _Pmin;
    MatrixCPU _Pmax;
    MatrixCPU _nVoisin;
    MatrixCPU _CoresBusAgent; // I
    MatrixCPU _LineImpedance; // B
    MatrixCPU _CoresBusLine; // C
    MatrixCPU _CoresLineBus; // Perso
    MatrixCPU _CoresLineBusReduce; // Perso
    MatrixCPU _SensiBusLine; // A
    MatrixCPU _SensiPower; // G = A*I
    MatrixCPU _SensiPowerReduce; // Gred
    MatrixCPU _lineLimits; // l
    MatrixCPU _lineLimitsReduce; // l
    MatrixCPU _lineLimitsChange; // l
    std::vector<int> _indiceLineConstraint;// numero de ligne contrainte
    std::vector<int> _indiceLineNonConstraint;// numero de ligne non contrainte
    
    MatrixCPU _Distance; // Dij = sum |Pl|
    MatrixCPU _zoneBus; // taille B*1 indique pour chaque agent la zone o� il est 
    std::vector<std::string> _nameZone;

    void setMatFromFile(const std::string& path, const std::string& date, MatrixCPU* Pgen, MatrixCPU* P0, MatrixCPU* costGen);
    void setGenFromFile(const std::string& path, MatrixCPU* Pgen, MatrixCPU* costGen, MatrixCPU* BusGen);
    void setGridFromFile(const std::string& path, MatrixCPU* fileBusAgent);
    void setBusFromFile(const std::string& path, MatrixCPU* fileCoresBus);
    
    void CalcGridSensi();
    float rand1() const; 
    
    

public:
    int getNFileline(std::string nameFile);
    int _invertMethod = 0;
    bool toReduce = false;
    float lineMin = 0;
    float lineoffset = 0;

    StudyCase();
    
    StudyCase(int nAgent, float P, float dP, float a, float da, float b, float db, float propCons = 0.5, float propPro = 0.125);
    StudyCase(int nAgent, float P0, float dP, float b, float db, float propCons = 0.5);
    StudyCase(const StudyCase& s);
    StudyCase(std::string fileName);
    StudyCase& operator= (const StudyCase& s);

    // generator
    void UpdateP0(MatrixCPU* P0);
    void genConnec(MatrixCPU* connec, int nCons, int nGen, int nPo);
    void genBetaUniforme(float beta);
    void genBetaDistance(float s);
    void genBetaDistanceByZone(MatrixCPU* s);
    void genGrid(int _nBus, int _nMajorLine, int _minorLine, float ReacMajor, float DeltaReacMajor, float ReacMinor, float DeltaReacMinor, float LlimitMajor, float dLlimitMajor, float LlimitMinor, float dLlimitMinor);
    void genGridFromFile(std::string path, bool alreadyDefine=true);
    void genAgents(int nAgent, float propCons, float Pconso, float dPconso,  float bProd, float dbProd, float Pprod, float dPpord, float gamma, float dGamma); // gamma = -1 distance ?
    void genLinkGridAgent();
    void genLineLimit(int nLine, float limit, float dlimit );

    //Setter
    void Set29node();
    void Set39Bus(std::string path = "data/", bool alreadyDefine=false);
    void Set3Bus(std::string path = "data/");
    void Set4node();
    void Set2node();
    void Set4nodeBis(std::string path);
    void Set2nodeConstraint(float lim = 0.8);
    void SetEuropeP0(const std::string& path, MatrixCPU* P0, bool alreadyDefine=0);
    void SetStudyCase(std::string path, std::string name, MatrixCPU* P0, bool alreadyDefine=0);
    void setDistance(bool alreadyDefine = false, std::string path = "data/distance.txt");
    void setBricolage(float offset = 1);
    void setLineLimitMin(float lineMin);
    void setLineLimitRelaxation(float eps);
    void setLineLimit(int line, float limit);
    // getter 
    MatrixCPU getBeta() const;
    MatrixCPU getC() const;
    MatrixCPU geta() const;
    MatrixCPU getb() const;
    MatrixCPU getUb() const;
    MatrixCPU getLb() const;
    MatrixCPU getPmin() const;
    MatrixCPU getPmax() const;
    MatrixCPU getNvoi() const;
    MatrixCPU getPowerSensi() const;
    MatrixCPU getLineLimit() const;
    MatrixCPU getCoresLineBus() const;
    float getTimeInit() const;
    int getNagent() const;
    int getNCons() const;
    int getNLine() const;
    int getNBus() const;
    MatrixCPU getVoisin(int agent) const;
    Agent getAgent(int agent) const;
    std::string getName() const;

    void removeLink(int i, int j);
    Agent removeAgent(int agent);
    void restoreAgent(Agent& agent, bool all = false);
    void addLink(int i, int j);
    
   
    
    void saveCSV(const std::string& fileName, bool all = true);
    void display(int type=0);
    void displayLineCores(MatrixCPU* g, bool all = true);
    ~StudyCase();

};
